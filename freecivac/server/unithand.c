/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "city.h"
#include "combat.h"
#include "events.h"
#include "fcintl.h"
#include "game.h"
#include "log.h"
#include "map.h"
#include "mem.h"
#include "packets.h"
#include "player.h"
#include "rand.h"
#include "shared.h"
#include "unit.h"

#include "barbarian.h"
#include "citytools.h"
#include "cityturn.h"
#include "diplomats.h"
#include "gamelog.h"
#include "gotohand.h"
#include "maphand.h"
#include "plrhand.h"
#include "settlers.h"
#include "spacerace.h"
#include "srv_main.h"
#include "unittools.h"

#include "aitools.h"
#include "aiunit.h"

#include "unithand.h"

static void city_add_or_build_error(struct player *pplayer,
				    struct unit *punit,
				    enum add_build_city_result res);
static void city_add_unit(struct player *pplayer, struct unit *punit);
static void city_build(struct player *pplayer, struct unit *punit,
		       char *name);
static void handle_unit_activity_request_targeted(struct unit *punit,
						  enum unit_activity
						  new_activity,
						  enum tile_special_type
						  new_target);

/**************************************************************************
...
**************************************************************************/
void handle_unit_goto_tile(struct player *pplayer, 
			   struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);

  if (!is_normal_map_pos(req->x, req->y) || !punit) {
    return;
  }

  set_goto_dest(punit, req->x, req->y);

  set_unit_activity(punit, ACTIVITY_GOTO);

  send_unit_info(NULL, punit);

  /* 
   * Normally units on goto does not pick up extra units, even if the
   * units are in a city and are sentried. But if we just started the
   * goto We want to take them with us, so we do this. 
   */
  if (get_transporter_capacity(punit) > 0) {
    assign_units_to_transporter(punit, TRUE);
  }

  (void) do_unit_goto(punit, GOTO_MOVE_ANY, TRUE);
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_airlift(struct player *pplayer,
			 struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);
  struct city *pcity;

  if (!is_normal_map_pos(req->x, req->y)) {
    return;
  }

  pcity = map_get_city(req->x, req->y);
  if (punit && pcity) {
    (void) do_airline(punit, pcity);
  }
}

/**************************************************************************
Handler for PACKET_UNIT_CONNECT request. The unit is send on way and will 
build something (roads only for now) along the way, using server-side
path-finding. 

FIXME: This should be rewritten to use client-side path finding along so 
that we can show in the client where the road-to-be-built will be and 
enable the use of waypoints to alter this route. - Per
**************************************************************************/
void handle_unit_connect(struct player *pplayer, 
		          struct packet_unit_connect *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);

  if (!is_normal_map_pos(req->dest_x, req->dest_y) || !punit
      || !can_unit_do_connect(punit, req->activity_type)) {
    return;
  }

  set_goto_dest(punit, req->dest_x, req->dest_y);

  set_unit_activity(punit, req->activity_type);
  punit->connecting = TRUE;

  send_unit_info(NULL, punit);

  /* 
   * Avoid wasting first turn if unit cannot do the activity on the
   * starting tile.
   */
  if (!can_unit_do_activity(punit, req->activity_type)) {
    (void) do_unit_goto(punit,
			get_activity_move_restriction(req->activity_type),
			FALSE);
  }
}

/**************************************************************************
 Upgrade all units of a given type.
**************************************************************************/
void handle_upgrade_unittype_request(struct player * const pplayer,
			const struct packet_unittype_info * const packet)
{
  const Unit_Type_id from_unittype = packet->type;
  const Unit_Type_id to_unittype = can_upgrade_unittype(pplayer,
							from_unittype, TRUE);

  if (to_unittype == -1) {
    notify_player(pplayer,
		  _("Game: Illegal packet, can't upgrade %s (yet)."),
                  unit_types[from_unittype].name);
  } else {
    const int cost = unit_upgrade_price(pplayer, from_unittype, to_unittype);
    int number_of_upgraded_units = 0;

    if (pplayer->economic.gold >= cost) {
      const int player_no = pplayer->player_no;

      /* Try to upgrade units. The order we upgrade in is arbitrary (if
       * the player really cared they should have done it manually). */
      conn_list_do_buffer(&pplayer->connections);
      unit_list_iterate(pplayer->units, punit) {
        if (punit->type == from_unittype) {
          const struct city * const pcity = map_get_city(punit->x, punit->y);

	  /* Only units in cities can be upgraded. */
          if (pcity && pcity->owner == player_no) {
            upgrade_unit(punit, to_unittype);
	    number_of_upgraded_units++;
            if ((pplayer->economic.gold -= cost) < cost) {
	      /* We can't upgrade any more units. */
              break;
            }
          }
        }
      } unit_list_iterate_end;
      conn_list_do_unbuffer(&pplayer->connections);
    }

    /* Alert the player about what happened. */
    if (number_of_upgraded_units > 0) {
      notify_player(pplayer, _("Game: %d %s upgraded to %s for %d gold."),
                    number_of_upgraded_units, unit_types[from_unittype].name,
                    unit_types[to_unittype].name,
                    cost * number_of_upgraded_units);
      send_player_info(pplayer, pplayer);
    } else {
      notify_player(pplayer, _("Game: No units could be upgraded."));
    }
  }
}

/**************************************************************************
 Upgrade a single unit.
 TODO: should upgrades in allied cities be possible?
**************************************************************************/
void handle_unit_upgrade_request(struct player *pplayer,
                                 struct packet_unit_request *packet)
{
  int cost;
  int from_unit, to_unit;
  struct unit *punit = player_find_unit_by_id(pplayer, packet->unit_id);
  struct city *pcity = player_find_city_by_id(pplayer, packet->city_id);
  
  if (!punit || !pcity) {
    return;
  }

  if(!same_pos(punit->x, punit->y, pcity->x, pcity->y))  {
    notify_player(pplayer, _("Game: Illegal move, unit not in city!"));
    return;
  }
  from_unit = punit->type;
  if((to_unit=can_upgrade_unittype(pplayer, punit->type, TRUE)) == -1) {
    notify_player(pplayer, _("Game: Illegal package, can't upgrade %s (yet)."), 
		  unit_type(punit)->name);
    return;
  }
  cost = unit_upgrade_price(pplayer, punit->type, to_unit);
  if(cost > pplayer->economic.gold) {
    notify_player(pplayer, _("Game: Insufficient funds, upgrade costs %d."),
		  cost);
    return;
  }
  pplayer->economic.gold -= cost;
  upgrade_unit(punit, to_unit);
  send_player_info(pplayer, pplayer);
  notify_player(pplayer, _("Game: %s upgraded to %s for %d gold."), 
		unit_name(from_unit), unit_name(to_unit), cost);
}


/***************************************************************
  Tell the client the cost of inciting a revolt or bribing a unit.
  Only send result back to the requesting connection, not all
  connections for that player.
***************************************************************/
void handle_incite_inq(struct connection *pconn,
		       struct packet_generic_integer *packet)
{
  struct player *pplayer = pconn->player;
  struct city *pcity = find_city_by_id(packet->value);
  struct unit *punit = find_unit_by_id(packet->value);
  struct packet_generic_values req;

  if (!pplayer) {
    freelog(LOG_ERROR, "Incite inquiry from non-player %s",
	    conn_description(pconn));
    return;
  }
  if (pcity) {
    req.value1 = city_incite_cost(pplayer, pcity);
    req.id = packet->value;
    send_packet_generic_values(pconn, PACKET_INCITE_COST, &req);
    return;
  }
  if (punit) {
    punit->bribe_cost = unit_bribe_cost(punit);
    req.id = packet->value;
    req.value1 = punit->bribe_cost;
    send_packet_generic_values(pconn, PACKET_INCITE_COST, &req);
  }
}

/***************************************************************
...
***************************************************************/
void handle_diplomat_action(struct player *pplayer, 
			    struct packet_diplomat_action *packet)
{
  struct unit *pdiplomat=player_find_unit_by_id(pplayer, packet->diplomat_id);
  struct unit *pvictim=find_unit_by_id(packet->target_id);
  struct city *pcity=find_city_by_id(packet->target_id);

  if (!pdiplomat || !unit_flag(pdiplomat, F_DIPLOMAT)) {
    return;
  }

  if(pdiplomat->moves_left > 0) {
    switch(packet->action_type) {
    case DIPLOMAT_BRIBE:
      if(pvictim && diplomat_can_do_action(pdiplomat, DIPLOMAT_BRIBE,
					   pvictim->x, pvictim->y)) {
	diplomat_bribe(pplayer, pdiplomat, pvictim);
      }
      break;
    case SPY_SABOTAGE_UNIT:
      if(pvictim && diplomat_can_do_action(pdiplomat, SPY_SABOTAGE_UNIT,
					   pvictim->x, pvictim->y)) {
	spy_sabotage_unit(pplayer, pdiplomat, pvictim);
      }
      break;
     case DIPLOMAT_SABOTAGE:
      if(pcity && diplomat_can_do_action(pdiplomat, DIPLOMAT_SABOTAGE,
					 pcity->x, pcity->y)) {
	/* packet value is improvement ID + 1 (or some special codes) */
	diplomat_sabotage(pplayer, pdiplomat, pcity, packet->value - 1);
      }
      break;
    case SPY_POISON:
      if(pcity && diplomat_can_do_action(pdiplomat, SPY_POISON,
					 pcity->x, pcity->y)) {
	spy_poison(pplayer, pdiplomat, pcity);
      }
      break;
    case DIPLOMAT_INVESTIGATE:
      if(pcity && diplomat_can_do_action(pdiplomat,DIPLOMAT_INVESTIGATE,
					 pcity->x, pcity->y)) {
	diplomat_investigate(pplayer, pdiplomat, pcity);
      }
      break;
    case DIPLOMAT_EMBASSY:
      if(pcity && diplomat_can_do_action(pdiplomat, DIPLOMAT_EMBASSY,
					 pcity->x, pcity->y)) {
	diplomat_embassy(pplayer, pdiplomat, pcity);
      }
      break;
    case DIPLOMAT_INCITE:
      if(pcity && diplomat_can_do_action(pdiplomat, DIPLOMAT_INCITE,
					 pcity->x, pcity->y)) {
	diplomat_incite(pplayer, pdiplomat, pcity);
      }
      break;
    case DIPLOMAT_MOVE:
      if(pcity && diplomat_can_do_action(pdiplomat, DIPLOMAT_MOVE,
					 pcity->x, pcity->y)) {
	(void) handle_unit_move_request(pdiplomat, pcity->x, pcity->y,
					FALSE, TRUE);
      }
      break;
    case DIPLOMAT_STEAL:
      if(pcity && diplomat_can_do_action(pdiplomat, DIPLOMAT_STEAL,
					 pcity->x, pcity->y)) {
	/* packet value is technology ID (or some special codes) */
	diplomat_get_tech(pplayer, pdiplomat, pcity, packet->value);
      }
      break;
    case SPY_GET_SABOTAGE_LIST:
      if(pcity && diplomat_can_do_action(pdiplomat, SPY_GET_SABOTAGE_LIST,
					 pcity->x, pcity->y)) {
	spy_get_sabotage_list(pplayer, pdiplomat, pcity);
      }
      break;
    }
  }
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_change_homecity(struct player *pplayer, 
				 struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);
  struct city *old_pcity, *new_pcity =
      player_find_city_by_id(pplayer, req->city_id);

  if (!punit || !new_pcity) {
    return;
  }
  old_pcity = player_find_city_by_id(pplayer, punit->homecity);

  unit_list_insert(&new_pcity->units_supported, punit);
  if (old_pcity) {
    unit_list_unlink(&old_pcity->units_supported, punit);
  }

  punit->homecity = req->city_id;
  send_unit_info(pplayer, punit);

  city_refresh(new_pcity);
  send_city_info(pplayer, new_pcity);

  if (old_pcity) {
    city_refresh(old_pcity);
    send_city_info(pplayer, old_pcity);
  }
}

/**************************************************************************
  Disband a unit.  If its in a city, add 1/2 of the worth of the unit
  to the city's shield stock for the current production.
  "iter" may be NULL, see wipe_unit_safe for details.
**************************************************************************/
static void do_unit_disband_safe(struct city *pcity, struct unit *punit,
				 struct genlist_iterator *iter)
{
  if (pcity) {
    pcity->shield_stock += (unit_type(punit)->build_cost/2);
    /* If we change production later at this turn. No penalty is added. */
    pcity->disbanded_shields += (unit_type(punit)->build_cost/2);

    /* Note: Nowadays it's possible to disband unit in allied city and
     * your ally receives those shields. Should it be like this? Why not?
     * That's why we must use city_owner instead of pplayer -- Zamar */
    send_city_info(city_owner(pcity), pcity);
  }
  wipe_unit_safe(punit, iter);
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_disband_safe(struct player *pplayer, 
			      struct packet_unit_request *req,
			      struct genlist_iterator *iter)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);

  if (!punit) {
    return;
  }

  do_unit_disband_safe(map_get_city(punit->x, punit->y), punit, iter);
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_disband(struct player *pplayer, 
			 struct packet_unit_request *req)
{
  handle_unit_disband_safe(pplayer, req, NULL);
}

/**************************************************************************
 This function assumes that there is a valid city at punit->(x,y) for
 certain values of test_add_build_or_city.  It should only be called
 after a call to unit_add_build_city_result, which does the
 consistency checking.
**************************************************************************/
static void city_add_or_build_error(struct player *pplayer,
				    struct unit *punit,
				    enum add_build_city_result res)
{
  /* Given that res came from test_unit_add_or_build_city, pcity will
     be non-null for all required status values. */
  struct city *pcity = map_get_city(punit->x, punit->y);
  char *unit_name = unit_type(punit)->name;

  switch (res) {
  case AB_NOT_BUILD_LOC:
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: Can't place city here."));
    break;
  case AB_NOT_BUILD_UNIT:
    {
      const char *us = get_units_with_flag_string(F_CITIES);
      if (us) {
	notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
			 _("Game: Only %s can build a city."),
			 us);
	free((void *) us);
      } else {
	notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
			 _("Game: Can't build a city."));
      }
    }
    break;
  case AB_NOT_ADDABLE_UNIT:
    {
      const char *us = get_units_with_flag_string(F_ADD_TO_CITY);
      if (us) {
	notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
			 _("Game: Only %s can add to a city."),
			 us);
	free((void *) us);
      } else {
	notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
			 _("Game: Can't add to a city."));
      }
    }
    break;
  case AB_NO_MOVES_ADD:
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: %s unit has no moves left to add to %s."),
		     unit_name, pcity->name);
    break;
  case AB_NO_MOVES_BUILD:
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: %s unit has no moves left to build city."),
		     unit_name);
    break;
  case AB_TOO_BIG:
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: %s is too big to add %s."),
		     pcity->name, unit_name);
    break;
  case AB_NO_AQUEDUCT:
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: %s needs an improvement to grow, so "
		       "you cannot add %s."),
		     pcity->name, unit_name);
    break;
  default:
    /* Shouldn't happen */
    freelog(LOG_ERROR, "Cannot add %s to %s for unknown reason",
	    unit_name, pcity->name);
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: Can't add %s to %s."),
		     unit_name, pcity->name);
    break;
  }
}

/**************************************************************************
 This function assumes that there is a valid city at punit->(x,y) It
 should only be called after a call to a function like
 test_unit_add_or_build_city, which does the checking.
**************************************************************************/
static void city_add_unit(struct player *pplayer, struct unit *punit)
{
  struct city *pcity = map_get_city(punit->x, punit->y);
  char *unit_name = unit_type(punit)->name;

  assert(unit_pop_value(punit->type) > 0);
  pcity->size += unit_pop_value(punit->type);
  add_adjust_workers(pcity);
  wipe_unit(punit);
  send_city_info(NULL, pcity);
  notify_player_ex(pplayer, pcity->x, pcity->y, E_NOEVENT,
		   _("Game: %s added to aid %s in growing."),
		   unit_name, pcity->name);
}

/**************************************************************************
 This function assumes a certain level of consistency checking: There
 is no city under punit->(x,y), and that location is a valid one on
 which to build a city. It should only be called after a call to a
 function like test_unit_add_or_build_city, which does the checking.
**************************************************************************/
static void city_build(struct player *pplayer, struct unit *punit,
		       char *name)
{
  if (!is_sane_name(name)) {
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: Let's not build a city with "
		       "such a stupid name."));
    return;
  }

  if (!is_allowed_city_name(pplayer, name, punit->x, punit->y, TRUE)) {
    return;
  }

  create_city(pplayer, punit->x, punit->y, name);
  wipe_unit(punit);
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_build_city(struct player *pplayer,
			    struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);
  enum add_build_city_result res;

  if (!punit) {
    return;
  }

  res = test_unit_add_or_build_city(punit);

  if (res == AB_BUILD_OK)
    city_build(pplayer, punit, req->name);
  else if (res == AB_ADD_OK)
    city_add_unit(pplayer, punit);
  else
    city_add_or_build_error(pplayer, punit, res);
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_info(struct player *pplayer, struct packet_unit_info *pinfo)
{
  struct unit *punit = player_find_unit_by_id(pplayer, pinfo->id);

  if (!punit || !is_normal_map_pos(pinfo->x, pinfo->y)) {
    return;
  }

  if (!same_pos(punit->x, punit->y, pinfo->x, pinfo->y)) {
    if (is_tiles_adjacent(punit->x, punit->y, pinfo->x, pinfo->y)) {
      punit->ai.control = FALSE;
      (void) handle_unit_move_request(punit, pinfo->x, pinfo->y, FALSE, FALSE);
    } else {
      /* This can happen due to lag, so don't complain too loudly */
      freelog(LOG_DEBUG, "tiles are not adjacent, unit pos %d,%d trying "
	      "to go to  %d,%d!\n", punit->x, punit->y, pinfo->x, pinfo->y);
    }
  } else if (punit->activity != pinfo->activity ||
	     punit->activity_target != pinfo->activity_target ||
	     punit->ai.control) {
    punit->ai.control = FALSE;
    handle_unit_activity_request_targeted(punit,
					  pinfo->activity,
					  pinfo->activity_target);

    /* Exploring is handled here explicitly, since the player expects to
     * see an immediate response from setting a unit to auto-explore.
     * Handling it deeper in the code leads to some tricky recursive loops -
     * see PR#2631. */
    if (punit->moves_left > 0 && pinfo->activity == ACTIVITY_EXPLORE) {
      int id = punit->id;
      bool more_to_explore = ai_manage_explorer(punit);

      if ((punit = find_unit_by_id(id))) {
	if (more_to_explore) {
	  /* ai_manage_explorer sets the activity to idle, so we reset
	   * it. */
          set_unit_activity(punit, ACTIVITY_EXPLORE);
        } else {
          punit->ai.control = FALSE;
        }
        send_unit_info(NULL, punit);
      }
    }
  }
}

/**************************************************************************
...
**************************************************************************/
void handle_move_unit(struct player *pplayer, struct packet_move_unit *pmove)
{
  struct unit *punit = player_find_unit_by_id(pplayer, pmove->unid);

  if (!is_normal_map_pos(pmove->x, pmove->y) || !punit
      || !is_tiles_adjacent(punit->x, punit->y, pmove->x, pmove->y)) {
    return;
  }
  (void) handle_unit_move_request(punit, pmove->x, pmove->y, FALSE, FALSE);
}

/**************************************************************************
This function assumes the attack is legal. The calling function should have
already made all neccesary checks.
**************************************************************************/
static void handle_unit_attack_request(struct unit *punit, struct unit *pdefender)
{
  struct player *pplayer = unit_owner(punit);
  struct packet_unit_combat combat;
  struct unit *plooser, *pwinner;
  struct city *pcity;
  int moves_used, def_moves_used; 
  int def_x = pdefender->x, def_y = pdefender->y;
  struct packet_unit_info unit_att_packet, unit_def_packet;

  freelog(LOG_DEBUG, "Start attack: %s's %s against %s's %s.",
	  pplayer->name, unit_type(punit)->name, 
	  unit_owner(pdefender)->name,
	  unit_type(pdefender)->name);

  /* Sanity checks */
  if (pplayers_non_attack(unit_owner(punit), unit_owner(pdefender))) {
    die("Trying to attack a unit with which you have peace "
	"or cease-fire at %i, %i", def_x, def_y);
  }
  if (pplayers_allied(unit_owner(punit), unit_owner(pdefender))
      && !(unit_flag(punit, F_NUCLEAR) && punit == pdefender)) {
    die("Trying to attack a unit with which you have alliance at %i, %i",
	def_x, def_y);
  }

  if (unit_flag(punit, F_NUCLEAR)) {
    struct packet_nuke_tile packet;
    
    packet.x=def_x;
    packet.y=def_y;
    if ((pcity=sdi_defense_close(unit_owner(punit), def_x, def_y))) {
      notify_player_ex(pplayer, punit->x, punit->y, E_UNIT_LOST_ATT,
		       _("Game: Your Nuclear missile was shot down by"
			 " SDI defences, what a waste."));
      notify_player_ex(city_owner(pcity), def_x, def_y, E_UNIT_WIN,
		       _("Game: The nuclear attack on %s was avoided by"
			 " your SDI defense."), pcity->name);
      wipe_unit(punit);
      return;
    } 

    lsend_packet_nuke_tile(&game.game_connections, &packet);

    wipe_unit(punit);
    do_nuclear_explosion(pplayer, def_x, def_y);
    return;
  }
  moves_used = unit_move_rate(punit) - punit->moves_left;
  def_moves_used = unit_move_rate(pdefender) - pdefender->moves_left;

  unit_versus_unit(punit, pdefender);

  /* Adjust attackers moves_left _after_ unit_versus_unit() so that
   * the movement attack modifier is correct! --dwp
   *
   * For greater Civ2 compatibility (and game balance issues), we recompute 
   * the new total MP based on the HP the unit has left after being damaged, 
   * and subtract the MPs that had been used before the combat (plus the 
   * points used in the attack itself, for the attacker). -GJW, Glip
   */
  punit->moves_left = unit_move_rate(punit) - moves_used - SINGLE_MOVE;
  pdefender->moves_left = unit_move_rate(pdefender) - def_moves_used;
  
  if (punit->moves_left < 0) {
    punit->moves_left = 0;
  }
  if (pdefender->moves_left < 0) {
    pdefender->moves_left = 0;
  }

  if (punit->hp &&
      (pcity=map_get_city(def_x, def_y)) &&
      pcity->size>1 &&
      !is_under_effect(B_LAST,pcity,NULL,EFT_UNIT_NO_LOSE_POP) &&
      kills_citizen_after_attack(punit)) {
    city_reduce_size(pcity,1);
    city_refresh(pcity);
    send_city_info(NULL, pcity);
  }
  if (unit_flag(punit, F_ONEATTACK)) 
    punit->moves_left = 0;
  pwinner = (punit->hp > 0) ? punit : pdefender;
  plooser = (pdefender->hp > 0) ? punit : pdefender;
    
  combat.attacker_unit_id=punit->id;
  combat.defender_unit_id=pdefender->id;
  combat.attacker_hp=punit->hp;
  combat.defender_hp=pdefender->hp;
  combat.make_winner_veteran=pwinner->veteran?1:0;

  package_unit(punit, &unit_att_packet, FALSE, UNIT_INFO_IDENTITY, 0,
	       FALSE);
  package_unit(pdefender, &unit_def_packet, FALSE, UNIT_INFO_IDENTITY,
	       0, FALSE);
  
  players_iterate(other_player) {
    if (map_get_known_and_seen(punit->x, punit->y, other_player) ||
	map_get_known_and_seen(def_x, def_y, other_player)) {

      /* 
       * Special case for attacking/defending:
       * 
       * Normally the player doesn't get the information about the
       * units inside a city. However for attacking/defending the
       * player has to know the unit of the other side.
       */

      lsend_packet_unit_info(&other_player->connections, &unit_att_packet);
      lsend_packet_unit_info(&other_player->connections, &unit_def_packet);
      lsend_packet_unit_combat(&other_player->connections, &combat);
    }
  } players_iterate_end;

  conn_list_iterate(game.game_connections, pconn) {
    if (!pconn->player && pconn->observer) {
      send_packet_unit_info(pconn, &unit_att_packet);
      send_packet_unit_info(pconn, &unit_def_packet);
      send_packet_unit_combat(pconn, &combat);
    }
  } conn_list_iterate_end;
  
  if (punit == plooser) {
    /* The attacker lost */
    freelog(LOG_DEBUG, "Attacker lost: %s's %s against %s's %s.",
	    pplayer->name, unit_type(punit)->name,
	    unit_owner(pdefender)->name, unit_type(pdefender)->name);

    notify_player_ex(unit_owner(pwinner),
		     pwinner->x, pwinner->y, E_UNIT_WIN,
		     _("Game: Your %s%s survived the pathetic attack"
		       " from %s's %s."),
		     unit_name(pwinner->type),
		     get_location_str_in(unit_owner(pwinner),
					 pwinner->x, pwinner->y),
		     unit_owner(plooser)->name, unit_name(plooser->type));
    
    notify_player_ex(unit_owner(plooser),
		     def_x, def_y, E_UNIT_LOST_ATT,
		     _("Game: Your attacking %s failed "
		       "against %s's %s%s!"),
		     unit_name(plooser->type), unit_owner(pwinner)->name,
		     unit_name(pwinner->type),
		     get_location_str_at(unit_owner(plooser),
					 pwinner->x, pwinner->y));
    wipe_unit(plooser);
  } else {
    /* The defender lost, the attacker punit lives! */
    freelog(LOG_DEBUG, "Defender lost: %s's %s against %s's %s.",
	    pplayer->name, unit_type(punit)->name,
	    unit_owner(pdefender)->name, unit_type(pdefender)->name);

    punit->moved = TRUE;	/* We moved */

    notify_player_ex(unit_owner(pwinner), punit->x, punit->y,
		     E_UNIT_WIN_ATT,
		     _("Game: Your attacking %s succeeded"
		       " against %s's %s%s!"), unit_name(pwinner->type),
		     unit_owner(plooser)->name, unit_name(plooser->type),
		     get_location_str_at(unit_owner(pwinner), plooser->x,
					 plooser->y));
    kill_unit(pwinner, plooser);
               /* no longer pplayer - want better msgs -- Syela */
  }
  if (pwinner == punit && unit_flag(punit, F_MISSILE)) {
    wipe_unit(pwinner);
    return;
  }

  /* If attacker wins, and occupychance > 0, it might move in.  Don't move in
   * if there are enemy units in the tile (a fortress, city or air base with
   * multiple defenders and unstacked combat). Note that this could mean 
   * capturing (or destroying) a city. */

  if (pwinner == punit && myrand(100) < game.occupychance &&
      !is_non_allied_unit_tile(map_get_tile(def_x, def_y),
			       unit_owner(punit))) {

    /* Hack: make sure the unit has enough moves_left for the move to succeed,
       and adjust moves_left to afterward (if successful). */

    int old_moves = punit->moves_left;
    int full_moves = unit_move_rate(punit);
    punit->moves_left = full_moves;
    if (handle_unit_move_request(punit, def_x, def_y, FALSE, FALSE)) {
      punit->moves_left = old_moves - (full_moves - punit->moves_left);
      if (punit->moves_left < 0) {
	punit->moves_left = 0;
      }
    } else {
      punit->moves_left = old_moves;
    }
  }

  if (pwinner == punit && punit->activity != ACTIVITY_IDLE) {
    /* Ensure we remove ACTIVITY_GOTO here */
    set_unit_activity(punit, ACTIVITY_IDLE);
  }
  send_unit_info(NULL, pwinner);
}

/**************************************************************************
...
**************************************************************************/
static void how_to_declare_war(struct player *pplayer)
{
  notify_player_ex(pplayer, -1, -1, E_NOEVENT,
		   _("Game: Cancel treaty in the players dialog first (F3)."));
}

/**************************************************************************
...
**************************************************************************/
static bool can_unit_move_to_tile_with_notify(struct unit *punit, int dest_x,
					      int dest_y, bool igzoc)
{
  enum unit_move_result reason;
  int src_x = punit->x, src_y = punit->y;

  reason =
      test_unit_move_to_tile(punit->type, unit_owner(punit),
			     punit->activity, punit->connecting,
			     punit->x, punit->y, dest_x, dest_y, igzoc);
  if (reason == MR_OK)
    return TRUE;

  if (reason == MR_BAD_TYPE_FOR_CITY_TAKE_OVER) {
    const char *units_str = get_units_with_flag_string(F_MARINES);
    if (units_str) {
      notify_player_ex(unit_owner(punit), src_x, src_y,
		       E_NOEVENT, _("Game: Only %s can attack from sea."),
		       units_str);
      free((void *) units_str);
    } else {
      notify_player_ex(unit_owner(punit), src_x, src_y,
		       E_NOEVENT, _("Game: Cannot attack from sea."));
    }
  } else if (reason == MR_NO_WAR) {
    notify_player_ex(unit_owner(punit), src_x, src_y,
		     E_NOEVENT,
		     _("Game: Cannot attack unless you declare war first."));
  } else if (reason == MR_ZOC) {
    notify_player_ex(unit_owner(punit), src_x, src_y, E_NOEVENT,
		     _("Game: %s can only move into your own zone of control."),
		     unit_type(punit)->name);
  }
  return FALSE;
}

/**************************************************************************
  Will try to move to/attack the tile dest_x,dest_y.  Returns true if this
  could be done, false if it couldn't for some reason.
  
  'igzoc' means ignore ZOC rules - not necessary for igzoc units etc, but
  done in some special cases (moving barbarians out of initial hut).
  Should normally be FALSE.

  'move_diplomat_city' is another special case which should normally be
  FALSE.  If TRUE, try to move diplomat (or spy) into city (should be
  allied) instead of telling client to popup diplomat/spy dialog.

  FIXME: This function needs a good cleaning.
**************************************************************************/
bool handle_unit_move_request(struct unit *punit, int dest_x, int dest_y,
			     bool igzoc, bool move_diplomat_city)
{
  struct player *pplayer = unit_owner(punit);
  struct tile *pdesttile = map_get_tile(dest_x, dest_y);
  struct unit *pdefender = get_defender(punit, dest_x, dest_y);
  struct city *pcity = pdesttile->city;

  if (!is_normal_map_pos(dest_x, dest_y)) {
    return FALSE;
  }

  /* this occurs often during lag, and to the AI due to some quirks -- Syela */
  if (!is_tiles_adjacent(punit->x, punit->y, dest_x, dest_y)) {
    freelog(LOG_DEBUG, "tiles not adjacent in move request");
    return FALSE;
  }

  if (unit_flag(punit, F_TRADE_ROUTE) && pcity && pcity->owner != punit->owner
      && !pplayers_allied(city_owner(pcity), unit_owner(punit))
      && punit->homecity != 0) {
    struct packet_unit_request req;
    req.unit_id = punit->id;
    req.city_id = pcity->id;
    req.name[0] = '\0';
    return handle_unit_establish_trade(pplayer, &req);
  }

  /* Pop up a diplomat action dialog in the client.  If the AI has used
     a goto to send a diplomat to a target do not pop up a dialog in
     the client.  For allied cities, keep moving if move_diplomat_city
     tells us to, or if the unit is on goto and the city is not the
     final destination.
  */
  if (is_diplomat_unit(punit)
      && (is_non_allied_unit_tile(pdesttile, pplayer)
	  || is_non_allied_city_tile(pdesttile, pplayer)
	  || !move_diplomat_city)) {
    if (is_diplomat_action_available(punit, DIPLOMAT_ANY_ACTION,
				     dest_x, dest_y)) {
      struct packet_diplomat_action packet;

      if (pplayer->ai.control) {
	return FALSE;
      }

      /* If we didn't send_unit_info the client would sometimes think that
	 the diplomat didn't have any moves left and so don't pop up the box.
	 (We are in the middle of the unit restore cycle when doing goto's, and
	 the unit's movepoints have been restored, but we only send the unit
	 info at the end of the function.) */
      send_unit_info(pplayer, punit);

      /* if is_diplomat_action_available() then there must be a city or a unit */
      if (pcity) {
	packet.target_id = pcity->id;
      } else if (pdefender) {
	packet.target_id = pdefender->id;
      } else {
	die("Bug in unithand.c: no diplomat target.");
      }
      packet.diplomat_id = punit->id;
      packet.action_type = DIPLOMAT_CLIENT_POPUP_DIALOG;
      lsend_packet_diplomat_action(player_reply_dest(pplayer), &packet);
      return FALSE;
    } else if (!can_unit_move_to_tile(punit, dest_x, dest_y, igzoc)) {
      notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		       is_ocean(map_get_terrain(punit->x, punit->y))
		       ? _("Game: Unit must be on land to "
			   "perform diplomatic action.")
		       : _("Game: No diplomat action possible."));
      return FALSE;
    }
  }

  /*** Try to attack if there is an enemy unit on the target tile ***/
  if (pdefender
      && pplayers_at_war(unit_owner(punit), unit_owner(pdefender))) {
    if (pcity && !pplayers_at_war(city_owner(pcity), unit_owner(punit))) {
      notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		       _("Game: Can't attack %s's unit in the city of %s "
			 "because you are not at war with %s."),
		       unit_owner(pdefender)->name,
		       pcity->name,
		       city_owner(pcity)->name);
      how_to_declare_war(pplayer);
      return FALSE;
    }

    /* Tile must contain ONLY enemy units. */
    unit_list_iterate(pdesttile->units, aunit) {
      if (!pplayers_at_war(unit_owner(aunit), unit_owner(punit))) {
        notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		         _("Game: Can't attack %s's unit since it is "
			   "stacked with %s's unit(s) and you are not "
                           "at war with %s."), 
                         unit_owner(pdefender)->name,
                         unit_owner(aunit)->name, 
                         unit_owner(aunit)->name);
        how_to_declare_war(pplayer);
        return FALSE;
      }
    } unit_list_iterate_end;

    if (!can_unit_attack_unit_at_tile(punit, pdefender, dest_x , dest_y)) {
      notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
  		       _("Game: You can't attack there."));
      return FALSE;
    }
 
    if (punit->moves_left<=0)  {
      notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
 		       _("Game: This unit has no moves left."));
      return FALSE;
    }

    /* FIXME: This code will never activate for AI players, and for
     * human players the server-side goto implementation should be
     * obsoleted for client usage. So in time, remove the code below. */
    if (punit->activity == ACTIVITY_GOTO && 
        !same_pos(goto_dest_x(punit), goto_dest_y(punit), dest_x, dest_y)) {
      notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
 		       _("Game: %s aborted GOTO as there are units in the way."),
 		       unit_type(punit)->name);
      return FALSE;
    }
 
    handle_unit_attack_request(punit, pdefender);
    return TRUE;
  } /* End attack case */
 
  /* There are no players we are at war with at desttile. But if there
     is a unit we have a treaty!=alliance with we can't move there */
  pdefender = is_non_allied_unit_tile(pdesttile, unit_owner(punit));
  if (pdefender) {
    notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		     _("Game: No war declared against %s, cannot attack."),
		     unit_owner(pdefender)->name);
    how_to_declare_war(pplayer);
    return FALSE;
  }

  /* If there is a city it is empty.
     If not it would have been caught in the attack case. */
  if (pcity && !pplayers_allied(city_owner(pcity), unit_owner(punit))) {
    if (is_air_unit(punit) || !is_military_unit(punit) || is_sailing_unit(punit)) {
      notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		       _("Game: Only ground troops can take over "
			 "a city."));
      return FALSE;
    }

    if (!pplayers_at_war(city_owner(pcity), unit_owner(punit))) {
      notify_player_ex(pplayer, punit->x, punit->y, E_NOEVENT,
		       _("Game: No war declared against %s, cannot take "
			 "over city."), city_owner(pcity)->name);
      how_to_declare_war(pplayer);
      return FALSE;
    }
  }

  /******* ok now move the unit *******/
  if (can_unit_move_to_tile_with_notify(punit, dest_x, dest_y, igzoc)
      && try_move_unit(punit, dest_x, dest_y)) {
    int move_cost = map_move_cost(punit, dest_x, dest_y);
    /* The ai should assign the relevant units itself, but for now leave this */
    bool take_from_land = punit->activity == ACTIVITY_IDLE;

    (void) move_unit(punit, dest_x, dest_y, TRUE, take_from_land, move_cost);

    return TRUE;
  } else {
    return FALSE;
  }
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_help_build_wonder(struct player *pplayer, 
				   struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);
  struct city *pcity_dest = find_city_by_id(req->city_id);
  const char *text;

  if (!punit || !unit_flag(punit, F_HELP_WONDER) || !pcity_dest
      || !unit_can_help_build_wonder(punit, pcity_dest)) {
    return;
  }

  if (!is_tiles_adjacent(punit->x, punit->y, pcity_dest->x, pcity_dest->y)
      && !same_pos(punit->x, punit->y, pcity_dest->x, pcity_dest->y))
    return;

  if (!(same_pos(punit->x, punit->y, pcity_dest->x, pcity_dest->y)
	|| try_move_unit(punit, pcity_dest->x, pcity_dest->y)))
    return;

  /* we're there! */
  pcity_dest->shield_stock += unit_type(punit)->build_cost;
  pcity_dest->caravan_shields += unit_type(punit)->build_cost;

  conn_list_do_buffer(&pplayer->connections);

  if (build_points_left(pcity_dest) >= 0) {
    text = _("Game: Your %s helps build the %s in %s (%d remaining).");
  } else {
    text = _("Game: Your %s helps build the %s in %s (%d surplus).");
  }
  notify_player_ex(pplayer, pcity_dest->x, pcity_dest->y, E_NOEVENT,
		   text, /* Must match arguments below. */
		   unit_name(punit->type),
		   get_improvement_type(pcity_dest->currently_building)->name,
		   pcity_dest->name, 
		   abs(build_points_left(pcity_dest)));

  wipe_unit(punit);
  send_player_info(pplayer, pplayer);
  send_city_info(pplayer, pcity_dest);
  conn_list_do_unbuffer(&pplayer->connections);
}

/**************************************************************************
...
**************************************************************************/
bool handle_unit_establish_trade(struct player *pplayer, 
				 struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);
  struct city *pcity_out_of_home = NULL, *pcity_out_of_dest = NULL;
  struct city *pcity_homecity, *pcity_dest = find_city_by_id(req->city_id);
  int revenue, i;
  bool home_full = FALSE, dest_full = FALSE, can_establish;
  
  if (!punit || !pcity_dest || !unit_flag(punit, F_TRADE_ROUTE)) {
    return FALSE;
  }

  pcity_homecity = player_find_city_by_id(pplayer, punit->homecity);
  if (!pcity_homecity || pcity_homecity == pcity_dest) {
    return FALSE;
  }
    
  if (!is_tiles_adjacent(punit->x, punit->y, pcity_dest->x, pcity_dest->y)
      && !same_pos(punit->x, punit->y, pcity_dest->x, pcity_dest->y)) {
    return FALSE;
  }

  if (!(same_pos(punit->x, punit->y, pcity_dest->x, pcity_dest->y)
	|| try_move_unit(punit, pcity_dest->x, pcity_dest->y))) {
    return FALSE;
  }

  if (!can_cities_trade(pcity_homecity, pcity_dest)) {
    return FALSE;
  }
  
  /* This part of code works like can_establish_trade_route, except that
   * we actually do the action of making the trade route. */

  /* If we can't make a new trade route we can still get the trade bonus. */
  can_establish = !have_cities_trade_route(pcity_homecity, pcity_dest);
    
  if (can_establish) {
    home_full = (city_num_trade_routes(pcity_homecity) == NUM_TRADEROUTES);
    dest_full = (city_num_trade_routes(pcity_dest) == NUM_TRADEROUTES);
  }
  
  if (home_full || dest_full) {
    int slot, trade = trade_between_cities(pcity_homecity, pcity_dest);

    /* See if there's a trade route we can cancel at the home city. */
    if (home_full) {
      if (get_city_min_trade_route(pcity_homecity, &slot) < trade) {
	pcity_out_of_home = find_city_by_id(pcity_homecity->trade[slot]);
	assert(pcity_out_of_home != NULL);
      } else {
	notify_player_ex(pplayer, pcity_dest->x, pcity_dest->y, E_NOEVENT,
		     _("Game: Sorry, your %s cannot establish"
		       " a trade route here!"), unit_name(punit->type));
        notify_player_ex(pplayer, pcity_dest->x, pcity_dest->y, E_NOEVENT,
		       _("      The city of %s already has %d "
			 "better trade routes!"), pcity_homecity->name,
		       NUM_TRADEROUTES);
	can_establish = FALSE;
      }
    }
    
    /* See if there's a trade route we can cancel at the dest city. */
    if (can_establish && dest_full) {
      if (get_city_min_trade_route(pcity_dest, &slot) < trade) {
	pcity_out_of_dest = find_city_by_id(pcity_dest->trade[slot]);
	assert(pcity_out_of_dest != NULL);
      } else {
	notify_player_ex(pplayer, pcity_dest->x, pcity_dest->y, E_NOEVENT,
		     _("Game: Sorry, your %s cannot establish"
		       " a trade route here!"), unit_name(punit->type));
        notify_player_ex(pplayer, pcity_dest->x, pcity_dest->y, E_NOEVENT,
		       _("      The city of %s already has %d "
			 "better trade routes!"), pcity_dest->name,
		       NUM_TRADEROUTES);
	can_establish = FALSE;
      }
    }

    /* Now cancel the trade route from the home city. */
    if (can_establish && pcity_out_of_home) {
      remove_trade_route(pcity_homecity, pcity_out_of_home);
      notify_player_ex(city_owner(pcity_out_of_home),
		       pcity_out_of_home->x, pcity_out_of_home->y, E_NOEVENT,
		       _("Game: Sorry, %s has canceled the trade route "
			 "from %s to your city %s."),
		       city_owner(pcity_homecity)->name,
		       pcity_homecity->name, pcity_out_of_home->name);
    }

    /* And the same for the dest city. */
    if (can_establish && pcity_out_of_dest) {
      remove_trade_route(pcity_dest, pcity_out_of_dest);
      notify_player_ex(city_owner(pcity_out_of_dest),
		       pcity_out_of_dest->x, pcity_out_of_dest->y, E_NOEVENT,
		       _("Game: Sorry, %s has canceled the trade route "
			 "from %s to your city %s."),
		       city_owner(pcity_dest)->name,
		       pcity_dest->name, pcity_out_of_dest->name);
    }
  }
  
  revenue = get_caravan_enter_city_trade_bonus(pcity_homecity, pcity_dest);
  if (can_establish) {
    /* establish traderoute */
    for (i = 0; i < NUM_TRADEROUTES; i++) {
      if (pcity_homecity->trade[i] == 0) {
        pcity_homecity->trade[i] = pcity_dest->id;
        break;
      }
    }
    assert(i < NUM_TRADEROUTES);
  
    for (i = 0; i < NUM_TRADEROUTES; i++) {
      if (pcity_dest->trade[i] == 0) {
        pcity_dest->trade[i] = pcity_homecity->id;
        break;
      }
    }
    assert(i < NUM_TRADEROUTES);
  } else {
    /* enter marketplace */
    revenue = (revenue + 2) / 3;
  }
  
  conn_list_do_buffer(&pplayer->connections);
  notify_player_ex(pplayer, pcity_dest->x, pcity_dest->y, E_NOEVENT,
		   _("Game: Your %s from %s has arrived in %s,"
		     " and revenues amount to %d in gold and research."), 
		   unit_name(punit->type), pcity_homecity->name,
		   pcity_dest->name, revenue);
  wipe_unit(punit);
  pplayer->economic.gold += revenue;
  update_tech(pplayer, revenue);
  
  if (can_establish) {
    /* Refresh the cities. */
    city_refresh(pcity_homecity);
    city_refresh(pcity_dest);
    if (pcity_out_of_home) {
      city_refresh(pcity_out_of_home);
    }
    if (pcity_out_of_dest) {
      city_refresh(pcity_out_of_dest);
    }
  
    /* Notify the owners of the cities. */
    send_city_info(pplayer, pcity_homecity);
    send_city_info(city_owner(pcity_dest), pcity_dest);
    if(pcity_out_of_home) {
      send_city_info(city_owner(pcity_out_of_home), pcity_out_of_home);
    }
    if(pcity_out_of_dest) {
      send_city_info(city_owner(pcity_out_of_dest), pcity_out_of_dest);
    }

    /* Notify each player about the other cities so that they know about
     * the tile_trade value. */
    if (pplayer != city_owner(pcity_dest)) {
      send_city_info(city_owner(pcity_dest), pcity_homecity);
      send_city_info(pplayer, pcity_dest);
    }

    if (pcity_out_of_home) {
      if (city_owner(pcity_dest) != city_owner(pcity_out_of_home)) {
        send_city_info(city_owner(pcity_dest), pcity_out_of_home);
	 send_city_info(city_owner(pcity_out_of_home), pcity_dest);
      }
      if (pplayer != city_owner(pcity_out_of_home)) {
        send_city_info(pplayer, pcity_out_of_home);
	 send_city_info(city_owner(pcity_out_of_home), pcity_homecity);
      }
      if (pcity_out_of_dest && city_owner(pcity_out_of_home) !=
					city_owner(pcity_out_of_dest)) {
	 send_city_info(city_owner(pcity_out_of_home), pcity_out_of_dest);
      }
    }

    if (pcity_out_of_dest) {
      if (city_owner(pcity_dest) != city_owner(pcity_out_of_dest)) {
        send_city_info(city_owner(pcity_dest), pcity_out_of_dest);
	 send_city_info(city_owner(pcity_out_of_dest), pcity_dest);
      }
      if (pplayer != city_owner(pcity_out_of_dest)) {
	 send_city_info(pplayer, pcity_out_of_dest);
	 send_city_info(city_owner(pcity_out_of_dest), pcity_homecity);
      }
      if (pcity_out_of_home && city_owner(pcity_out_of_home) !=
					city_owner(pcity_out_of_dest)) {
	 send_city_info(city_owner(pcity_out_of_dest), pcity_out_of_home);
      }
    }
  }
  
  send_player_info(pplayer, pplayer);
  conn_list_do_unbuffer(&pplayer->connections);
  return TRUE;
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_auto_request(struct player *pplayer, 
			      struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);

  if (!punit || !can_unit_do_auto(punit))
    return;

  punit->ai.control = TRUE;
  send_unit_info(pplayer, punit);
}

/**************************************************************************
...
**************************************************************************/
static void handle_unit_activity_dependencies(struct unit *punit,
					      enum unit_activity old_activity,
					      int old_target)
{
  switch (punit->activity) {
  case ACTIVITY_IDLE:
    switch (old_activity) {
    case ACTIVITY_PILLAGE: 
      {
        enum tile_special_type prereq =
	  map_get_infrastructure_prerequisite(old_target);
        if (prereq != S_NO_SPECIAL) {
          unit_list_iterate (map_get_tile(punit->x, punit->y)->units, punit2)
            if ((punit2->activity == ACTIVITY_PILLAGE) &&
                (punit2->activity_target == prereq)) {
              set_unit_activity(punit2, ACTIVITY_IDLE);
              send_unit_info(NULL, punit2);
            }
          unit_list_iterate_end;
        }
        break;
      }
    case ACTIVITY_EXPLORE:
      /* Restore unit's control status */
      punit->ai.control = FALSE;
      break;
    default: 
      ; /* do nothing */
    }
    break;
  case ACTIVITY_EXPLORE:
    punit->ai.control = TRUE;
    set_unit_activity(punit, ACTIVITY_EXPLORE);
    send_unit_info(NULL, punit);
    break;
  default:
    /* do nothing */
    break;
  }
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_activity_request(struct unit *punit, 
				  enum unit_activity new_activity)
{
  if (can_unit_do_activity(punit, new_activity)) {
    enum unit_activity old_activity = punit->activity;
    enum tile_special_type old_target = punit->activity_target;
    set_unit_activity(punit, new_activity);
    if (punit->pgr) {
      free(punit->pgr->pos);
      free(punit->pgr);
      punit->pgr = NULL;
    }
    send_unit_info(NULL, punit);
    handle_unit_activity_dependencies(punit, old_activity, old_target);
  }
}

/**************************************************************************
...
**************************************************************************/
static void handle_unit_activity_request_targeted(struct unit *punit,
						  enum unit_activity
						  new_activity,
						  enum tile_special_type
						  new_target)
{
  if (can_unit_do_activity_targeted(punit, new_activity, new_target)) {
    enum unit_activity old_activity = punit->activity;
    enum tile_special_type old_target = punit->activity_target;
    set_unit_activity_targeted(punit, new_activity, new_target);

    if (punit->pgr) {
      free(punit->pgr->pos);
      free(punit->pgr);
      punit->pgr = NULL;
    }

    send_unit_info_to_onlookers(NULL, punit, punit->x, punit->y, FALSE);

    handle_unit_activity_dependencies(punit, old_activity, old_target);
  }
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_unload_request(struct player *pplayer, 
				struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);

  if (!punit) {
    return;
  }

  unit_list_iterate(map_get_tile(punit->x, punit->y)->units, punit2) {
    if (punit != punit2 && punit2->activity == ACTIVITY_SENTRY) {
      bool wakeup = FALSE;

      if (is_ground_units_transport(punit)) {
	if (is_ground_unit(punit2))
	  wakeup = TRUE;
      }

      if (unit_flag(punit, F_MISSILE_CARRIER)) {
	if (unit_flag(punit2, F_MISSILE))
	  wakeup = TRUE;
      }

      if (unit_flag(punit, F_CARRIER)) {
	if (is_air_unit(punit2))
	  wakeup = TRUE;
      }

      if (wakeup) {
	set_unit_activity(punit2, ACTIVITY_IDLE);
	send_unit_info(NULL, punit2);
      }
    }
  } unit_list_iterate_end;
}

/**************************************************************************
Explode nuclear at a tile without enemy units
**************************************************************************/
void handle_unit_nuke(struct player *pplayer, 
                     struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);

  if (!punit) {
    return;
  }
  handle_unit_attack_request(punit, punit);
}

/**************************************************************************
...
**************************************************************************/
void handle_unit_paradrop_to(struct player *pplayer, 
			     struct packet_unit_request *req)
{
  struct unit *punit = player_find_unit_by_id(pplayer, req->unit_id);
  
  if (!punit) {
    return;
  }

  (void) do_paradrop(punit, req->x, req->y);
}



/**************************************************************************
Sanity checks on the goto route.
**************************************************************************/
static bool check_route(struct player *pplayer, struct packet_goto_route *packet)
{
  struct unit *punit = player_find_unit_by_id(pplayer, packet->unit_id);

  if (!punit) {
    return FALSE;
  }

  if (packet->first_index != 0) {
    freelog(LOG_ERROR, "Bad route packet, first_index is %d!",
	    packet->first_index);
    return FALSE;
  }
  if (packet->last_index != packet->length - 1) {
    freelog(LOG_ERROR, "bad route, li: %d != l-1: %d",
	    packet->last_index, packet->length - 1);
    return FALSE;
  }

  return TRUE;
}

/**************************************************************************
Receives goto route packages.
**************************************************************************/
static void handle_route(struct player *pplayer, struct packet_goto_route *packet)
{
  struct goto_route *pgr = NULL;
  struct unit *punit = player_find_unit_by_id(pplayer, packet->unit_id);

  if (punit->pgr) {
    free(punit->pgr->pos);
    free(punit->pgr);
  }

  pgr = fc_malloc(sizeof(struct goto_route));

  pgr->pos = packet->pos; /* here goes the malloced packet->pos */
  pgr->first_index = 0;
  pgr->length = packet->length;
  pgr->last_index = packet->length-1; /* index magic... */

  punit->pgr = pgr;

#ifdef DEBUG
  {
    int i = pgr->first_index;
    freelog(LOG_DEBUG, "first:%d, last:%d, length:%d",
	   pgr->first_index, pgr->last_index, pgr->length);
    for (; i < pgr->last_index;i++)
      freelog(LOG_DEBUG, "%d,%d", pgr->pos[i].x, pgr->pos[i].y);
  }
#endif

  if (punit->activity == ACTIVITY_GOTO) {
    set_goto_dest(punit, pgr->pos[pgr->last_index-1].x,
		  pgr->pos[pgr->last_index-1].y);
    send_unit_info(pplayer, punit);
  }

  assign_units_to_transporter(punit, TRUE);
  (void) goto_route_execute(punit);
}

/**************************************************************************
Receives goto route packages.
**************************************************************************/
void handle_goto_route(struct player *pplayer, struct packet_goto_route *packet)
{
  struct unit *punit = player_find_unit_by_id(pplayer, packet->unit_id);

  if (!check_route(pplayer, packet))
    return;

  handle_unit_activity_request(punit, ACTIVITY_GOTO);
  handle_route(pplayer, packet);
}

/**************************************************************************
Receives patrol route packages.
**************************************************************************/
void handle_patrol_route(struct player *pplayer, struct packet_goto_route *packet)
{
  struct unit *punit = player_find_unit_by_id(pplayer, packet->unit_id);

  if (!check_route(pplayer, packet)) {
    free(packet->pos);
    packet->pos = NULL;
    return;
  }

  handle_unit_activity_request(punit, ACTIVITY_PATROL);
  handle_route(pplayer, packet);
}
