/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/
#ifndef FC__IMPROVEMENT_H
#define FC__IMPROVEMENT_H

/* City Improvements, including Wonders.  (Alternatively "Buildings".) */

#include "shared.h"		/* MAX_LEN_NAME */
#include "tech.h"		/* Tech_Type_id */
#include "terrain.h"		/* enum tile_terrain_type etc */
#include "unittype.h"		/* Unit_Class_id, Unit_Type_id */

struct player;
struct map_position;


/* Improvement types.
 * improvement types are loaded from the buildings.ruleset file.) */
typedef int Impr_Type_id;

/* Improvement status (for cities' lists of improvements)
 * An enum or bitfield would be neater here, but we use a typedef for
 * a) less memory usage and b) compatibility with old behaviour */
typedef unsigned char Impr_Status;
#define I_NONE       0   /* Improvement not built */
#define I_ACTIVE     1   /* Improvement built, and having its effect */
#define I_OBSOLETE   2   /* Built, but obsoleted by a tech */
#define I_REDUNDANT  3   /* Built, but replaced by wonder/other building */


/* FIXME: Remove this define when there is per-file need for this enum. */
#define OLD_IMPR_TYPE_ENUM

/* FIXME: Remove this enum and the ifdef/endif when gen-impr implemented. */
#ifdef OLD_IMPR_TYPE_ENUM
enum improvement_type_id {
  B_AIRPORT=0, B_AQUEDUCT, B_BANK, B_BARRACKS, B_BARRACKS2, B_BARRACKS3, 
  B_CATHEDRAL, B_orCITY, B_COASTAL, B_COLOSSEUM, B_COURTHOUSE,  B_FACTORY, 
  B_GRANARY, B_HARBOUR, B_HYDRO, B_LIBRARY, B_MARKETPLACE, B_MASS, B_MFG, 
  B_NUCLEAR, B_OFFSHORE, B_orPALACE, B_POLICE, B_PORT, B_POWER,
  B_RECYCLING, B_RESEARCH, B_SAM, B_SDI, B_SEWER, B_SOLAR, B_SCOMP, 
  B_SMODULE, B_SSTRUCTURAL, B_STOCK, B_SUPERHIGHWAYS, B_SUPERMARKET, B_TEMPLE,
  B_UNIVERSITY,  
  
  B_APOLLO, B_ASMITHS, B_COLLOSSUS, B_COPERNICUS, B_CURE, B_DARWIN, B_EIFFEL,
  B_GREAT, B_WALL, B_HANGING, B_HOOVER, B_ISAAC, B_BACH, B_RICHARDS, 
  B_LEONARDO, B_LIGHTHOUSE, B_MAGELLAN, B_MANHATTEN, B_MARCO, B_MICHELANGELO, 
  B_ORACLE, B_PYRAMIDS, B_SETI, B_SHAKESPEARE, B_LIBERTY, B_SUNTZU, 
  B_UNITED, B_WOMENS,
  B_CAPITAL, B_LAST_ENUM
};
#endif

/* Some code checks explicitly for City Walls or Palace - as a temporary
   fix, set these to the indices of improvements which provide the
   EFT_UNIT_DEFEND (aff_unit=Land) and EFT_CAPITAL_CITY effects */
extern int B_CITY, B_PALACE;

/* B_LAST is a value which is guaranteed to be larger than all
 * actual Impr_Type_id values.  It is used as a flag value;
 * it can also be used for fixed allocations to ensure ability
 * to hold full number of improvement types.  */
#define B_LAST MAX_NUM_ITEMS

/* Range of equivalence (used in equiv_range fields)
 * These must correspond to impr_range_names[] in improvement.c. */
enum impr_range {
  IR_NONE,
  IR_CITY,
  IR_ISLAND,
  IR_PLAYER,
  IR_WORLD,
  IR_LAST      /* keep this last */
};

#define B_NATION (B_LAST + 1)
#define B_GOV    (B_LAST + 2)

/* Range of effects (used in equiv_range and effect.range fields)
 * These must correspond to effect_range_names[] in improvement.c. */
enum effect_range {
  EFR_NONE,
  EFR_LOCAL,
  EFR_CITY,
  EFR_ISLAND,
  EFR_PLAYER,
  EFR_WORLD,
  EFR_LAST	/* keep this last */
};

/* Type of effects. (Used in effect.type field)
 * These must correspond to effect_type_names[] in improvement.c. */
enum effect_type {
  EFT_ADV_PARASITE,
  EFT_AIRLIFT,
  EFT_ANY_GOVERNMENT,
  EFT_BARB_ATTACK,
  EFT_BARB_DEFEND,
  EFT_CAPITAL_CITY,
  EFT_CAPITAL_EXISTS,
  EFT_ENABLE_NUKE,
  EFT_ENABLE_SPACE,
  EFT_ENEMY_PEACEFUL,
  EFT_FOOD_ADD_TILE,
  EFT_FOOD_BONUS,
  EFT_FOOD_INC_TILE,
  EFT_FOOD_PER_TILE,
  EFT_GIVE_IMM_ADV,
  EFT_GROWTH_FOOD,
  EFT_HAVE_EMBASSIES,
  EFT_IMPROVE_REP,
  EFT_LUXURY_BONUS,
  EFT_LUXURY_PCT,
  EFT_MAKE_CONTENT,
  EFT_MAKE_CONTENT_MIL,
  EFT_MAKE_CONTENT_PCT,
  EFT_MAKE_HAPPY,
  EFT_MAY_DECLARE_WAR,
  EFT_NO_ANARCHY,
  EFT_NO_SINK_DEEP,
  EFT_NUKE_PROOF,
  EFT_POLLU_ADJ,
  EFT_POLLU_POP_ADJ,
  EFT_POLLU_PROD_ADJ,
  EFT_POLLU_PCT,
  EFT_POLLU_POP_PCT,
  EFT_POLLU_PROD_PCT,
  EFT_PROD_ADD_TILE,
  EFT_PROD_BONUS,
  EFT_PROD_INC_TILE,
  EFT_PROD_PER_TILE,
  EFT_PROD_TO_GOLD,
  EFT_CORRUPT_ADJ,
  EFT_CORRUPT_PCT,
  EFT_REVEAL_CITIES,
  EFT_REVEAL_MAP,
  EFT_REVOLT_DIST_ADJ,
  EFT_SCIENCE_BONUS,
  EFT_SCIENCE_PCT,
  EFT_SIZE_UNLIMIT,
  EFT_SLOW_NUKE_WINTER,
  EFT_SLOW_GLOBAL_WARM,
  EFT_SPACE_PART,
  EFT_SPY_RESISTANT,
  EFT_TAX_BONUS,
  EFT_TAX_PCT,
  EFT_TRADE_ADD_TILE,
  EFT_TRADE_BONUS,
  EFT_TRADE_INC_TILE,
  EFT_TRADE_PER_TILE,
  EFT_TRADE_ROUTE_PCT,
  EFT_UNIT_DEFEND,
  EFT_UNIT_MOVE,
  EFT_UNIT_NO_LOSE_POP,
  EFT_UNIT_RECOVER,
  EFT_UNIT_REPAIR,
  EFT_UNIT_VET_COMBAT,
  EFT_UNIT_VETERAN,
  EFT_UPGRADE_UNITS,
  EFT_UPGRADE_ONE_STEP,
  EFT_UPGRADE_ONE_LEAP,
  EFT_UPGRADE_ALL_STEP,
  EFT_UPGRADE_ALL_LEAP,
  EFT_UPKEEP_FREE,
  EFT_FOOD_PCT,
  EFT_PROD_PCT,
  EFT_REVOLT_DIST_PCT,
  EFT_TRADE_PCT,
  EFT_MAKE_CONTENT_MIL_PER,
  EFT_FORCE_CONTENT,
  EFT_FORCE_CONTENT_PCT,
  EFT_FORCE_HAPPY,
  EFT_UNIT_ATTACK,
  EFT_UNIT_ATTACK_FIREPOWER,
  EFT_UNIT_DEFEND_FIREPOWER,
  EFT_UNIT_COST,
  EFT_BUILDING_COST,
  EFT_UNIT_COST_PCT,
  EFT_BUILDING_COST_PCT,
  EFT_LAST	/* keep this last */
};

/* An effect conferred by an improvement. */
struct impr_effect {
  enum effect_type type;
  enum effect_range range;
  int amount;
  int survives;			   /* 1 = effect survives wonder destruction */
  int outside;				/* 1 = effect influences units outside
					 * of cities, not just those in the
					 * center tile */
  Impr_Type_id cond_bldg;	   /* B_LAST = unconditional */
  int cond_gov;			   /* game.government_count = unconditional */
  int cond_govmod;		   /* game.num_gov_modifiers = unconditional */
  Tech_Type_id cond_adv;	   /* A_NONE = unconditional; A_LAST = never */
  enum effect_type cond_eff;	   /* EFT_LAST = unconditional */
  Unit_Class_id aff_unit;	   /* UCL_LAST = all */
  enum tile_terrain_type aff_terr; /* T_UNKNOWN = all; T_LAST = none */
  enum tile_special_type aff_spec; /* S_* bit mask of specials affected */
};

/* Status of a city's improvement effects
 * (a bitfield: bit 0 set = first effect active, etc.) */
typedef unsigned Eff_Status;

/* Maximum number of effects per improvement 
 * (this should not be more than the number of bits in the Eff_Status type) */
#define MAX_EFFECTS 16

/* Keeps track of which improvement effects are active in a city.  */
struct eff_city {
  Impr_Type_id impr;  /* The ID of the improvement that confers the effects;
		       * if B_LAST, then this instance is unused and ready
		       * to be freed (or replaced with a new improvement);
		       * if B_NATION or B_GOV, then the effects are not
		       * from an improvement at all, but are conferred by
		       * the nation or governement type */
  Eff_Status active;  /* Which of the actual impr_effect effects are active */
};

/* Copy of eff_city effect activity for effects with Player-, Island-,
 * and World- ranges */
struct eff_global {
  struct eff_city eff; /* Should be updated whenever the corresponding
			* structure in the improvement's home city is
			* modified. N.B. Keep this first in the structure
			* so that a (struct eff_city) cast works */
  int cityid;          /* ID of the city that owns the improvment (if -1,
			* then the effect has survived the city destruction,
			* and should therefore be placed in the savefile) */
  struct player *plr;  /* Player that owns whatever confers this effect */
};

/* Type of improvement. (Read from buildings.ruleset file.) */
struct impr_type {
  char name[MAX_LEN_NAME];
  char name_orig[MAX_LEN_NAME];		/* untranslated */
  char graphic_str[MAX_LEN_NAME];	/* city icon of improv. */
  char graphic_alt[MAX_LEN_NAME];	/* city icon of improv. */
  Tech_Type_id tech_req;		/* A_LAST = never; A_NONE = always */
  Impr_Type_id bldg_req;		/* B_LAST = none required */
  enum tile_terrain_type *terr_gate;	/* list; T_LAST terminated */
  enum tile_special_type *spec_gate;	/* list; S_NO_SPECIAL terminated */
  enum impr_range equiv_range;
  Impr_Type_id *equiv_dupl;		/* list; B_LAST terminated */
  Impr_Type_id *equiv_repl;		/* list; B_LAST terminated */
  Tech_Type_id obsolete_by;		/* A_LAST = never obsolete */
  bool is_wonder;
  int build_cost;
  int upkeep;
  int sabotage;
  struct impr_effect *effect;		/* list; .type==EFT_LAST terminated */
  int variant;			/* FIXME: remove when gen-impr obsoletes */
  struct Sprite *sprite;		/* icon of the improvement */
  char *helptext;
  char soundtag[MAX_LEN_NAME];
  char soundtag_alt[MAX_LEN_NAME];
};


extern struct impr_type improvement_types[B_LAST];

/* get 'struct ceff_vector' and related functions: */
#define SPECVEC_TAG ceff
#define SPECVEC_TYPE struct eff_city
#include "specvec.h"

/* get 'struct geff_vector' and related functions: */
#define SPECVEC_TAG geff
#define SPECVEC_TYPE struct eff_global
#include "specvec.h"

/* impr range id/string converters */
enum impr_range impr_range_from_str(const char *str);
const char *impr_range_name(enum impr_range id);

/* An iterator to look at the effects affecting a building/city/player. */
struct eff_iter {
  struct impr_effect *imeff;           /* The next active effect */
  struct city *pcity;
  struct player *pplayer;
  Impr_Type_id impr;
  struct ceff_vector *effs[EFR_LAST];
  enum effect_range range;
  int index;
  Eff_Status bitmask;
  Unit_Type_id utype;
  struct unit_classes aff_unit;
  enum tile_terrain_type aff_terr;
  enum tile_special_type aff_spec;
};

/* improvement effect functions */
enum effect_range effect_range_from_str(const char *str);
const char *effect_range_name(enum effect_range id);
enum effect_type effect_type_from_str(const char *str);
const char *effect_type_name(enum effect_type id);

void get_effect_vectors(struct ceff_vector *ceffs[],
			struct geff_vector *geffs[],
			Impr_Type_id impr, struct city *pcity);
void get_all_effect_vectors(struct ceff_vector *effs[EFR_LAST],
			    Impr_Type_id impr, struct city *pcity,
			    int cont, struct player *plr);
bool is_under_effect(Impr_Type_id impr, struct city *pcity,
		     struct player *pplayer, enum effect_type cond_eff);
bool is_effect_activated(Impr_Type_id impr, struct city *pcity,
			 struct player *pplayer, struct impr_effect *imeff,
			 bool *cond_eff);
struct ceff_vector *get_geff_parent(struct geff_vector *geff);
void update_global_effect(struct city *pcity, struct player *pplayer,
			  struct eff_city *effect);
struct eff_city *append_ceff(struct ceff_vector *x);
struct eff_global *append_geff(struct geff_vector *x);
void eff_iterator_impr_init(struct eff_iter *iter, Impr_Type_id impr,
                            struct city *pcity, struct player *pplayer);
void eff_iterator_unit_init(struct eff_iter *iter, Unit_Type_id utype,
    			    Unit_Type_id aff_utype,
                            struct player *pplayer, struct map_position *mp);
struct impr_effect *eff_iterator_next(struct eff_iter *iter);
struct impr_effect *eff_iterator_next_full(struct eff_iter *iter,
                                           struct unit_classes *aff_unit,
                                           enum tile_terrain_type aff_terr,
                                           enum tile_special_type aff_spec);
Impr_Type_id eff_iterator_get_improvement(struct eff_iter *iter);
int is_unit_terrain_affected(struct impr_effect *imeff,
    			     struct unit_classes *aff_unit,
                             enum tile_terrain_type aff_terr,
                             enum tile_special_type aff_spec);
bool is_city_affected(struct impr_effect *imeff, struct city *pcity);
bool is_effect_outside(struct impr_effect *imeff, struct city *pcity);

#define city_effects_iterate(EI_iter, pcity) \
  { \
    struct eff_iter EI_iter; \
    eff_iterator_impr_init(&EI_iter, B_LAST, pcity, city_owner(pcity)); \
    while (eff_iterator_next(&EI_iter)) {

#define city_effects_iterate_end \
    } \
  }

#define city_effects_iterate_full(EI_iter, pcity, uclass, terr, spec) \
  { \
    struct eff_iter EI_iter; \
    eff_iterator_impr_init(&EI_iter, B_LAST, pcity, city_owner(pcity)); \
    while (eff_iterator_next_full(&EI_iter, uclass, terr, spec)) {

#define city_effects_iterate_full_end \
    } \
  }

#define impr_effects_iterate(EI_iter, impr, pcity, pplayer) \
  { \
    struct eff_iter EI_iter; \
    eff_iterator_impr_init(&EI_iter, impr, pcity, pplayer); \
    while (eff_iterator_next(&EI_iter)) {

#define impr_effects_iterate_end \
    } \
  }

#define impr_effects_iterate_full(EI_iter, impr, pcity, pplayer, uclass, terr, spec) \
  { \
    struct eff_iter EI_iter; \
    eff_iterator_impr_init(&EI_iter, impr, pcity, pplayer); \
    while (eff_iterator_next_full(&EI_iter, uclass, terr, spec)) {

#define impr_effects_iterate_full_end \
    } \
  }

#define unit_effects_iterate(EI_iter, punit, aff_unit) \
  { \
    struct eff_iter EI_iter; \
    struct map_position EI_mappos; \
    EI_mappos.x = (punit)->x; EI_mappos.y = (punit)->y; \
    eff_iterator_unit_init(&iter, (punit)->type, (aff_unit)->type, unit_owner(punit), &EI_mappos); \
    while (eff_iterator_next(&EI_iter)) {

#define unit_effects_iterate_end \
    } \
  }

#define unittype_effects_iterate(EI_iter, utype, aff_utype, pplayer, mappos) \
  { \
    struct eff_iter EI_iter; \
    eff_iterator_unit_init(&iter, utype, aff_utype, pplayer, mappos); \
    while (eff_iterator_next(&EI_iter)) {

#define unittype_effects_iterate_end \
    } \
  }

/* improvement functions */
void improvements_free(void);
struct impr_type *get_improvement_type(Impr_Type_id id);
bool improvement_exists(Impr_Type_id id);
int improvement_value(Impr_Type_id id);
int improvement_value_adjusted(Impr_Type_id id, struct city *pcity);
bool is_wonder(Impr_Type_id id);
const char *get_improvement_name(Impr_Type_id id);

/* FIXME: remove improvement_variant() when gen-impr obsoletes */
int improvement_variant(Impr_Type_id id);  

bool improvement_obsolete(struct player *pplayer, Impr_Type_id id);
bool improvement_redundant(struct player *pplayer,struct city *pcity,
                          Impr_Type_id id, bool want_to_build);
bool wonder_obsolete(Impr_Type_id id);
bool is_wonder_useful(Impr_Type_id id);
Impr_Type_id find_improvement_by_name(const char *s);
void improvement_status_init(Impr_Status * improvements, size_t elements);
Impr_Type_id get_improvement_for_effect(struct player *pplayer,
                                        enum effect_type id,
                                        struct unit_classes *aff_unit);
struct impr_effect *get_base_effect(Impr_Type_id impr, struct player *pplayer);
const char *get_effect_cause_name(Impr_Type_id impr, struct player *pplayer);

/* player related improvement and unit functions */
bool could_player_eventually_build_improvement(struct player *p, 
                                               Impr_Type_id id);
bool can_player_build_improvement(struct player *p, Impr_Type_id id);

/* city related improvement functions */
void add_destroyed_improvement(struct player *p, Impr_Type_id id);
void mark_improvement(struct city *pcity,Impr_Type_id id,Impr_Status status);
struct geff_vector *get_eff_world(void);
struct geff_vector *get_eff_player(struct player *plr);
struct geff_vector *get_eff_island(int cont, struct player *plr);
struct ceff_vector *get_eff_city(struct city *pcity);
void remove_global_effects(Impr_Type_id impr, struct player *pplayer);
void add_global_effects(Impr_Type_id impr, struct player *pplayer);
void append_effects(struct impr_effect **dest, struct impr_effect *src);

void allot_island_improvs(void);
void improvements_update_obsolete(void);
void improvements_update_redundant(struct player *pplayer, struct city *pcity,
                                   int cont, enum impr_range range);

/* Iterates over all improvements. Creates a new variable names m_i
 * with type Impr_Type_id which holds the id of the current improvement. */
#define impr_type_iterate(m_i)                                                \
{                                                                             \
  Impr_Type_id m_i;                                                           \
  for (m_i = 0; m_i < game.num_impr_types; m_i++) {

#define impr_type_iterate_end                                                 \
  }                                                                           \
}

#endif  /* FC__IMPROVEMENT_H */
