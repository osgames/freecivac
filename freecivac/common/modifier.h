/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/
#ifndef FC__MODIFIER_H
#define FC__MODIFIER_H

#include "shared.h"

struct impr_effect;

/* Maximum number of modifier types */
#define MAX_MODS 16

struct modifier {
  char name[MAX_LEN_NAME];
  int type;
  int tech_requirement;
  int obsoleted_by;
  char *action;
  char *final_action;
  struct impr_effect *effect; /* list; .type==EFT_LAST terminated */
};
  
struct modifier_type {
  char name[MAX_LEN_NAME];
  int id;
  int tech_requirement;
  int obsoleted_by;
  bool must_use;
};

int modifier_type_from_str(const char *s,
    			   struct modifier_type *modifier_types,
			   int num_mod_types);
int find_modifier_by_name(const char *s,
    			  struct modifier *modifiers,
			  int num_mods);

#endif  /* FC__MODIFIER_H */
