/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <string.h>

#include "game.h"
#include "government.h"
#include "log.h"
#include "map.h"
#include "mem.h"
#include "shared.h" /* ARRAY_SIZE */
#include "support.h"
#include "tech.h"

#include "improvement.h"

int B_CITY, B_PALACE;

/* get 'struct ceff_vector' functions: */
#define SPECVEC_TAG ceff
#define SPECVEC_TYPE struct eff_city
#include "specvec_c.h"

/* get 'struct geff_vector' functions: */
#define SPECVEC_TAG geff
#define SPECVEC_TYPE struct eff_global
#include "specvec_c.h"

/* Names of impr ranges.
 * (These must correspond to enum impr_range_id in improvement.h.)
 * do not change these unless you know what you're doing! */
static const char *impr_range_names[] = {
  "None",
  "City",
  "Island",
  "Player",
  "World"
};

/**************************************************************************
All the city improvements:
Use get_improvement_type(id) to access the array.
The improvement_types array is now setup in:
   server/ruleset.c (for the server)
   client/packhand.c (for the client)
**************************************************************************/
struct impr_type improvement_types[B_LAST];

/* Names of effect ranges.
 * (These must correspond to enum effect_range_id in improvement.h.)
 */
static const char *effect_range_names[] = {
  "None",
  "Local",
  "City",
  "Island",
  "Player",
  "World"
};

/* Names of effect types.
 * (These must correspond to enum effect_type_id in improvement.h.)
 */
static const char *effect_type_names[] = {
  "Adv_Parasite",
  "Airlift",
  "Any_Government",
  "Barb_Attack",
  "Barb_Defend",
  "Capital_City",
  "Capital_Exists",
  "Enable_Nuke",
  "Enable_Space",
  "Enemy_Peaceful",
  "Food_Add_Tile",
  "Food_Bonus",
  "Food_Inc_Tile",
  "Food_Per_Tile",
  "Give_Imm_Adv",
  "Growth_Food",
  "Have_Embassies",
  "Improve_Rep",
  "Luxury_Bonus",
  "Luxury_Pct",
  "Make_Content",
  "Make_Content_Mil",
  "Make_Content_Pct",
  "Make_Happy",
  "May_Declare_War",
  "No_Anarchy",
  "No_Sink_Deep",
  "Nuke_Proof",
  "Pollu_Adj",
  "Pollu_Pop_Adj",
  "Pollu_Prod_Adj",
  "Pollu_Pct",
  "Pollu_Pop_Pct",
  "Pollu_Prod_Pct",
  "Prod_Add_Tile",
  "Prod_Bonus",
  "Prod_Inc_Tile",
  "Prod_Per_Tile",
  "Prod_To_Gold",
  "Corrupt_Adj",
  "Corrupt_Pct",
  "Reveal_Cities",
  "Reveal_Map",
  "Revolt_Dist_Adj",
  "Science_Bonus",
  "Science_Pct",
  "Size_Unlimit",
  "Slow_Nuke_Winter",
  "Slow_Global_Warm",
  "Space_Part",
  "Spy_Resistant",
  "Tax_Bonus",
  "Tax_Pct",
  "Trade_Add_Tile",
  "Trade_Bonus",
  "Trade_Inc_Tile",
  "Trade_Per_Tile",
  "Trade_Route_Pct",
  "Unit_Defend",
  "Unit_Move",
  "Unit_No_Lose_Pop",
  "Unit_Recover",
  "Unit_Repair",
  "Unit_Vet_Combat",
  "Unit_Veteran",
  "Upgrade_Units",
  "Upgrade_One_Step",
  "Upgrade_One_Leap",
  "Upgrade_All_Step",
  "Upgrade_All_Leap",
  "Upkeep_Free",
  "Food_Pct",
  "Prod_Pct",
  "Revolt_Dist_Pct",
  "Trade_Pct",
  "Make_Content_Mil_Per",
  "Force_Content",
  "Force_Content_Pct",
  "Force_Happy",
  "Unit_Attack",
  "Unit_Attack_Firepower",
  "Unit_Defend_Firepower",
  "Unit_Cost",
  "Building_Cost",
  "Unit_Cost_Pct",
  "Building_Cost_Pct"
};

/**************************************************************************
  Convert effect range names to enum; case insensitive;
  returns EFR_LAST if can't match.
**************************************************************************/
enum effect_range effect_range_from_str(const char *str)
{
  enum effect_range ret_id;

  assert(ARRAY_SIZE(effect_range_names) == EFR_LAST);

  for (ret_id = 0; ret_id < EFR_LAST; ret_id++) {
    if (0 == mystrcasecmp(effect_range_names[ret_id], str)) {
      return ret_id;
    }
  }

  return EFR_LAST;
}

/**************************************************************************
  Return effect range name; NULL if bad id.
**************************************************************************/
const char *effect_range_name(enum effect_range id)
{
  assert(ARRAY_SIZE(effect_range_names) == EFR_LAST);

  if (id < EFR_LAST) {
    return effect_range_names[id];
  } else {
    return NULL;
  }
}

/**************************************************************************
  Convert effect type names to enum; case insensitive;
  returns EFT_LAST if can't match.
**************************************************************************/
enum effect_type effect_type_from_str(const char *str)
{
  enum effect_type ret_id;

  assert(ARRAY_SIZE(effect_type_names) == EFT_LAST);

  for (ret_id = 0; ret_id < EFT_LAST; ret_id++) {
    if (0 == mystrcasecmp(effect_type_names[ret_id], str)) {
      return ret_id;
    }
  }

  return EFT_LAST;
}

/**************************************************************************
  Return effect type name; NULL if bad id.
**************************************************************************/
const char *effect_type_name(enum effect_type id)
{
  assert(ARRAY_SIZE(effect_type_names) == EFT_LAST);

  if (id < EFT_LAST) {
    return effect_type_names[id];
  } else {
    return NULL;
  }
}

/**************************************************************************
  Convert impr range names to enum; case insensitive;
  returns IR_LAST if can't match.
**************************************************************************/
enum impr_range impr_range_from_str(const char *str)
{
  enum impr_range ret_id;

  assert(ARRAY_SIZE(impr_range_names) == IR_LAST);

  for (ret_id = 0; ret_id < IR_LAST; ret_id++) {
    if (0 == mystrcasecmp(impr_range_names[ret_id], str)) {
      return ret_id;
    }
  }

  return IR_LAST;
}

/**************************************************************************
  Return impr range name; NULL if bad id.
**************************************************************************/
const char *impr_range_name(enum impr_range id)
{
  assert(ARRAY_SIZE(impr_range_names) == IR_LAST);

  if (id < IR_LAST) {
    return impr_range_names[id];
  } else {
    return NULL;
  }
}

/**************************************************************************
  Frees the memory associated with this improvement.
**************************************************************************/
static void improvement_free(Impr_Type_id id)
{
  struct impr_type *p = get_improvement_type(id);

  free(p->terr_gate);
  p->terr_gate = NULL;

  free(p->spec_gate);
  p->spec_gate = NULL;

  free(p->equiv_dupl);
  p->equiv_dupl = NULL;

  free(p->equiv_repl);
  p->equiv_repl = NULL;

  free(p->effect);
  p->effect = NULL;

  free(p->helptext);
  p->helptext = NULL;
}

/***************************************************************
 Frees the memory associated with all improvements.
***************************************************************/
void improvements_free()
{
  impr_type_iterate(impr) {
    improvement_free(impr);
  } impr_type_iterate_end;
}

/**************************************************************************
Returns 1 if the improvement_type "exists" in this game, 0 otherwise.
An improvement_type doesn't exist if one of:
- id is out of range;
- the improvement_type has been flagged as removed by setting its
  tech_req to A_LAST;
- it is a space part, and the spacerace is not enabled.
Arguably this should be called improvement_type_exists, but that's too long.
**************************************************************************/
bool improvement_exists(Impr_Type_id id)
{
  if (id<0 || id>=B_LAST || id>=game.num_impr_types)
    return FALSE;

  if ((id==B_SCOMP || id==B_SMODULE || id==B_SSTRUCTURAL)
      && !game.spacerace)
    return FALSE;

  return (improvement_types[id].tech_req!=A_LAST);
}

/**************************************************************************
...
**************************************************************************/
struct impr_type *get_improvement_type(Impr_Type_id id)
{
  return &improvement_types[id];
}

/**************************************************************************
...
**************************************************************************/
const char *get_improvement_name(Impr_Type_id id)
{
  return get_improvement_type(id)->name; 
}

/**************************************************************************
...
**************************************************************************/
int improvement_value(Impr_Type_id id)
{
  return (improvement_types[id].build_cost);
}

/**************************************************************************
  Returns the effective value of the improvement, after applying any
  cost modifiers.
**************************************************************************/
int improvement_value_adjusted(Impr_Type_id id, struct city *pcity)
{
  return improvement_value(id) * pcity->impr_cost_pct / 100;
}

/**************************************************************************
...
**************************************************************************/
bool is_wonder(Impr_Type_id id)
{
  return (improvement_types[id].is_wonder);
}

/**************************************************************************
Does a linear search of improvement_types[].name
Returns B_LAST if none match.
**************************************************************************/
Impr_Type_id find_improvement_by_name(const char *s)
{
  impr_type_iterate(i) {
    if (strcmp(improvement_types[i].name, s)==0)
      return i;
  } impr_type_iterate_end;

  return B_LAST;
}

/**************************************************************************
FIXME: remove when gen-impr obsoletes
**************************************************************************/
int improvement_variant(Impr_Type_id id)
{
  return improvement_types[id].variant;
}

/**************************************************************************
 Returns 1 if the improvement is obsolete, now also works for wonders
**************************************************************************/
bool improvement_obsolete(struct player *pplayer, Impr_Type_id id) 
{
  if (!tech_exists(improvement_types[id].obsolete_by)) {
    return FALSE;
  }

  if (improvement_types[id].is_wonder) {
    /* a wonder is obsolete, as soon as *any* player researched the
       obsolete tech */
   return game.global_advances[improvement_types[id].obsolete_by] != 0;
  }

  return (get_invention(pplayer, improvement_types[id].obsolete_by)
	  ==TECH_KNOWN);
}

/**************************************************************************
 Fills in lists of improvements at all impr_ranges that might affect the
 given city (owned by the given player)
**************************************************************************/
static void fill_ranges_improv_lists(Impr_Status *equiv_list[IR_LAST],
                                     struct city *pcity,
                                     struct player *pplayer)
{ 
  int i, cont = 0;

  for (i = IR_NONE; i < IR_LAST; i++) {
    equiv_list[i] = NULL;
  }

  if (pcity) {
    equiv_list[IR_CITY] = pcity->improvements;
    cont = map_get_continent(pcity->x, pcity->y);
  }

  if (pplayer) {
    equiv_list[IR_PLAYER] = pplayer->improvements;

    if (cont > 0 && pplayer->island_improv) {
      equiv_list[IR_ISLAND] =
                          &pplayer->island_improv[cont * game.num_impr_types];
    }
  }

  equiv_list[IR_WORLD] = game.improvements;
}

/**************************************************************************
 Checks whether the building is within the equiv_range of a building that
 replaces it
**************************************************************************/
bool improvement_redundant(struct player *pplayer,struct city *pcity,
                          Impr_Type_id id, bool want_to_build)
{
  int i;
  Impr_Status *equiv_list[IR_LAST];
  Impr_Type_id *ept;

  /* Make lists of improvements that affect this city */
  fill_ranges_improv_lists(equiv_list, pcity, pplayer);

  /* For every improvement named in equiv_dupl or equiv_repl, check for
     its presence in any of the lists (we check only for its presence, and
     assume that it has the "equiv" effect even if it itself is redundant) */
  for (ept = improvement_types[id].equiv_repl; ept && *ept != B_LAST; ept++) {
    for (i = IR_CITY; i < IR_LAST; i++) {
      if (equiv_list[i]) {
         Impr_Status stat = equiv_list[i][*ept];
         if (stat != I_NONE && stat != I_OBSOLETE) return TRUE;
      }
    }
  }

  /* equiv_dupl makes buildings redundant, but that shouldn't stop you
     from building them if you really want to */
  if (!want_to_build) {
    for (ept = improvement_types[id].equiv_dupl; ept && *ept != B_LAST; ept++) {
      for (i = IR_CITY; i < IR_LAST; i++) {
        if (equiv_list[i]) {
          Impr_Status stat = equiv_list[i][*ept];
          if (stat != I_NONE && stat != I_OBSOLETE) return TRUE;
        }
      }
    }
  }
  return FALSE;
}

/**************************************************************************
...
**************************************************************************/
bool wonder_obsolete(Impr_Type_id id)
{
  return improvement_obsolete(NULL, id);
}

/**************************************************************************
Barbarians don't get enough knowledges to be counted as normal players.
**************************************************************************/
bool is_wonder_useful(Impr_Type_id id)
{
  if ((id == B_GREAT) && (get_num_human_and_ai_players () < 3)) return FALSE;
  return TRUE;
}

/**************************************************************************
 Clears a list of improvements - sets them all to I_NONE
**************************************************************************/
void improvement_status_init(Impr_Status * improvements, size_t elements)
{
  /* 
   * Since this function is called with elements!=game.num_impr_types
   * impr_type_iterate can't used here.
   */
  Impr_Type_id i;

  for (i = 0; i < elements; i++) {
    improvements[i] = I_NONE;
  }
}

/**************************************************************************
  Whether player could build this improvement, assuming they had
  the tech req, and assuming a city with the right pre-reqs etc.
**************************************************************************/
bool could_player_eventually_build_improvement(struct player *p,
					      Impr_Type_id id)
{
  struct impr_type *impr;

  /* This also checks if tech req is Never */
  if (!improvement_exists(id))
    return FALSE;

  impr = get_improvement_type(id);

  if (impr->effect) {
    struct impr_effect *peffect = impr->effect;
    enum effect_type type;

    /* This if for a spaceship component is asked */
    while ((type = peffect->type) != EFT_LAST) {
      if (type == EFT_SPACE_PART) {
        if (p->spaceship.state >= SSHIP_LAUNCHED)
	  return FALSE;
	if (peffect->amount == 1 && p->spaceship.structurals >= NUM_SS_STRUCTURALS)
	  return FALSE;
	if (peffect->amount == 2 && p->spaceship.components >= NUM_SS_COMPONENTS)
	  return FALSE;
	if (peffect->amount == 3 && p->spaceship.modules >= NUM_SS_MODULES)
	  return FALSE;
      }
      peffect++;
    }
  }
  if (is_wonder(id)) {
    /* Can't build wonder if already built */
    if (game.global_wonders[id] != 0) return FALSE;
  } else {
    /* Can't build if obsolette */
    if (improvement_obsolete(p, id)) return FALSE;
  }
  return TRUE;
}

/**************************************************************************
...
**************************************************************************/
static bool could_player_build_improvement(struct player *p, Impr_Type_id id)
{
  if (!could_player_eventually_build_improvement(p, id))
    return FALSE;

  /* Make sure we have the tech /now/.*/
  if (get_invention(p, improvement_types[id].tech_req) == TECH_KNOWN)
    return TRUE;
  return FALSE;
}
  
/**************************************************************************
  Can a player build this improvement somewhere?  Ignores the fact that 
  player may not have a city with appropriate prereqs.
**************************************************************************/
bool can_player_build_improvement(struct player *p, Impr_Type_id id)
{
  if (!improvement_exists(id))
    return FALSE;
  if (!player_knows_improvement_tech(p,id))
    return FALSE;
  return(could_player_build_improvement(p, id));
}

/**************************************************************************
  Marks an improvment to the status
**************************************************************************/
void mark_improvement(struct city *pcity, Impr_Type_id id, Impr_Status status)
{
  enum impr_range range;
  Impr_Status *improvements = NULL, *equiv_list[IR_LAST];

  pcity->improvements[id] = status;
  range = improvement_types[id].equiv_range;

  /* Get the relevant improvement list */
  fill_ranges_improv_lists(equiv_list, pcity, city_owner(pcity));
  improvements = equiv_list[range];

  if (improvements) {
    /* And set the same status */
    improvements[id] = status;
  }
}

/**************************************************************************
  Rearranges the island-range effects to take account of continent number
  changes.
**************************************************************************/
static void allot_island_effects(struct player *pplayer)
{
  int cont;
  struct geff_vector *new_island_effects;

  new_island_effects = fc_calloc(map.num_continents + 1,
      				 sizeof(struct geff_vector));
  for (cont = 1; cont <= map.num_continents; cont++) {
    geff_vector_init(&new_island_effects[cont]);
  }

  for (cont = 1; cont <= map.old_num_continents; cont++) {
    int i;
    struct geff_vector *geff = get_eff_island(cont, pplayer);
    for (i = 0; i < geff_vector_size(geff); i++) {
      struct eff_global *eff = geff_vector_get(geff, i);
      if (eff->eff.impr != B_LAST && eff->cityid >= 0) {
        int newcont;
        struct eff_global *neweff;
        struct city *pcity = player_find_city_by_id(pplayer, eff->cityid);
        assert(pcity);
        newcont = map_get_continent(pcity->x, pcity->y);
        neweff = append_geff(&new_island_effects[newcont]);
        neweff->eff.impr = eff->eff.impr;
        neweff->eff.active = eff->eff.active;
        neweff->cityid = eff->cityid;
      }
    }
    geff_vector_free(geff);
  }
  if (map.old_num_continents > 0) {
    free(pplayer->island_effects);
  }
  pplayer->island_effects = new_island_effects;
}

/**************************************************************************
  Redimensions the lists of island-range improvements when number of
  continents changes. 
**************************************************************************/
void allot_island_improvs(void)
{
  int i;
  bool changed = FALSE;

  players_iterate(pplayer) {
    allot_island_effects(pplayer);
    pplayer->island_improv = fc_realloc(pplayer->island_improv,
                                        (map.num_continents + 1)
                                        * game.num_impr_types
                                        * sizeof(Impr_Status));
  
    /* We index into this array with the continent number, so don't use zero */
    for (i = 1; i <= map.num_continents; i++) {
      improvement_status_init(&pplayer->island_improv[i * game.num_impr_types],
                              game.num_impr_types);
    } 

    /* Fill the lists with existent improvements with Island equiv_range */
    city_list_iterate(pplayer->cities, pcity) {
      int cont = map_get_continent(pcity->x, pcity->y);
      Impr_Status *improvs = 
                           &pplayer->island_improv[cont * game.num_impr_types];

      built_impr_iterate(pcity, id) {
        if (improvement_types[id].equiv_range != IR_ISLAND) {
          continue;
        }
    
        changed = TRUE;
        improvs[id] = pcity->improvements[id];
      } built_impr_iterate_end;
    } city_list_iterate_end;
  } players_iterate_end;

  map.old_num_continents = map.num_continents;
  if (changed) {
    update_all_effects();
  }
}   

/**************************************************************************
  Update the obsolete status of all improvements.

  For all players since wonders are obsoleted when anybody discovers the
  obsolescence tech.
**************************************************************************/
void improvements_update_obsolete(void)
{   
  players_iterate(pplayer) {
    city_list_iterate(pplayer->cities, pcity) {
      built_impr_iterate(pcity, i) {
        if (improvement_obsolete(pplayer, i)) {
          freelog(LOG_DEBUG,"%s in %s is obsolete",
                  improvement_types[i].name, pcity->name);
          mark_improvement(pcity, i, I_OBSOLETE);
        }
      } built_impr_iterate_end;
    } city_list_iterate_end;
  } players_iterate_end;
}

/**************************************************************************
  Update the redundancy status of all improvements.

  This needs to be done: 
   o When an improvement is made obsolete (by tech discovery).
   o When an improvement is built.
   o When an improvement is destroyed.
   o When islands are reallotted: cities might be 'rearranged' into 
     equiv_range.

  We only check improvements within the equiv_range range. 

  N.B. Unlike effects, we do not have to do multiple iterations: an 
  improvement making another improvement redundant does not depend on 
  whether it itself it redundant or not. having been built is all that 
  counts.
**************************************************************************/
void improvements_update_redundant(struct player *pplayer, struct city *pcity,
                                   int cont, enum impr_range range)
{
#define CHECK_CITY_IMPR(_pcity)                                              \
{                                                                            \
  built_impr_iterate((_pcity), i) {                                          \
    if ((_pcity)->improvements[i] == I_OBSOLETE) {                           \
      continue;                                                              \
    }                                                                        \
                                                                             \
    if (improvement_redundant(city_owner(_pcity), (_pcity), i, FALSE)) {     \
      freelog(LOG_DEBUG,"%s in %s is redundant",                             \
              improvement_types[i].name, (_pcity)->name);                    \
      mark_improvement((_pcity), i, I_REDUNDANT);                            \
    } else {                                                                 \
      freelog(LOG_DEBUG,"%s in %s is active!",                               \
             improvement_types[i].name, (_pcity)->name);                     \
      mark_improvement((_pcity), i, I_ACTIVE);                               \
    }                                                                        \
  } built_impr_iterate_end;                                                  \
}

  switch (range) {
  case IR_NONE:
    break;
  case IR_CITY:
    assert(pcity);
    CHECK_CITY_IMPR(pcity);
    break;
  case IR_WORLD:
    players_iterate(plr) {
      city_list_iterate(plr->cities, pcity2) {
        CHECK_CITY_IMPR(pcity2);
      } city_list_iterate_end;
    } players_iterate_end;
    break;
  case IR_PLAYER:
    assert(pplayer);
    city_list_iterate(pplayer->cities, pcity2) {
      CHECK_CITY_IMPR(pcity2);
    } city_list_iterate_end;
    break;
  case IR_ISLAND:
    assert(cont > 0);
    city_list_iterate(pplayer->cities, pcity2) {
      if (map_get_continent(pcity2->x, pcity2->y) == cont) {
        CHECK_CITY_IMPR(pcity2);
      }
    } city_list_iterate_end;
    break;
  default:
    break;
  }

#undef CHECK_CITY_IMPR 
}

/**************************************************************************
...
**************************************************************************/
struct geff_vector *get_eff_world(void)
{
  return (&game.effects);
}
  
/**************************************************************************
...
**************************************************************************/
struct geff_vector *get_eff_player(struct player *plr)
{
  return (&plr->effects);
}
  
/**************************************************************************
...
**************************************************************************/
struct geff_vector *get_eff_island(int cont, struct player *plr)
{
  return (&plr->island_effects[cont]);
}
  
/**************************************************************************
...
**************************************************************************/
struct ceff_vector *get_eff_city(struct city *pcity)
{
  return (&pcity->effects);
}

/**************************************************************************
  Converts the given geff_vector into a ceff_vector (struct eff_global is
  a derived class of struct eff_city, and by the same token geff_vector
  is derived from ceff_vector). The returned vector functions exactly as
  the orginal geff_vector, but returns eff_city structures.
**************************************************************************/
struct ceff_vector *get_geff_parent(struct geff_vector *geff)
{
  return (struct ceff_vector *)geff;
}

/**************************************************************************
  Fills in the efflist pointer array with the eff_global lists that could
  be changed by the given improvement (in the given city) being built
  or destroyed.
**************************************************************************/
void get_effect_vectors(struct ceff_vector *ceffs[],
    			struct geff_vector *geffs[],
			Impr_Type_id impr, struct city *pcity)
{
  struct impr_effect *ie;
  int j, i, cont;
  bool effects[EFR_LAST];
  struct player *plr;

  assert(pcity && impr >= 0 && impr < game.num_impr_types);

  for (i = 0; i < EFR_LAST; i++) {
    effects[i] = FALSE;
  }

  if ((ie = improvement_types[impr].effect)) {
    for (; ie->type < EFT_LAST; ie++) {
      effects[ie->range] = TRUE;
    }
  }

  plr = city_owner(pcity);
  cont = map_get_continent(pcity->x, pcity->y);

  i = 0;
  for (j = 0; j < EFR_LAST; j++) {
    if (effects[j]) {
      switch(j) {
      case EFR_ISLAND:
	geffs[i++] = get_eff_island(cont, plr);
	break;
      case EFR_PLAYER:
	geffs[i++] = get_eff_player(plr);
	break;
      case EFR_WORLD:
	geffs[i++] = get_eff_world();
	break;
      default:
	break;
      }
    }
  }
  geffs[i++] = NULL;

  ceffs[0] = get_eff_city(pcity);
  ceffs[1] = NULL;
}

/**************************************************************************
  Updates the relevant global effect structures, to bring them into
  line with the current activity status of their parent city structure,
  which should be owned by the passed pcity and pplayer (either can be
  NULL for destroyed effects).
**************************************************************************/
void update_global_effect(struct city *pcity, struct player *pplayer,
			  struct eff_city *effect)
{
  struct geff_vector *effs[EFR_LAST];
  int i, j;

  for (i = 0; i < EFR_LAST; i++) {
    effs[i] = NULL;
  }

  if (pplayer) {
    effs[EFR_PLAYER] = get_eff_player(pplayer);
    if (pcity) {
      int cont = map_get_continent(pcity->x, pcity->y);
      effs[EFR_ISLAND] = get_eff_island(cont, pplayer);
    }
  }
  effs[EFR_WORLD] = get_eff_world();

  for (j = 0; j < EFR_LAST; j++) {
    if (effs[j]) {
      for (i = 0; i < geff_vector_size(effs[j]); i++) {
        struct eff_global *eff = geff_vector_get(effs[j], i);

	if ((!pcity || eff->cityid == pcity->id)
	    && (!pplayer || eff->plr == pplayer)
	    && eff->eff.impr == effect->impr) {
	  eff->eff.active = effect->active;
	  break;
        }
      }
    }
  }
}

/**************************************************************************
...
**************************************************************************/
struct eff_city *append_ceff(struct ceff_vector *x)
{
  int i, n;
  struct eff_city *eff;

  /* Try for an unused vector instance if possible. */
  n = ceff_vector_size(x);
  for (i = 0; i < n; i++) {
    eff = ceff_vector_get(x, i);
    if (eff->impr == B_LAST) {
      return eff;
    }
  }

  /* That didn't work, so add a new instance to the vector. */
  ceff_vector_reserve(x, n + 1);
  return ceff_vector_get(x, n);
}

/**************************************************************************
...
**************************************************************************/
struct eff_global *append_geff(struct geff_vector *x)
{
  int i, n;
  struct eff_global *eff;

  /* Try for an unused vector instance if possible. */
  n = geff_vector_size(x);
  for (i = 0; i < n; i++) {
    eff = geff_vector_get(x, i);
    if (eff->eff.impr == B_LAST) {
      return eff;
    }
  }

  /* That didn't work, so add a new instance to the vector. */
  geff_vector_reserve(x, n + 1);
  return geff_vector_get(x, n);
}

/**************************************************************************
  Fills in the efflist pointer array with the relevant ceff_vectors for
  the given improvement, in the given city, on the given island, and the
  given player. B_LAST, NULL, -1 and NULL respectively cause those ranges
  to be skipped (i.e. returned as NULL).
**************************************************************************/
void get_all_effect_vectors(struct ceff_vector *effs[EFR_LAST],
			    Impr_Type_id impr, struct city *pcity,
                            int cont, struct player *plr)
{
  int i;

  for (i = 0; i < EFR_LAST; i++) {
    effs[i] = NULL;
  }

  if (pcity) {
    effs[EFR_CITY] = get_eff_city(pcity);
    if (impr != B_LAST) {
      effs[EFR_LOCAL] = effs[EFR_CITY];
    }
  }

  if (plr) {
    effs[EFR_PLAYER] = get_geff_parent(get_eff_player(plr));
    if (cont != -1) {
      effs[EFR_ISLAND] = get_geff_parent(get_eff_island(cont, plr));
    }
  }

  effs[EFR_WORLD] = get_geff_parent(get_eff_world());
}

/**************************************************************************
  Returns TRUE if the given building (if not B_LAST), city or player
  are being affected by the given effect; the player should be the owner of
  the city (and will be filled in if city is specified and player is NULL)
**************************************************************************/
bool is_under_effect(Impr_Type_id impr, struct city *pcity,
		     struct player *pplayer, enum effect_type cond_eff)
{
 if (!pplayer && pcity) {
   pplayer = city_owner(pcity);
 }

 impr_effects_iterate(iter, impr, pcity, pplayer) {
   if (iter.imeff->type == cond_eff) {
     return TRUE;
   }
 } impr_effects_iterate_end;

  return FALSE;
}

/**************************************************************************
  Returns TRUE if the given effect (in the given city, owned by the given
  player) is activated by all of the necessary prerequisites;
  *cond_eff is set to TRUE if the effect could not be activated because it
  depends on another effect;
  pcity and/or pplayer can be NULL for destroyed effects.
  Note: the cond_bldg field, and the various aff_xxx fields, are not
        checked, as one effect may affect several cities, terrains, etc.
        Use the is_city_affected and is_unit_terrain_affected functions to
        determine if a so-called "active" effect really is.
**************************************************************************/
bool is_effect_activated(Impr_Type_id impr, struct city *pcity,
			 struct player *pplayer, struct impr_effect *imeff,
                         bool *cond_eff)
{
  if (imeff->cond_gov != game.government_count && pplayer
      && pplayer->government != imeff->cond_gov) {
    freelog(LOG_DEBUG, "Effect requires a government");
    return FALSE;
  }
  if (imeff->cond_govmod != game.num_gov_modifiers && pplayer
      && !government_has_modifier(get_gov_pplayer(pplayer),
				  imeff->cond_govmod)) {
    freelog(LOG_DEBUG, "Effect requires a government modifier");
    return FALSE;
  }
  if (pplayer && get_invention(pplayer, imeff->cond_adv) != TECH_KNOWN) {
    freelog(LOG_DEBUG, "Effect requires a future tech");
    return FALSE;
  }
  if (imeff->cond_eff != EFT_LAST
      && !is_under_effect(impr, pcity, pplayer, imeff->cond_eff)) {
    freelog(LOG_DEBUG, "Effect requires another effect");
    if (cond_eff) {
      *cond_eff = TRUE;
    }
    return FALSE;
  }
  return TRUE;
}

/**************************************************************************
  Sets an effect iterator to the start of the effect lists - common
  function used by other effect iterator initialisation functions
**************************************************************************/
static void eff_iterator_start(struct eff_iter *iter)
{
  iter->imeff = NULL;
  iter->range = 0;
  iter->index = 0;
  iter->bitmask = 1;
}

/**************************************************************************
  Initialises an effect iterator structure (a pointer to which should be
  passed in as the first argument). The iterator will return all improvement
  effects which affect the designated improvement in the given city owned
  by the given player (if impr is B_LAST, and/or pcity/pplayer is NULL,
  the respective ranges are skipped)
**************************************************************************/
void eff_iterator_impr_init(struct eff_iter *iter, Impr_Type_id impr,
                            struct city *pcity, struct player *pplayer)
{
  int cont = -1;

  assert(iter != NULL);

  if (pcity)
    cont = map_get_continent(pcity->x, pcity->y);

  get_all_effect_vectors(iter->effs, impr, pcity, cont, pplayer);

  iter->utype = U_LAST;
  iter->impr = impr;
  iter->pcity = pcity;
  iter->pplayer = pplayer;
  iter->aff_unit.move_type = LAST_MOVING;

  if (pcity) {
    iter->aff_terr = map_get_terrain(pcity->x, pcity->y);
    iter->aff_spec = map_get_special(pcity->x, pcity->y);
  } else {
    iter->aff_terr = T_UNKNOWN;
    iter->aff_spec = S_ALL;
  }

  eff_iterator_start(iter);
}

/**************************************************************************
  Similar to the above function, this initialises an effect iterator for
  considering effects that influence a unit that is owned by the given
  player and is at the given map coordinates (if pplayer and/or mp are NULL,
  the unit is considered unowned and/or unpositioned)
**************************************************************************/
void eff_iterator_unit_init(struct eff_iter *iter, Unit_Type_id utype,
    			    Unit_Type_id aff_utype,
                            struct player *pplayer, struct map_position *mp)
{
  int cont = -1;
  struct city *pcity = NULL;

  assert(iter != NULL);

  if (mp) {
    pcity = map_get_city(mp->x, mp->y);
    cont = map_get_continent(mp->x, mp->y);
  }

  get_all_effect_vectors(iter->effs, B_LAST, pcity, cont, pplayer);

  iter->utype = utype;
  iter->impr = B_LAST;
  iter->pcity = pcity;
  iter->pplayer = pplayer;
  get_unittype_classes(aff_utype, &iter->aff_unit);

  if (mp) {
    iter->aff_terr = map_get_terrain(mp->x, mp->y);
    iter->aff_spec = map_get_special(mp->x, mp->y);
  } else {
    iter->aff_terr = T_UNKNOWN;
    iter->aff_spec = S_ALL;
  }

  eff_iterator_start(iter);
}

struct impr_effect *get_base_effect(Impr_Type_id impr, struct player *pplayer)
{
  switch(impr) {
  case B_NATION:
    return get_nation_by_plr(pplayer)->effect;
  case B_GOV:
    return get_gov_pplayer(pplayer)->effect;
  default:
    break;
  }
  return improvement_types[impr].effect;
}

const char *get_effect_cause_name(Impr_Type_id impr, struct player *pplayer)
{
  switch(impr) {
  case B_NATION:
    return pplayer ? get_nation_name(pplayer->nation) : "unknown nation";
  case B_GOV:
    return pplayer ? get_government_name(pplayer->government)
                   : "unknown government";
  default:
    break;
  }
  return get_improvement_name(impr);
}

/**************************************************************************
  Given an initialised effect iterator, returns the next active effect.
  iter->imeff also points to this effect (the data is in the ruleset, so
  should NOT be freed when you're done with it). NULL is returned when no
  more effects can be found. Does _not_ consider the various aff_xxx fields.

  N.B. Don't go adding/removing effects while an iteration is in progress!
**************************************************************************/
static struct impr_effect *eff_iterator_next_internal(struct eff_iter *iter)
{
  struct ceff_vector *ceff;
  struct eff_city *effect;
  int efflen;

  assert(iter != NULL);

  /* Return immediately if the iterator is exhausted */
  if (!iter->imeff && iter->range != 0)
    return NULL;

  /* Loop over all valid ranges */
  for (; iter->range < EFR_LAST; iter->range++) {
    ceff = iter->effs[iter->range];
    /* We don't currently pre-activate unittype effects, so must check
     * each effect for activation */
    if (iter->range == EFR_LOCAL && iter->utype != U_LAST) {
      if (iter->imeff) {
        iter->imeff++;
      } else {
        iter->imeff = get_unit_type(iter->utype)->effect;
      }
      for (; iter->imeff && iter->imeff->type != EFT_LAST; iter->imeff++) {
        if (is_effect_activated(iter->impr, iter->pcity, iter->pplayer,
				iter->imeff, NULL)
	    && is_city_affected(iter->imeff, iter->pcity)) {
	  return iter->imeff;
	}
      }
      iter->imeff = NULL;
    } else if (ceff) {
      efflen = ceff_vector_size(ceff);
      for (; iter->index < efflen; iter->index++) {
        effect = ceff_vector_get(ceff, iter->index);
        if (effect->impr != B_LAST) {
          if (iter->imeff) {
            /* Make sure we don't return the same effect again */
            iter->imeff++; iter->bitmask <<= 1;
          } else {
            iter->imeff = get_base_effect(effect->impr, iter->pplayer);
            iter->bitmask = 1;
          }
          for (; iter->imeff && iter->imeff->type != EFT_LAST;
               iter->imeff++, iter->bitmask <<= 1) {
	    if (iter->imeff->range == iter->range) {
	      freelog(LOG_DEBUG, "Effect iteration: effect %s at range %s",
		      effect_type_name(iter->imeff->type),
		      effect_range_name(iter->range));
	    }

            if (effect->active & iter->bitmask
                && iter->imeff->range == iter->range
                && (iter->imeff->range != EFR_LOCAL
                    || effect->impr == iter->impr)
                && is_city_affected(iter->imeff, iter->pcity)
		&& is_effect_outside(iter->imeff, iter->pcity)) {
              return iter->imeff;
            }
          }
          /* Force reinitialisation of imeff next time round */
          iter->imeff = NULL;
        }
      }
    }
    /* Make sure we restart at zero for the next range out */
    iter->index = 0;
  }
  /* We didn't match any more effects, so signal that we're done */
  iter->imeff = NULL;
  return NULL;
}

/**************************************************************************
  Returns the next active effect that satisfies the aff_xxx fields set for
  the city/unit etc. when the iterator was initialised.
**************************************************************************/
struct impr_effect *eff_iterator_next(struct eff_iter *iter)
{
  struct impr_effect *imeff;

  do {
    imeff = eff_iterator_next_internal(iter);
    if (imeff && is_unit_terrain_affected(imeff, &iter->aff_unit,
                                          iter->aff_terr, iter->aff_spec)) {
      return imeff;
    }
  } while(imeff);
  return NULL;
}

/**************************************************************************
  Returns the next active effect that satisfies the various aff_xxx fields
  passed (use NULL, T_UNKNOWN and/or S_ALL respectively if you want
  any value - except "None" - to match)
**************************************************************************/
struct impr_effect *eff_iterator_next_full(struct eff_iter *iter,
                                           struct unit_classes *aff_unit,
                                           enum tile_terrain_type aff_terr,
                                           enum tile_special_type aff_spec)
{
  struct impr_effect *imeff;

  do {
    imeff = eff_iterator_next_internal(iter);
    if (imeff && is_unit_terrain_affected(imeff, aff_unit, aff_terr,
                                          aff_spec)) {
      return imeff;
    }
  } while(imeff);
  return NULL;
}

/**************************************************************************
  Returns the ID of the improvement that is conferring the last effect
  returned from the iterator, or B_LAST if no building is doing so
**************************************************************************/
Impr_Type_id eff_iterator_get_improvement(struct eff_iter *iter)
{
  struct ceff_vector *ceff;
  struct eff_city *effect;

  assert(iter != NULL);

  if (!iter->imeff)
    return B_LAST;

  ceff = iter->effs[iter->range];
  effect = ceff_vector_get(ceff, iter->index);

  return effect->impr;
}

/**************************************************************************
  Returns TRUE if the given effect should affect the given city (i.e. it
  has the building required for the effect to be active)
**************************************************************************/
bool is_city_affected(struct impr_effect *imeff, struct city *pcity)
{
  return (imeff->cond_bldg == B_LAST
          || (pcity && city_got_building(pcity, imeff->cond_bldg)));
}

/**************************************************************************
  Returns TRUE if the given effect is in a city, or if it's in the field
  and its "outside" flag is set, or if "outside" is ignored for this effect
**************************************************************************/
bool is_effect_outside(struct impr_effect *imeff, struct city *pcity)
{
  if (!pcity && !imeff->outside) {
    switch(imeff->type) {
    case EFT_UNIT_DEFEND:
    case EFT_UNIT_ATTACK:
    case EFT_UNIT_DEFEND_FIREPOWER:
    case EFT_UNIT_ATTACK_FIREPOWER:
    case EFT_UNIT_RECOVER:
    case EFT_UNIT_VET_COMBAT:
    case EFT_UPGRADE_ONE_STEP:
    case EFT_UPGRADE_ONE_LEAP:
    case EFT_UPGRADE_ALL_STEP:
    case EFT_UPGRADE_ALL_LEAP:
      return FALSE;
    default:
      break;
    }
  }
  return TRUE;
}

/**************************************************************************
  Returns TRUE if the given effect should affect the given
  unit/terrain/special, by checking the relevant aff_xxx fields
  (Pass in NULL, T_UNKNOWN, and S_ALL respectively if you don't care
  about that field; these values will match everything except "None".)
**************************************************************************/
int is_unit_terrain_affected(struct impr_effect *imeff,
    			     struct unit_classes *aff_unit,
                             enum tile_terrain_type aff_terr,
                             enum tile_special_type aff_spec)
{
  return is_unit_class_in_set(imeff->aff_unit, aff_unit)
         && (((aff_terr == T_UNKNOWN && imeff->aff_terr != T_LAST)
             || imeff->aff_terr == T_UNKNOWN || imeff->aff_terr == aff_terr)
             || imeff->aff_spec & aff_spec);
}

/**************************************************************************
  Returns the index of the improvement which provides the given effect
  (constrained to the given unit class and player, if not 0 and
  NULL respectively) or B_LAST if no suitable improvement could be found.
**************************************************************************/
Impr_Type_id get_improvement_for_effect(struct player *pplayer,
                                        enum effect_type id,
                                        struct unit_classes *aff_unit)
{
  int impr;
  struct impr_effect *eff;

  for (impr = 0; impr < game.num_impr_types; impr++) {
    for (eff = improvement_types[impr].effect;
         eff && eff->type != EFT_LAST; eff++) {
      if (eff->type == id
          && (!pplayer || can_player_build_improvement(pplayer, impr))
          && is_unit_terrain_affected(eff, aff_unit, T_UNKNOWN, S_ALL)) {
        return impr;
      }
    }
  }
  return B_LAST;
}

/**************************************************************************
  Adds the effects of a previously-destroyed improvement to the given
  players's destroyed effects vector (if player=NULL, then adds them to
  the global game destroyed effects vector instead).
**************************************************************************/
void add_destroyed_improvement(struct player *p, Impr_Type_id id)
{
  struct ceff_vector *desteff;
  struct eff_city *ceff;

  game.destroyed_owner[id] = p;
  freelog(LOG_DEBUG, "Adding destroyed improvement %s to %s",
          improvement_types[id].name, p ? p->name : "global list");

  desteff = (p ? &p->destroyed_effects : &game.destroyed_effects);
  ceff = append_ceff(desteff);
  ceff->impr = id;
  ceff->active = 0;
  add_global_effects(id, p);
}

static void get_global_effects(Impr_Type_id impr, struct player *pplayer,
    			       struct geff_vector *globeff[])
{
  struct impr_effect *imeff;

  globeff[0] = globeff[1] = NULL;
  for (imeff = get_base_effect(impr, pplayer);
       imeff && imeff->type != EFT_LAST; ++imeff) {
    if (imeff->range == EFR_WORLD) {
      globeff[1] = get_eff_world();
    } else if (imeff->range == EFR_PLAYER) {
      globeff[0] = get_eff_player(pplayer);
    }
  }
}

void remove_global_effects(Impr_Type_id impr, struct player *pplayer)
{
  struct geff_vector *globeff[2];
  int i, effind;

  get_global_effects(impr, pplayer, globeff);
  for (i = 0; i < 2; ++i) {
    if (globeff[i]) {
      for (effind = 0; effind < geff_vector_size(globeff[i]); ++effind) {
        struct eff_global *eff = geff_vector_get(globeff[i], effind);

	if (eff->cityid == -1 && eff->eff.impr == impr && eff->plr == pplayer) {
	  eff->eff.impr = B_LAST;
	}
      }
    }
  }
}

void add_global_effects(Impr_Type_id impr, struct player *pplayer)
{
  struct geff_vector *globeff[2];
  int i;

  get_global_effects(impr, pplayer, globeff);
  for (i = 0; i < 2; ++i) {
    if (globeff[i]) {
      struct eff_global *geff;

      geff = append_geff(globeff[i]);
      geff->eff.impr = impr;
      geff->eff.active = 0;
      geff->cityid = -1;
      geff->plr = pplayer;
    }
  }
}

void append_effects(struct impr_effect **dest, struct impr_effect *src)
{
  int numsrc = 0, numdest = 0, numeff;
  struct impr_effect *eff;

  for (eff = *dest; eff && eff->type != EFT_LAST; ++eff) {
    numdest++;
  }
  for (eff = src; eff && eff->type != EFT_LAST; ++eff) {
    numsrc++;
  }
  numeff = numdest + numsrc;

  if (numeff > MAX_EFFECTS) {
    freelog(LOG_FATAL, "MAX_EFFECTS exceeded");
    exit(EXIT_FAILURE);
  }

  if (numsrc) {
    *dest = fc_realloc(*dest, (numeff + 1) * sizeof(struct impr_effect));
    memcpy(*dest + numdest, src, (numsrc + 1) * sizeof(struct impr_effect));
  } else if (!*dest) {
    *dest = fc_malloc(sizeof(struct impr_effect));
    (*dest)->type = EFT_LAST;
  }
}
