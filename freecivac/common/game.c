/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "capstr.h"
#include "city.h"
#include "connection.h"
#include "fcintl.h"
#include "government.h"
#include "idex.h"
#include "log.h"
#include "map.h"
#include "mem.h"
#include "nation.h"
#include "player.h"
#include "shared.h"
#include "spaceship.h"
#include "support.h"
#include "tech.h"
#include "unit.h"

#include "game.h"

void dealloc_id(int id);
struct civ_game game;

/*
struct player_score {
  int happy;
  int content;
  int unhappy;
  int angry;
  int taxmen;
  int scientists;
  int elvis;
  int wonders;
  int techs;
  int landarea;
  int settledarea;
  int population;
  int cities;
  int units;
  int pollution;
  int literacy;
  int bnp;
  int mfg;
  int spaceship;
};
*/

/**************************************************************************
...
**************************************************************************/
int total_player_citizens(struct player *pplayer)
{
  return (pplayer->score.happy
	  +pplayer->score.content
	  +pplayer->score.unhappy
	  +pplayer->score.angry
	  +pplayer->score.scientists
	  +pplayer->score.elvis
	  +pplayer->score.taxmen);
}

/**************************************************************************
Count the # of thousand citizen in a civilisation.
**************************************************************************/
int civ_population(struct player *pplayer)
{
  int ppl=0;
  city_list_iterate(pplayer->cities, pcity)
    ppl+=city_population(pcity);
  city_list_iterate_end;
  return ppl;
}


/**************************************************************************
...
**************************************************************************/
struct city *game_find_city_by_name(const char *name)
{
  players_iterate(pplayer) {
    struct city *pcity = city_list_find_name(&pplayer->cities, name);

    if (pcity) {
      return pcity;
    }
  } players_iterate_end;

  return NULL;
}


/**************************************************************************
  Often used function to get a city pointer from a city ID.
  City may be any city in the game.  This now always uses fast idex
  method, instead of looking through all cities of all players.
**************************************************************************/
struct city *find_city_by_id(int id)
{
  return idex_lookup_city(id);
}


/**************************************************************************
  Find unit out of all units in game: now uses fast idex method,
  instead of looking through all units of all players.
**************************************************************************/
struct unit *find_unit_by_id(int id)
{
  return idex_lookup_unit(id);
}

/**************************************************************************
  In the server call wipe_unit(), and never this function directly.
**************************************************************************/
void game_remove_unit(struct unit *punit)
{
  struct city *pcity;

  freelog(LOG_DEBUG, "game_remove_unit %d", punit->id);
  freelog(LOG_DEBUG, "removing unit %d, %s %s (%d %d) hcity %d",
	  punit->id, get_nation_name(unit_owner(punit)->nation),
	  unit_name(punit->type), punit->x, punit->y, punit->homecity);

  pcity = player_find_city_by_id(unit_owner(punit), punit->homecity);
  if (pcity) {
    unit_list_unlink(&pcity->units_supported, punit);
  }

  if (pcity) {
    freelog(LOG_DEBUG, "home city %s, %s, (%d %d)", pcity->name,
	    get_nation_name(city_owner(pcity)->nation), pcity->x,
	    pcity->y);
  }

  unit_list_unlink(&map_get_tile(punit->x, punit->y)->units, punit);
  unit_list_unlink(&unit_owner(punit)->units, punit);

  idex_unregister_unit(punit);

  if (is_server) {
    dealloc_id(punit->id);
  }
  destroy_unit_virtual(punit);
}

/**************************************************************************
...
**************************************************************************/
void game_remove_city(struct city *pcity)
{
  int i;

  /* Update list of destroyed wonders properly */
  for (i = 0; i < B_LAST; i++) {
    if (game.global_wonders[i] == pcity->id) {
      game.global_wonders[i] = -1;
    }
  }

  freelog(LOG_DEBUG, "game_remove_city %d", pcity->id);
  freelog(LOG_DEBUG, "removing city %s, %s, (%d %d)", pcity->name,
	   get_nation_name(city_owner(pcity)->nation), pcity->x, pcity->y);

  ceff_vector_free(&pcity->effects);

  city_map_checked_iterate(pcity->x, pcity->y, x, y, mx, my) {
    set_worker_city(pcity, x, y, C_TILE_EMPTY);
  } city_map_checked_iterate_end;
  city_list_unlink(&city_owner(pcity)->cities, pcity);
  map_set_city(pcity->x, pcity->y, NULL);
  idex_unregister_city(pcity);
  remove_city_virtual(pcity);
}

/***************************************************************
...
***************************************************************/
void game_init(void)
{
  int i;
  game.is_new_game   = TRUE;
  game.globalwarming = 0;
  game.warminglevel  = 8;
  game.nuclearwinter = 0;
  game.coolinglevel  = 8;
  game.gold          = GAME_DEFAULT_GOLD;
  game.tech          = GAME_DEFAULT_TECHLEVEL;
  game.skill_level   = GAME_DEFAULT_SKILL_LEVEL;
  game.timeout       = GAME_DEFAULT_TIMEOUT;
  game.timeoutint    = GAME_DEFAULT_TIMEOUTINT;
  game.timeoutintinc = GAME_DEFAULT_TIMEOUTINTINC;
  game.timeoutinc    = GAME_DEFAULT_TIMEOUTINC;
  game.timeoutincmult= GAME_DEFAULT_TIMEOUTINCMULT;
  game.timeoutcounter= 1;
  game.tcptimeout    = GAME_DEFAULT_TCPTIMEOUT;
  game.netwait       = GAME_DEFAULT_NETWAIT;
  game.last_ping     = 0;
  game.pingtimeout   = GAME_DEFAULT_PINGTIMEOUT;
  game.pingtime      = GAME_DEFAULT_PINGTIME;
  game.end_year      = GAME_DEFAULT_END_YEAR;
  game.year          = GAME_START_YEAR;
  game.turn          = 0;
  game.spaceage_year = GAME_MAX_END_YEAR + 1;
  game.min_players   = GAME_DEFAULT_MIN_PLAYERS;
  game.max_players  = GAME_DEFAULT_MAX_PLAYERS;
  game.aifill      = GAME_DEFAULT_AIFILL;
  game.nplayers=0;
  game.researchcost = GAME_DEFAULT_RESEARCHCOST;
  game.diplcost    = GAME_DEFAULT_DIPLCOST;
  game.diplchance  = GAME_DEFAULT_DIPLCHANCE;
  game.freecost    = GAME_DEFAULT_FREECOST;
  game.conquercost = GAME_DEFAULT_CONQUERCOST;
  game.settlers    = GAME_DEFAULT_SETTLERS;
  game.explorer    = GAME_DEFAULT_EXPLORER;
  game.dispersion  = GAME_DEFAULT_DISPERSION;
  game.cityfactor  = GAME_DEFAULT_CITYFACTOR;
  game.citymindist = GAME_DEFAULT_CITYMINDIST;
  game.civilwarsize= GAME_DEFAULT_CIVILWARSIZE;
  game.contactturns= GAME_DEFAULT_CONTACTTURNS;
  game.rapturedelay= GAME_DEFAULT_RAPTUREDELAY;
  game.savepalace  = GAME_DEFAULT_SAVEPALACE;
  game.natural_city_names = GAME_DEFAULT_NATURALCITYNAMES;
  game.unhappysize = GAME_DEFAULT_UNHAPPYSIZE;
  game.angrycitizen= GAME_DEFAULT_ANGRYCITIZEN;
  game.foodbox     = GAME_DEFAULT_FOODBOX;
  game.aqueductloss= GAME_DEFAULT_AQUEDUCTLOSS;
  game.killcitizen = GAME_DEFAULT_KILLCITIZEN;
  game.scorelog    = GAME_DEFAULT_SCORELOG;
  game.techpenalty = GAME_DEFAULT_TECHPENALTY;
  game.civstyle    = GAME_DEFAULT_CIVSTYLE;
  game.razechance  = GAME_DEFAULT_RAZECHANCE;
  game.spacerace   = GAME_DEFAULT_SPACERACE;
  game.fogofwar    = GAME_DEFAULT_FOGOFWAR;
  game.fogofwar_old= game.fogofwar;
  game.borders     = GAME_DEFAULT_BORDERS;
  game.auto_ai_toggle = GAME_DEFAULT_AUTO_AI_TOGGLE;
  game.notradesize    = GAME_DEFAULT_NOTRADESIZE;
  game.fulltradesize  = GAME_DEFAULT_FULLTRADESIZE;
  game.barbarianrate  = GAME_DEFAULT_BARBARIANRATE;
  game.onsetbarbarian = GAME_DEFAULT_ONSETBARBARIAN;
  game.nbarbarians = 0;
  game.occupychance= GAME_DEFAULT_OCCUPYCHANCE;

  geff_vector_init(&game.effects);
  ceff_vector_init(&game.destroyed_effects);
  for (i = 0; i < B_LAST; i++) {
    game.destroyed_owner[i] = NULL;
  }

  game.heating     = 0;
  game.cooling     = 0;
  sz_strlcpy(game.save_name, GAME_DEFAULT_SAVE_NAME);
  game.save_nturns=10;
#ifdef HAVE_LIBZ
  game.save_compress_level = GAME_DEFAULT_COMPRESS_LEVEL;
#else
  game.save_compress_level = GAME_NO_COMPRESS_LEVEL;
#endif
  game.randseed=GAME_DEFAULT_RANDSEED;
  game.watchtower_vision=GAME_DEFAULT_WATCHTOWER_VISION;
  game.watchtower_extra_vision=GAME_DEFAULT_WATCHTOWER_EXTRA_VISION,
  game.allowed_city_names = GAME_DEFAULT_ALLOWED_CITY_NAMES;

  sz_strlcpy(game.rulesetdir, GAME_DEFAULT_RULESETDIR);

  game.num_unit_flags = F_LAST;
  game.num_unit_types = 0;
  game.num_impr_types = 0;
  game.num_tech_types = 0;

  game.government_count = 0;
  game.default_government = G_MAGIC;        /* flag */
  game.government_when_anarchy = G_MAGIC;   /* flag */
  game.ai_goal_government = G_MAGIC;        /* flag */

  sz_strlcpy(game.demography, GAME_DEFAULT_DEMOGRAPHY);
  sz_strlcpy(game.allow_connect, GAME_DEFAULT_ALLOW_CONNECT);

  game.save_options.save_random = TRUE;
  game.save_options.save_players = TRUE;
  game.save_options.save_known = TRUE;
  game.save_options.save_starts = TRUE;
  game.save_options.save_private_map = TRUE;

  game.load_options.load_random = TRUE;
  game.load_options.load_players = TRUE;
  game.load_options.load_known = TRUE;
  game.load_options.load_starts = TRUE;
  game.load_options.load_private_map = TRUE;
  game.load_options.load_settings = TRUE;

  init_our_capability();    
  map_init();
  idex_init();
  
  for(i=0; i<MAX_NUM_PLAYERS+MAX_NUM_BARBARIANS; i++)
    player_init(&game.players[i]);
  for (i=0; i<A_LAST; i++)      /* game.num_tech_types = 0 here */
    game.global_advances[i]=0;
  for (i=0; i<B_LAST; i++)      /* game.num_impr_types = 0 here */
    game.global_wonders[i]=0;
  game.conn_id = 0;
  game.player_idx=0;
  game.player_ptr=&game.players[0];
  terrain_control.river_help_text = NULL;
}

/***************************************************************
...
***************************************************************/
static void game_remove_all_players(void)
{
  players_iterate(pplayer) {
    game_remove_player(pplayer);
  } players_iterate_end;

  game.nplayers=0;
  game.nbarbarians=0;
}

/***************************************************************
  Frees all memory of the game.
***************************************************************/
void game_free(void)
{
  geff_vector_free(&game.effects);
  ceff_vector_free(&game.destroyed_effects);
  game_remove_all_players();
  map_free();
  idex_free();
  ruleset_data_free();
}

/***************************************************************
 Frees all memory which in objects which are read from a ruleset.
***************************************************************/
void ruleset_data_free()
{
  techs_free();
  governments_free();
  nations_free();
  unit_types_free();
  improvements_free();
  city_styles_free();
  tile_types_free();
}

/***************************************************************
...
***************************************************************/
void initialize_globals(void)
{
  players_iterate(plr) {
    city_list_iterate(plr->cities, pcity) {
      built_impr_iterate(pcity, i) {
	if (is_wonder(i))
	  game.global_wonders[i] = pcity->id;
      } built_impr_iterate_end;
    } city_list_iterate_end;
  } players_iterate_end;
}

/***************************************************************
  Returns the relevant calendar for the given year
***************************************************************/
const struct calendar *game_get_calendar(int year)
{
  const static struct calendar spaceage_cal = {
    0, N_("Space Age"), 0, 1, A_NONE, A_NONE
  };
  int calind;

  /* If a spaceship has been launched, then use the hard-coded 1 year/turn
   * "space age" calendar (for Civ compatibility). Otherwise, we use a
   * ruleset-defined calendar */
  if (game.spacerace && year >= game.spaceage_year) {
    return &spaceage_cal;
  }

  for (calind = 1; calind < game.num_calendars; ++calind) {
    /* Someone must have the req_tech to reach a calendar. */
    if (tech_exists(game.calendars[calind].req_tech)
        && !game.global_advances[game.calendars[calind].req_tech]) {
      break;
    }
    /* Either the first_year must be reached, or someone must have
     * the early_tech, to reach a calendar. */
    if (year < game.calendars[calind].first_year
        && (game.calendars[calind].early_tech == A_NONE
            || !game.global_advances[game.calendars[calind].early_tech])) {
      break;
    }
  }
  calind--;

  return &game.calendars[calind];
}

/***************************************************************
  Returns the relevant calendar for the current game year
***************************************************************/
const struct calendar *game_get_current_calendar(void)
{
  return game_get_calendar(game.year);
}

/***************************************************************
  Returns the next year in the game.
***************************************************************/
int game_next_year(int year)
{
  int oldyear = year;
  const struct calendar *cal = game_get_calendar(year);

  year += cal->turn_years;

  /* There is no such thing as "0AD", so skip a year if we passed it */
  if (oldyear < 0 && year >= 0) {
    year++;
  }

  return year;
}

/***************************************************************
  Advance the game year.
***************************************************************/
void game_advance_year(void)
{
  game.year = game_next_year(game.year);
  game.turn++;
}

/***************************************************************
...
***************************************************************/
void game_remove_player(struct player *pplayer)
{
  if (pplayer->attribute_block.data) {
    free(pplayer->attribute_block.data);
    pplayer->attribute_block.data = NULL;
  }

  geff_vector_free(&pplayer->effects);
  ceff_vector_free(&pplayer->destroyed_effects);
  if (pplayer->island_improv) {
    free(pplayer->island_improv);
    pplayer->island_improv = NULL;
  }

  unit_list_iterate(pplayer->units, punit) 
    game_remove_unit(punit);
  unit_list_iterate_end;

  city_list_iterate(pplayer->cities, pcity) 
    game_remove_city(pcity);
  city_list_iterate_end;

  if (is_barbarian(pplayer)) game.nbarbarians--;
}

/***************************************************************
...
***************************************************************/
void game_renumber_players(int plrno)
{
  int i;

  for (i = plrno; i < game.nplayers - 1; i++) {
    game.players[i]=game.players[i+1];
    game.players[i].player_no=i;
    conn_list_iterate(game.players[i].connections, pconn)
      pconn->player = &game.players[i];
    conn_list_iterate_end;
  }

  if(game.player_idx>plrno) {
    game.player_idx--;
    game.player_ptr=&game.players[game.player_idx];
  }

  game.nplayers--;

  /* a bit of cleanup to keep connections sane */
  conn_list_init(&game.players[game.nplayers].connections);
  game.players[game.nplayers].is_connected = FALSE;
  game.players[game.nplayers].ai.control = FALSE;
  sz_strlcpy(game.players[game.nplayers].name, ANON_PLAYER_NAME);
  sz_strlcpy(game.players[game.nplayers].username, ANON_USER_NAME);
}

/**************************************************************************
get_player() - Return player struct pointer corresponding to player_id.
               Eg: player_id = punit->owner, or pcity->owner
**************************************************************************/
struct player *get_player(int player_id)
{
    return &game.players[player_id];
}

/**************************************************************************
This function is used by is_wonder_useful to estimate if it is worthwhile
to build the great library.
**************************************************************************/
int get_num_human_and_ai_players(void)
{
  return game.nplayers-game.nbarbarians;
}

/***************************************************************
  For various data, copy eg .name to .name_orig and put
  translated version in .name
  (These could be in the separate modules, but since they are
  all almost the same, and all needed together, it seems a bit
  easier to just do them all here.)
***************************************************************/
void translate_data_names(void)
{
  int i;
  static const char too_long_msg[]
    = "Translated name is too long, truncating: %s";

#define name_strlcpy(dst, src) ((void) sz_loud_strlcpy(dst, src, too_long_msg))
  
  tech_type_iterate(tech_id) {
    struct advance *tthis = &advances[tech_id];
    sz_strlcpy(tthis->name_orig, tthis->name);
    name_strlcpy(tthis->name, Q_(tthis->name_orig));
  } tech_type_iterate_end;

  unit_type_iterate(i) {
    struct unit_type *tthis = &unit_types[i];
    sz_strlcpy(tthis->name_orig, tthis->name);
    name_strlcpy(tthis->name, Q_(tthis->name_orig));
  } unit_type_iterate_end;

  impr_type_iterate(i) {
    struct impr_type *tthis = &improvement_types[i];
    sz_strlcpy(tthis->name_orig, tthis->name);
    name_strlcpy(tthis->name, Q_(tthis->name_orig));
  } impr_type_iterate_end;

  for (i=T_FIRST; i<T_COUNT; i++) {
    struct tile_type *tthis = &tile_types[i];
    sz_strlcpy(tthis->terrain_name_orig, tthis->terrain_name);
    name_strlcpy(tthis->terrain_name,
		 (strcmp(tthis->terrain_name_orig, "") != 0) ?
			Q_(tthis->terrain_name_orig) : "");
    sz_strlcpy(tthis->special_1_name_orig, tthis->special_1_name);
    name_strlcpy(tthis->special_1_name,
		 (strcmp(tthis->special_1_name_orig, "") != 0) ?
			Q_(tthis->special_1_name_orig) : "");
    sz_strlcpy(tthis->special_2_name_orig, tthis->special_2_name);
    name_strlcpy(tthis->special_2_name,
		 (strcmp(tthis->special_2_name_orig, "") != 0) ?
			Q_(tthis->special_2_name_orig) : "");
  }
  government_iterate(tthis) {
    int j;

    sz_strlcpy(tthis->name_orig, tthis->name);
    name_strlcpy(tthis->name, Q_(tthis->name_orig));
    for(j=0; j<tthis->num_ruler_titles; j++) {
      struct ruler_title *that = &tthis->ruler_titles[j];
      sz_strlcpy(that->male_title_orig, that->male_title);
      name_strlcpy(that->male_title, Q_(that->male_title_orig));
      sz_strlcpy(that->female_title_orig, that->female_title);
      name_strlcpy(that->female_title, Q_(that->female_title_orig));
    }
  } government_iterate_end;
  for (i=0; i<game.nation_count; i++) {
    struct nation_type *tthis = get_nation_by_idx(i);
    sz_strlcpy(tthis->name_orig, tthis->name);
    name_strlcpy(tthis->name, Q_(tthis->name_orig));
    sz_strlcpy(tthis->name_plural_orig, tthis->name_plural);
    name_strlcpy(tthis->name_plural, Q_(tthis->name_plural_orig));
  }
  for (i=0; i<game.styles_count; i++) {
    struct citystyle *tthis = &city_styles[i];
    sz_strlcpy(tthis->name_orig, tthis->name);
    name_strlcpy(tthis->name, Q_(tthis->name_orig));
  }

#undef name_strlcpy

}

/***************************************************************
  Marks all effects in the given vector as inactive.
***************************************************************/
static void deactivate_effects(struct ceff_vector *ceff)
{
  int i, efflen;

  assert(ceff != NULL);
  efflen = ceff_vector_size(ceff);
  for (i = 0; i < efflen; ++i) {
    struct eff_city *effect = ceff_vector_get(ceff, i);
    effect->active = 0;
  }
}

/***************************************************************
  Marks all effects in all effect vectors as inactive.
***************************************************************/
static void deactivate_all_effects(void)
{
  deactivate_effects(get_geff_parent(get_eff_world()));
  deactivate_effects(&game.destroyed_effects);

  players_iterate(pplayer) {
    int contid;

    deactivate_effects(get_geff_parent(get_eff_player(pplayer)));
    for (contid = 1; contid <= map.num_continents; ++contid) {
      deactivate_effects(get_geff_parent(get_eff_island(contid, pplayer)));
    }
    city_list_iterate(pplayer->cities, pcity) {
      deactivate_effects(get_eff_city(pcity));
    } city_list_iterate_end;
    deactivate_effects(&pplayer->destroyed_effects);
  } players_iterate_end;

  freelog(LOG_DEBUG, "All effects deactivated");
}

/***************************************************************
  Mark as active all relevant effects in the given effect
  vector.
***************************************************************/
static void update_city_effects(struct ceff_vector *ceff, struct city *pcity,
				struct player *pplayer,
				bool *cond_eff, bool *changed)
{
  int i;

  for (i = 0; i < ceff_vector_size(ceff); i++) {
    struct eff_city *effect = ceff_vector_get(ceff, i);

    /* Consider the effects of all active improvements */
    if (effect->impr != B_LAST
        && (!pcity || pcity->improvements[effect->impr] == I_ACTIVE)) {
      Eff_Status testbit;
      struct impr_effect *imeff = get_base_effect(effect->impr, pplayer);
      bool this_changed = FALSE;

      for (testbit = 1;
           imeff && imeff->type != EFT_LAST;
           imeff++, testbit <<= 1) {

        /* Do not activate destroyed effects that do not "survive" */
        if (!pcity && !imeff->survives) {
          freelog(LOG_DEBUG,
                  "Not activating non-surviving destroyed effect %s (from %s)",
                  effect_type_name(imeff->type),
		  get_effect_cause_name(effect->impr, pplayer));

        /* Do nothing if the effect is already active */
        } else if (effect->active & testbit) {
          freelog(LOG_DEBUG,"Effect %s (from %s) already active in %s",
                  effect_type_name(imeff->type),
		  get_effect_cause_name(effect->impr, pplayer),
                  pcity ? pcity->name : "destroyed city");

        /* Test to see if the effect is activated by prerequisites */
        } else if (is_effect_activated(effect->impr, pcity, pplayer,
                                       imeff, cond_eff)) {
          freelog(LOG_DEBUG,"Effect %s (from %s) active in %s!",
                  effect_type_name(imeff->type),
		  get_effect_cause_name(effect->impr, pplayer),
                  pcity ? pcity->name : "destroyed city");
          effect->active |= testbit;
          this_changed = *changed = TRUE;
        }
      }

      /* Sync wide-range effect structures with the city structure */
      if (this_changed) {
        update_global_effect(pcity, pplayer, effect);
      }
    }
  }
}

/***************************************************************
  Update all improvement effects.
***************************************************************/
void update_all_effects(void)
{
  bool changed, cond_eff;

  improvements_update_obsolete();
  improvements_update_redundant(NULL, NULL, 0, IR_WORLD);

  deactivate_all_effects();

  /* We may need to loop several times, if some effects are dependent on other
   * effects (cond_eff=TRUE) _and_ one or more effects (which may satisfy this
   * dependency) were activated in this pass (changed=TRUE) */
  do {
    changed = cond_eff = FALSE;
    freelog(LOG_DEBUG, "Effect test pass");
    players_iterate(pplayer) {
      city_list_iterate(pplayer->cities, pcity) {
	struct ceff_vector *ceff = get_eff_city(pcity);
	update_city_effects(ceff, pcity, pplayer, &cond_eff, &changed);
      } city_list_iterate_end;

      /* Update all effects of this player's destroyed Wonders */
      update_city_effects(&pplayer->destroyed_effects, NULL, pplayer,
			  &cond_eff, &changed);
    } players_iterate_end;

    /* Update all effects of destroyed Wonders that belonged to
     * now-dead players */
    update_city_effects(&game.destroyed_effects, NULL, NULL,
			&cond_eff, &changed);
  } while (changed && cond_eff);

  players_iterate(pplayer) {
    city_list_iterate(pplayer->cities, pcity) {
      update_city_bonuses(pcity);
    } city_list_iterate_end;
  } players_iterate_end;
}

/***************************************************************
  Creates a set of calendars that reproduce the behaviour of
  original Freeciv. These are used if the calendars cannot be
  read from the rulesets (i.e. server and client do not share
  the "calendars" capability).
***************************************************************/
void setup_default_calendars(void)
{
  int i;

  assert(C_LAST >= 7);
  game.num_calendars = 7;
  for (i = 0; i < game.num_calendars; ++i) {
    game.calendars[i].req_tech = A_NONE;
    game.calendars[i].early_tech = A_NONE;
  }
  sz_strlcpy(game.calendars[0].name, N_("Prehistory"));
  game.calendars[0].first_year = -4001;
  game.calendars[0].turn_years = 50;
  sz_strlcpy(game.calendars[1].name, N_("Ancient Age"));
  game.calendars[1].first_year = -1001;
  game.calendars[1].turn_years = 25;
  sz_strlcpy(game.calendars[2].name, _("Classical Age"));
  game.calendars[2].first_year = -1;
  game.calendars[2].turn_years = 20;
  sz_strlcpy(game.calendars[3].name, N_("Dark Ages"));
  game.calendars[3].first_year = 1000;
  game.calendars[3].turn_years = 10;
  sz_strlcpy(game.calendars[4].name, N_("Middle Ages"));
  game.calendars[4].first_year = 1500;
  game.calendars[4].turn_years = 5;
  sz_strlcpy(game.calendars[5].name, N_("Renaissance"));
  game.calendars[5].first_year = 1750;
  game.calendars[5].turn_years = 2;
  sz_strlcpy(game.calendars[6].name, N_("Modern Age"));
  game.calendars[6].first_year = 1900;
  game.calendars[6].turn_years = 1;
}
