/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <string.h>
#include <slang/slang.h>

#include "fcintl.h"
#include "game.h"
#include "government.h"
#include "log.h"
#include "shared.h"
#include "support.h"
#include "unit.h"

#include "script.h"

static void ut_set_name(int *id, char *name)
{
  struct unit_type *ut = get_unit_type(*id);
  sz_strlcpy(ut->name_orig, name);
  sz_strlcpy(ut->name, _(name));
}

static void ut_set_graphic(int *id, char *name, char *name_alt)
{
  struct unit_type *ut = get_unit_type(*id);
  sz_strlcpy(ut->graphic_str, name);
  sz_strlcpy(ut->graphic_alt, name_alt);
}

static void ut_set_move_rate(int *id, int *move_rate)
{
  struct unit_type *ut = get_unit_type(*id);
  ut->move_rate = *move_rate * SINGLE_MOVE;
}

struct members {
  char *name;
  unsigned long offset;
};

#define FC_UT_MEMBER(member, name) \
  {#name, ((unsigned long) ((char *) &((struct unit_type *)0)->member))}

#define FC_MEMBER_P(struct_p, offset) \
  ((int *) ((char *)(struct_p) + offset))

static struct members ut_members[] =
{
  FC_UT_MEMBER(move_type, move_type),
  FC_UT_MEMBER(build_cost, build_cost),
  FC_UT_MEMBER(pop_cost, pop_cost),
  FC_UT_MEMBER(attack_strength, attack),
  FC_UT_MEMBER(defense_strength, defense),
  FC_UT_MEMBER(vision_range, vision_range),
  FC_UT_MEMBER(transport_capacity, transport_cap),
  FC_UT_MEMBER(hp, hitpoints),
  FC_UT_MEMBER(firepower, firepower),
  FC_UT_MEMBER(fuel, fuel),
  FC_UT_MEMBER(happy_cost, uk_happy),
  FC_UT_MEMBER(shield_cost, uk_shield),
  FC_UT_MEMBER(food_cost, uk_food),
  FC_UT_MEMBER(gold_cost, uk_gold)
};
#define num_ut_members (sizeof(ut_members) / sizeof(ut_members[0]))

static void ut_get(int *id)
{
  char *field_names[num_ut_members + 6];
  unsigned char field_types[num_ut_members + 6];
  VOID_STAR field_values[num_ut_members + 6];
  struct unit_type *ut = get_unit_type(*id);
  int move_rate = ut->move_rate / SINGLE_MOVE;
  char *name = ut->name_orig;
  char *graphic = ut->graphic_str, *graphic_alt = ut->graphic_alt;
  int i;

  for (i = 0; i < num_ut_members; i++) {
    field_names[i] = ut_members[i].name;
    field_types[i] = SLANG_INT_TYPE;
    field_values[i] = FC_MEMBER_P(ut, ut_members[i].offset);
  }

  field_names[i] = "name";
  field_types[i] = SLANG_STRING_TYPE;
  field_values[i++] = &name;

  field_names[i] = "graphic";
  field_types[i] = SLANG_STRING_TYPE;
  field_values[i++] = &graphic;

  field_names[i] = "graphic_alt";
  field_types[i] = SLANG_STRING_TYPE;
  field_values[i++] = &graphic_alt;

  field_names[i] = "move_rate";
  field_types[i] = SLANG_INT_TYPE;
  field_values[i++] = &move_rate;

  field_names[i] = "flags";
  field_types[i] = SLANG_NULL_TYPE;
  field_values[i++] = NULL;

  field_names[i] = "roles";
  field_types[i] = SLANG_NULL_TYPE;
  field_values[i++] = NULL;

  SLstruct_create_struct(i, field_names, field_types, field_values);
}

static void ut_set_ints(void)
{
  int i, id;
  unsigned int num = SLang_Num_Function_Args;
  struct unit_type *ut;

  assert(num == num_ut_members + 1);

  SLang_pop_integer(&id);
  ut = get_unit_type(id);

  for (i = num_ut_members - 1; i >= 0; i--) {
    int val, *pt;
    SLang_pop_integer(&val);
    pt = FC_MEMBER_P(ut, ut_members[i].offset);
    *pt = val;
  }
}

static void gt_set_name(int *id, char *name)
{
  struct government *gov = get_government(*id);
  sz_strlcpy(gov->name_orig, name);
  sz_strlcpy(gov->name, _(name));
}

#define FC_GT_MEMBER(member, name) \
  {#name, ((unsigned long) ((char *) &((struct government *)0)->member))}

static struct members gt_members[] =
{
  FC_GT_MEMBER(max_rate, max_single_rate),
  FC_GT_MEMBER(civil_war, civil_war_chance),
  FC_GT_MEMBER(free_happy, unit_free_unhappy),
  FC_GT_MEMBER(free_shield, unit_free_shield),
  FC_GT_MEMBER(free_food, unit_free_food),
  FC_GT_MEMBER(free_gold, unit_free_gold)
};
#define num_gt_members (sizeof(gt_members) / sizeof(gt_members[0]))

static void gt_get(int *id)
{
  char *field_names[num_gt_members + 3];
  unsigned char field_types[num_gt_members + 3];
  VOID_STAR field_values[num_gt_members + 3];
  struct government *gov = get_government(*id);
  char *name = gov->name_orig;
  char *graphic = gov->graphic_str, *graphic_alt = gov->graphic_alt;
  int i;

  for (i = 0; i < num_gt_members; i++) {
    field_names[i] = gt_members[i].name;
    field_types[i] = SLANG_INT_TYPE;
    field_values[i] = FC_MEMBER_P(gov, gt_members[i].offset);
  }

  field_names[i] = "name";
  field_types[i] = SLANG_STRING_TYPE;
  field_values[i++] = &name;

  field_names[i] = "graphic";
  field_types[i] = SLANG_STRING_TYPE;
  field_values[i++] = &graphic;

  field_names[i] = "graphic_alt";
  field_types[i] = SLANG_STRING_TYPE;
  field_values[i++] = &graphic_alt;

  SLstruct_create_struct(i, field_names, field_types, field_values);
}

static void sl_freelog(char *str)
{
  freelog(LOG_NORMAL, "%s", str);
}

static void ut_get_flags(int *id)
{
  int i, dims[1] = {F_LAST};
  SLang_Array_Type *arr;

  arr = SLang_create_array(SLANG_INT_TYPE, 0, NULL, dims, 1);

  for (i = 0; i < F_LAST; i++) {
    int val = unit_type_flag(*id, i) ? 1 : 0;
    dims[0] = i;
    SLang_set_array_element(arr, dims, &val);
  }

  SLang_push_array(arr, 1);
}

static void ut_set_flags(void)
{
  SLang_Array_Type *arr;
  int id, i;
  struct unit_type *ut;

  SLang_pop_array_of_type(&arr, SLANG_INT_TYPE);
  assert(arr->num_dims == 1 && arr->dims[0] == F_LAST);

  SLang_pop_integer(&id);
  ut = get_unit_type(id);
  BV_CLR_ALL(ut->flags);
  for (i = 0; i < F_LAST; i++) {
    int val;
    SLang_get_array_element(arr, &i, &val);
    if (val) {
      BV_SET(ut->flags, i);
    }
  }
  SLang_free_array(arr);
}

static void ut_get_roles(int *id)
{
  int i, dims[1] = {L_LAST - L_FIRST};
  SLang_Array_Type *arr;

  arr = SLang_create_array(SLANG_INT_TYPE, 0, NULL, dims, 1);

  for (i = L_FIRST; i < L_LAST; i++) {
    int val = unit_has_role(*id, i) ? 1 : 0;
    dims[0] = i - L_FIRST;
    SLang_set_array_element(arr, dims, &val);
  }

  SLang_push_array(arr, 1);
}

static void ut_set_roles(void)
{
  SLang_Array_Type *arr;
  int id, i;
  struct unit_type *ut;

  SLang_pop_array_of_type(&arr, SLANG_INT_TYPE);
  assert(arr->num_dims == 1 && arr->dims[0] == L_LAST - L_FIRST);

  SLang_pop_integer(&id);
  ut = get_unit_type(id);
  BV_CLR_ALL(ut->roles);
  for (i = 0; i < L_LAST - L_FIRST; i++) {
    int val;
    SLang_get_array_element(arr, &i, &val);
    if (val) {
      BV_SET(ut->roles, i);
    }
  }
  SLang_free_array(arr);
}

static void register_intrinsics(void)
{
  char buffer[1024];
  static SLang_Intrin_Fun_Type intrins[] =
  {
    MAKE_INTRINSIC_I("ut_get", ut_get, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_IS("ut_set_name", ut_set_name, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_ISS("ut_set_graphic", ut_set_graphic, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_II("ut_set_move_rate", ut_set_move_rate, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_0("ut_set_flags", ut_set_flags, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_I("ut_get_flags", ut_get_flags, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_0("ut_set_roles", ut_set_roles, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_I("ut_get_roles", ut_get_roles, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_0("ut_set_ints", ut_set_ints, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_S("unit_flag_from_str", unit_flag_from_str, SLANG_INT_TYPE),
    MAKE_INTRINSIC_S("unit_role_from_str", unit_role_from_str, SLANG_INT_TYPE),
    MAKE_INTRINSIC_I("gt_get", gt_get, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_IS("gt_set_name", gt_set_name, SLANG_VOID_TYPE),
    MAKE_INTRINSIC_S("freelog", sl_freelog, SLANG_VOID_TYPE),
    SLANG_END_INTRIN_FUN_TABLE
  };
  SLadd_intrin_fun_table(intrins, NULL);
  my_snprintf(buffer, sizeof(buffer),
              "variable Land = %d, Sea = %d, Heli = %d, Air = %d;",
	      LAND_MOVING, SEA_MOVING, HELI_MOVING, AIR_MOVING);
  SLang_load_string(buffer);
  my_snprintf(buffer, sizeof(buffer),
              "variable F_LAST = %d, L_LAST = %d, L_FIRST = %d;",
	      F_LAST, L_LAST, L_FIRST);
  SLang_load_string(buffer);
}

void script_test(void)
{
  SLang_load_file("test.sl");
}

void script_execute(char *cmds)
{
  if (cmds) {
    if (SLang_load_string(cmds) == -1) {
      SLang_restart(1);
      SLang_Error = 0;
    }
  }
}

void script_load_library(void)
{
  char filename[512], *dfilename;

  my_snprintf(filename, sizeof(filename), "%s/library.sl", game.rulesetdir);
  dfilename = datafilename(filename);
  if (dfilename) {
    if (SLang_load_file(dfilename) == -1) {
      SLang_restart(1);
      SLang_Error = 0;
    }
  }
}

void script_init(void)
{
  if (SLang_init_slang() == -1) {
    freelog(LOG_ERROR, "SLang init error");
  } else {
    register_intrinsics();
  }
}
