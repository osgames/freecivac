/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "fcintl.h"
#include "game.h"
#include "log.h"
#include "mem.h"
#include "player.h"
#include "script.h"
#include "shared.h"
#include "support.h"
#include "tech.h"

#include "government.h"

/* TODO:
 * o Update and turn on government evaluation code.
 * o Check exact government rules vs Civ1,Civ2.
 * o The clients display wrong upkeep icons in the city display,
 *   not sure what to do here.  (Does the new terrain-ruleset code
 *   have icons that could be used here?)
 * o When change government, server should update cities and unit
 *   upkeep etc and send updated info to client.
 * o Implement actual cost for unit gold upkeep.
 * o Possibly remove ai_gov_tech_hint stuff?
 *   (See usage in ai_manage_cities() in aicity.c)
 * o Update help system, including dynamic help on governments.
 * o Test the new government evaluation code (AI).
 *   [ It seems fine to me, although it favours Democracy very early
 *   on. This is because of the huge trade bonus. -SKi ]
 * o Implement the features needed for fundamentalism:
 *   - A diplomatic penalty modifier when international incidents occur.
 *     (Spy places nuke in city, goes to war, etc).
 *     [ Is this one of those Civ II "features" best be ignored?  I am
 *     inclined to think so -SKi ]
 */

/*
 * WISHLIST:
 * o Features needed for CTP-style rules, just more trade, science and
 *   production modifiers.  (Just counting CTP governments, only
 *   basics).
 */

struct government *governments = NULL;
struct modifier_type gov_modifier_types[MAX_MODS];
struct modifier *gov_modifiers = NULL;

struct ai_gov_tech_hint ai_gov_tech_hints[MAX_NUM_TECH_LIST];

static const char *flag_names[] = {
  "(unused)", "Revolution_When_Unhappy", "Has_Senate",
  "Unbribable", "Inspires_Partisans", "Rapture_City_Growth",
  "Fanatic_Troops", "No_Unhappy_Citizens", "Convert_Tithes_To_Money",
  "Reduced_Research", "CanModify"
};
static const char *hint_names[] = {
  "Is_Nice", "Favors_Growth"
};

/***************************************************************
  Convert flag names to enum; case insensitive;
  returns G_LAST_FLAG if can't match.
***************************************************************/
enum government_flag_id government_flag_from_str(const char *s)
{
  enum government_flag_id i;

  assert(ARRAY_SIZE(flag_names) == G_LAST_FLAG);
  
  for(i=G_FIRST_FLAG; i<G_LAST_FLAG; i++) {
    if (mystrcasecmp(flag_names[i], s)==0) {
      return i;
    }
  }
  return G_LAST_FLAG;
}

/***************************************************************
...
***************************************************************/
bool government_has_flag(const struct government *gov,
			enum government_flag_id flag)
{
  assert(flag>=G_FIRST_FLAG && flag<G_LAST_FLAG);
  return TEST_BIT(gov->flags, flag);
}

/***************************************************************
  Convert hint names to enum; case insensitive;
  returns G_LAST_HINT if can't match.
***************************************************************/
enum government_hint_id government_hint_from_str(const char *s)
{
  enum government_hint_id i;

  assert(ARRAY_SIZE(hint_names) == G_LAST_HINT);
  
  for(i=G_FIRST_HINT; i<G_LAST_HINT; i++) {
    if (mystrcasecmp(hint_names[i], s)==0) {
      return i;
    }
  }
  return G_LAST_HINT;
}

/***************************************************************
...
***************************************************************/
bool government_has_hint(const struct government *gov,
			enum government_hint_id hint)
{
  assert(hint>=G_FIRST_HINT && hint<G_LAST_HINT);
  return TEST_BIT(gov->hints, hint);
}

/***************************************************************
...
***************************************************************/
struct government *find_government_by_name(const char *name)
{
  government_iterate(gov) {
    if (mystrcasecmp(gov->name, name) == 0) {
      return gov;
    }
  } government_iterate_end;

  return NULL;
}

int find_gov_modifier_by_name(const char *s)
{
  return find_modifier_by_name(s, gov_modifiers, game.num_gov_modifiers);
}

bool government_has_modifier(struct government *gov, int govmod)
{
  if (gov && gov->basetype != G_MAGIC) {
    int i;

    for (i = 0; i < game.num_gov_modifier_types; ++i) {
      if (gov->modifiers[i] == govmod) {
	return TRUE;
      }
    }
  }
  return FALSE;
}

/***************************************************************
...
***************************************************************/
struct government *get_government(int gov)
{
  assert(game.government_count > 0 && gov >= 0
	 && gov < game.government_count);
  assert(governments[gov].index == gov);
  return &governments[gov];
}

/***************************************************************
...
***************************************************************/
struct government *get_gov_pplayer(struct player *pplayer)
{
  assert(pplayer != NULL);
  return get_government(pplayer->government);
}

/***************************************************************
...
***************************************************************/
struct government *get_gov_pcity(struct city *pcity)
{
  assert(pcity != NULL);
  return get_gov_pplayer(city_owner(pcity));
}


/***************************************************************
...
***************************************************************/
const char *get_ruler_title(int gov, bool male, int nation)
{
  struct government *g = get_government(gov);
  struct ruler_title *best_match = NULL;
  int i;

  for(i=0; i<g->num_ruler_titles; i++) {
    struct ruler_title *title = &g->ruler_titles[i];
    if (title->nation == DEFAULT_TITLE && !best_match) {
      best_match = title;
    } else if (title->nation == nation) {
      best_match = title;
      break;
    }
  }

  if (best_match) {
    return male ? best_match->male_title : best_match->female_title;
  } else {
    freelog(LOG_ERROR,
	    "get_ruler_title: found no title for government %d (%s) nation %d",
	    gov, g->name, nation);
    return male ? "Mr." : "Ms.";
  }
}

/***************************************************************
...
***************************************************************/
int get_government_max_rate(int type)
{
  if(type == G_MAGIC)
    return 100;
  if(type >= 0 && type < game.government_count)
    return governments[type].max_rate;
  return 50;
}

/***************************************************************
Added for civil war probability computation - Kris Bubendorfer
***************************************************************/
int get_government_civil_war_prob(int type)
{
  if(type >= 0 && type < game.government_count)
    return governments[type].civil_war;
  return 0;
}

/***************************************************************
...
***************************************************************/
const char *get_government_name(int type)
{
  if(type >= 0 && type < game.government_count)
    return governments[type].name;
  return "";
}

/***************************************************************
  Can change to government if appropriate tech exists, and one of:
   - no required tech (required is A_NONE)
   - player has required tech
   - we have an appropriate wonder
***************************************************************/
bool can_change_to_government(struct player *pplayer, int government)
{
  int req;

  assert(game.government_count > 0 &&
	 government >= 0 && government < game.government_count);

  req = governments[government].required_tech;
  if (!tech_is_available(pplayer, req)) {
    /* If the technology doesn't "exist" or if there is no way we can
     * ever get it, then we can't change to the gov type even if we have
     * a wonder that would otherwise allow it. */
    return FALSE;
  } else if (get_invention(pplayer, req) == TECH_KNOWN) {
    return TRUE;
  } else {
    /* Do we have the Any_Government effect? */
    impr_effects_iterate(iter, B_LAST, NULL, pplayer) {
      if (iter.imeff->type == EFT_ANY_GOVERNMENT) return TRUE;
    } impr_effects_iterate_end;
  }
  return FALSE;
}

/***************************************************************
...
***************************************************************/
void set_ruler_title(struct government *gov, int nation,
                     const char *male, const char *female)
{
  struct ruler_title *title;

  gov->num_ruler_titles++;
  gov->ruler_titles =
    (struct ruler_title *)fc_realloc(gov->ruler_titles,
      gov->num_ruler_titles*sizeof(struct ruler_title));
  title = &(gov->ruler_titles[gov->num_ruler_titles-1]);

  title->nation = nation;
  sz_strlcpy(title->male_title, male);
  sz_strlcpy(title->female_title, female);
}

/***************************************************************
 Allocate space for the given number of governments.
***************************************************************/
void governments_alloc(int num)
{
  int index;

  governments = fc_calloc(num, sizeof(struct government));
  game.government_count = num;

  for (index = 0; index < num; index++) {
    governments[index].index = index;
  }
}

/***************************************************************
 De-allocate resources associated with the given government.
***************************************************************/
static void government_free(struct government *gov)
{
  free(gov->effect);
  gov->effect = NULL;

  free(gov->ruler_titles);
  gov->ruler_titles = NULL;

  free(gov->helptext);
  gov->helptext = NULL;
}

/***************************************************************
 De-allocate the currently allocated governments.
***************************************************************/
void governments_free(void)
{
  government_iterate(gov) {
    government_free(gov);
  } government_iterate_end;
  free(governments);
  governments = NULL;
  game.government_count = 0;
}

/***************************************************************
...
***************************************************************/
static void copy_government(struct government *dest, struct government *src)
{
  int i;

  memcpy(dest, src, sizeof(struct government));

  dest->effect = NULL;
  append_effects(&dest->effect, src->effect);

  dest->num_ruler_titles = src->num_ruler_titles;
  dest->ruler_titles = fc_calloc(dest->num_ruler_titles,
      				 sizeof(struct ruler_title));
  for (i = 0; i < dest->num_ruler_titles; i++) {
    struct ruler_title *fromtitle = &(src->ruler_titles[i]);
    struct ruler_title *totitle = &(dest->ruler_titles[i]);

    totitle->nation = fromtitle->nation;
    sz_strlcpy(totitle->male_title, fromtitle->male_title);
    sz_strlcpy(totitle->female_title, fromtitle->female_title);
  }

  dest->helptext = src->helptext ? mystrdup(src->helptext) : NULL;
}

/***************************************************************
...
***************************************************************/
int form_compound_gov(struct player *pplayer, const char *name,
    		      const int basetype, const int modifiers[MAX_MODS])
{
  struct government *gov = NULL, *base;
  struct modifier *mod;
  char buffer[200];
  int id = -1, i;

  if (pplayer) {
    id = pplayer->government;
    gov = get_gov_pplayer(pplayer);
  }
  if (!pplayer || gov->basetype == G_MAGIC) {
    id = game.government_count++;
    if (id >= G_MAGIC) {
      freelog(LOG_FATAL, "form_compound_gov: out of government types");
      exit(EXIT_FAILURE);
    }

    governments = (struct government *)fc_realloc(governments,
						  game.government_count
						  * sizeof(struct government));
    gov = &governments[id];
  } else if (pplayer) {
    government_free(gov);
  }
  base = &governments[basetype];
  copy_government(gov, base);
  gov->index = id;
  gov->basetype = basetype;
  for (i = 0; i < game.num_gov_modifier_types; ++i) {
    gov->modifiers[i] = modifiers[i];
  }
  gov->required_tech = A_NONE;
  gov->name_orig[0] = '\0';
  my_snprintf(buffer, sizeof(buffer),
	      "variable gt = get_gov(%d);", id);
  script_execute(buffer);
  for (i = 0; i < game.num_gov_modifier_types; ++i) {
    if (gov->modifiers[i] < U_LAST) {
      mod = &gov_modifiers[gov->modifiers[i]];
      script_execute(mod->action);
    }
  }
  for (i = 0; i < game.num_gov_modifier_types; ++i) {
    if (gov->modifiers[i] < U_LAST) {
      mod = &gov_modifiers[gov->modifiers[i]];
      script_execute(mod->final_action);
      append_effects(&gov->effect, mod->effect);
    }
  }
  my_snprintf(buffer, sizeof(buffer),
	      "set_gov(%d, gt);", id);
  script_execute(buffer);
  if (name && name[0]) {
    sz_strlcpy(gov->name_orig, name);
    sz_strlcpy(gov->name, _(gov->name_orig));
  }

  return id;
}
