/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "modifier.h"
#include "shared.h"
#include "support.h"

/**************************************************************************
  Convert modifier_type names to int; case insensitive;
  returns -1 if can't match.
**************************************************************************/
int modifier_type_from_str(const char *s,
    			   struct modifier_type *modifier_types,
			   int num_mod_types)
{
  int i;
  for (i = 0; i < num_mod_types; ++i) {
    if (mystrcasecmp(modifier_types[i].name, s) == 0) {
      return i;
    }
  }
  return -1;
}

/**************************************************************************
Does a linear search of modifiers[].name
Returns -1 if none match.
**************************************************************************/
int find_modifier_by_name(const char *s,
    			  struct modifier *modifiers,
			  int num_mods)
{
  int i;
  for (i = 0; i < num_mods; ++i) {
    if (mystrcasecmp(modifiers[i].name, s) == 0) {
      return i;
    }
  }
  return -1;
}
