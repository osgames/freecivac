===========================
Willkommen bei Freeciv 1.14
===========================

Dies ist Version 1.14.0.

Dank geht an alle unsere Entwickler, die daran gearbeitet haben.

Diese Version enth�lt eine Menge �nderungen, die unten skizziert sind. 
Diejenigen, die an den Einzelheiten der �nderungen interessiert sind,
sollten einen Blick in die ChangeLog Datei werfen.

�NDERUNGEN SEIT VERSION 1.13.0

 - St�dtenamen k�nnen nur noch einmal vergeben werden. Optional sind
   St�dtenamen auch weltweit eindeutig.
 - Ein neuer Kartengenerator (generator 5), der sehr interessante 
   fraktale Karten produziert.
 - Spielst�nde k�nnen vom Server eingelesen werden (load Kommando).
 - Kommandozeilenoptionen f�r den Client k�nnen gespeichert werden.
 - Spezialisten werden zuerst von den zufriedenen B�rgern genommen,
   nicht von den unzufriedenen. Dadurch ist es schwieriger, Aufst�nde
   zur�ckzudr�ngen, indem man Steuereintreiber und Wissenschaftler
   einstellt.
 - Standardm��ig startet man jetzt ohne vorgegebene Technologien;
   auch w�tende B�rger sind m�glich, auch wenn sie selten auftreten.
 - Die Berechnung des Effekts von Steuerraten wurde ge�ndert. Es ist 
   nicht l�nger m�glich, aus einer 60%-Rate 100% Effekt zu erhalten.
 - Es gibt einen neuen, historisch genaueren Regelsatz.
 - Eine Option verz�gert die Wirkung von Stadtfesten um einige Runden
   (show rapturedelay).
 - Die Reihenfolge der Serveraktivit�ten beim Rundenwechsel wurde
   ge�ndert.
 - Einige KI-Verbesserungen. Die KI sollte nun Einheiten und Geb�ude
   etwas intelligenter bauen und kaufen.
 - Einige Arbeiten am Code, zur Vorbereitung k�nfiger Features.
 - /fix und /unfix Kommandos, um �nderungen der Spielregeln nach dem
   Spielstart zu verhindern.
 - Die Serverkommandos "rulesout", "log", "freestyle" und "crash" sind
   entfernt worden; "rfcstyle" schaltet jetzt um zwischen rfcstyle und
   freestyle.
 - Der Civ2-Regelsatz entspricht jetzt genauer den Civ2 "Gottheit" 
   Vorgaben.
 - Eine gro�e Menge von Fehlerkorrekturen.

�NDERUNGEN SEIT VERSION 1.12.0

 - Die Automatische Stadtverwaltung (ASV) verteilt selbst�ndig die
   Arbeiter und Spezialisten in den St�dten.
 - Soundunterst�tzung.
 - Das neue 'isotrident' Grafikset ist jetzt Standard. Die Grafiksets
   'hires' und 'engels' wurden aus der Distribution herausgenommen, k�nnen
   aber von der Website heruntergeladen werden.
 - Ein neuer Stadtdialog im GTK Client.
 - Eine Windows-Version des Clients. Er hat einen verbesserten Verbindungs-
   dialog und erm�glicht das Laden und Speichern vom Client aus.
 - Eine GTK 2.0 Version des Clients.
 - Bei einer Stadtgr�ndung werden Namen vorgeschlagen, die zum Terrain
   passen.
 - Der Spieler-Dialog pr�sentiert seine Informationen jetzt farbig und
   sortierbar. Auch die Spielerflaggen werden angezeigt.
 - Der Server unterst�tzt nicht mehr die '--server' Kommandozeilenoption,
   stattdessen k�nnen Sie mit der '--info' Option den Ank�ndigungstext f�r
   den Metaserver angeben. Die '-a' Option des Clients �berspringt den Ver-
   bindungsdialog.
 - Das Serverkommando 'wall' sendet eine Nachricht an alle Spieler.
 - Eine neue, flexible Rundenzeitvorgabe kann mit der 'timeoutinc' Server-
   option eingestellt werden.
 - �briggebliebene Forschungspunkte werden f�r den n�chsten Fortschritt
   angerechnet.
 - Handelswege bringen mehr ein.
 - Einheiten erhalten doppelte Feuerkraft, wenn sie in St�dten liegende
   Schiffe angreifen; der Verteidiger hat nur Feuerkraft 1.
 - Hubschrauber, die von Lufteinheiten angegriffen werden, erhalten 50 %
   Abzug und haben gegen J�ger nur die Feuerkraft 1.
 - Stadtmauern k�nnen auch gebaut werden, wenn man das Weltwunder "Die
   gro�e Mauer" besitzt.
 - Im Kommunismus wurden die Nahrungskosten von Siedlern in den Standard-
   regeln auf 1 ge�ndert, unter den Civ2-Regeln betragen sie 2.
 - Tarnkappenj�ger und -bomber sind nun wirklich getarnt, und sind teil-
   weise unsichtbar (so wie U-Boote). Au�erdem wurde die Angriffsst�rke
   von Tarnkappenbombern von 14 auf 18 erh�ht.
 - In den Civ2-Regeln gibt es nun Fundamentalismus.
 - Verbesserte F�higkeiten von Modpacks: Die Karavaneigenschaft wurde
   geteilt. Neue Wege zur Bestimmung der Forschungskosten. Bessere Doku-
   mentation. Die Regeln k�nnen Starttechnologien festlegen. Es ist mehr
   als eine Bonustechnologie m�glich. Siedlerf�higkeiten wurden geteilt.
   Die Regelsyntax f�r Geb�ude wurde deutlich erweitert, aber die Effekte
   funktionieren noch nicht.
 - Die Serveroption 'tiyisles' erlaubt 1x1 gro�e Inseln; 'seperatepoles' 
   erlaubt Polkontinente.
 - 'citymindist' legt einen Minimalabstand zwischen St�dten fest; mit 
   'notradesize' und 'fulltradesize' kann der Handelsertrag von kleinen
   St�dten verringert werden.
 - Mit der 'angrycitizens' Option k�nnen B�rger auch 'w�tend' werden.
 - Von 'Wacht�rmen' (Befestigungen) aus kann man weiter sehen. Siehe 
   'watchtower' Optionen.
 - Wenn der Palast verlorengeht, erhalten Sie in einer zuf�llig ausge-
   w�hlten Stadt einen neuen. Dieses Verhalten kann mit der Serveroption
   'savepalace' ausgeschaltet werden.
 - Regels�tze k�nnen nun mit dem Kommando "rulesetdir" in den Server
   geladen werden. 
 - Die Grenze f�r die m�gliche Anzahl von Nationen in Freeciv wurde
   entfernt.
 - Das Format f�r die isometrische Grafik wurde ge�ndert (spec-Datei).
 - Der Code f�r die Karte und die KI wurde wesentlich bereinigt.
 - �bersetzungen verbessert. Bessere Unterst�tzung f�r Pluralformen
   m�glich.
 - Einige Fehler wurden beseitigt, und unter der Oberfl�che eine
   Menge Arbeit getan.
 - Eine �bersicht �ber die verbliebenen Fehler steht in doc/de/BUGS.de .

�NDERUNGEN SEIT VERSION 1.11.4

 - Mehrsprachigkeit erweitert. Weitere Verbesserungen sind n�tig.
   Gegenw�rtige �bersetzungen: de en_GB es fr hu it ja nl no pl pt 
   pt_BR ro ru sv.
 - Isometrische Ansicht im GTK- und Amiga-Client mit dem weitgehend
   Civ 2-kompatiblen HiRes Tileset. Dies ist standardm��ig eingeschal-
   tet, aber die alte nichtisometrische Darstellung ist weiterhin ver-
   f�gbar, mit der --tiles Option des Client, z.B. 
   "civclient --tiles trident".
 - Bei Eingabe des Goto-Befehls (Taste "g") zeigt eine Linie die Route
   von der Einheit bis zum Mauszeiger an. Die Taste "g" setzt einen
   Wegpunkt an der Stelle des Mauszeigers.
 - Der Server vervollst�ndigt nun die Eingaben. Dies funktioniert auf
   allen Ebenen. z.B. "cu<TAB>" wird zu "cut", und wenn ein Spieler
   namens "paulz" existiert, wird "cut pau<TAB>" zu "cut paulz" kom-
   plettiert.
 - Spieler k�nnen sich gegenseitig Karteneinsicht geben, dann sieht man
   automatisch alles, was der andere Spieler sieht.
 - Im Men� lassen sich die Details einstellen, die auf der Karte ange-
   zeigt werden. Z.B. kann man w�hlen, das Stra�en nicht angezeigt 
   werden.
 - Der Server pr�ft die verbundenen Clients, und trennt die Verbindung
   zu denen, die nicht rechtzeitig antworten.
 - Geschicktere Plazierung der Partisanen.
 - Der Server startet nicht mehr automatisch, wenn die H�chstzahl der
   Spieler erreicht ist.
 - Wenn Zugriffsebenen verwendet werden und der kontrollierende Spieler
   ausscheidet, kann ein anderer Spieler mit dem "/firstlevel" Befehl
   die erste Zugriffsebene erhalten.
 - Die Datei README.ruleset enth�lt nun einen Abschnitt �ber
   "Einschr�nkungen und Grenzen".
 - Karawanen, Diplomaten und Spione k�nnen nun in verb�ndete St�dte 
   ziehen.
 - Elephanten, Kreuzritter, und Fanatiker sind nun in den Civ 2 Regeln
   enthalten (civ2 ruleset).
 - Dei Gr��e der st�dtischen Kornkammer wird nun �ber die ruleset-
   Variablen "granary_food_ini" und "granary_food_inc" eingestellt.
 - Die Anzahl der im ruleset einstellbaren Stadterweiterungen ist nun 
   unbegrenzt.
 - Kapitalisierung ist nun von Spielbeginn an verf�gbar, in den 
   Standardregeln.
 - St�dte k�nnen 0 Handelspunkte abwerfen (bisher mindestens 1).
 - Siedler k�nnen nur zu einer Stadt hinzugef�gt werden, die kleiner 
   als Gr��e 8 ist, wie in Civ 2.
 - Wenn ein Fallschirmspringer �ber unbekanntem Gebiet abspringt, und 
   das Zielfeld von einer feindlichen Einheit besetzt ist, ist der 
   Fallschirmspringer verloren. Springt er (aus Versehen) �ber dem Meer
   ab, ist er ebenfalls verloren.
 - Diplomaten und Spione k�nnen nicht vom Schiff aus arbeiten.
 - Das Auftanken der Lufteinheiten wird so durchgef�hrt, das Einheiten
   mit leerem Tank bevorzugt werden; in zweiter Linie werden die teu-
   ersten Einheiten bevorzugt.
 - Die Gefahr, Triremen zu verlieren, h�ngt davon ab, welche Techno-
   logien bekannt sind.
 - Leonardos Werkstatt modernisiert die Einheiten jetzt in zuf�lliger
   Reihenfolge.
 - Truppen in verb�ndeten St�dten erzeugen keine zus�tzliche Unzufrie-
   denheit.
 - W�sten werden vorzugsweise zwischen dem 15. und 35. Breitengrad 
   erzeugt.
 - An den Polen befinden sich nun ausschlie�lich Gletscher.
 - Der Server weist darauf hin, wenn eine neue Regierungsform verf�gbar
   wird.
 - Vor Hungersn�ten wird nun rechtzeitig gewarnt.
 - Es wird mitgeteilt, wenn ein Weltwunder kurz vor der Vollendung 
   steht.
 - Die Spieler werden darauf hingewiesen, wenn eines ihrer Wunder 
   ausgelaufen (veraltet) ist.
 - Im Spieler-Dialog wird nun aufgef�hrt, welche Spieler bei Dir eine 
   Botschaft gegr�ndet haben. Deine eigenen Botschaften werden eben-
   falls angezeigt.
 - Im Weltwunder-Bericht werden auch die im Bau befindlichen Wunder 
   gelistet.
 - Der "save" Befehl speichert den Spielstand unter
   <auto-save Namenspr�fix><jahr>m.sav[.gz] wenn kein Argument
   angegeben wird.
 - Die Serveroption "quitidle" beendet den Server, wenn kein Spieler 
   mehr verbunden ist.
 - Mit der Option "autotoggle" werden menschliche Nationen von der KI 
   �bernommen, wenn kein Spieler mehr mit der Nation verbunden ist.
 - Der Server blockiert nicht mehr, wenn er einem langsamen Host
   Daten sendet, einstellbar �ber die Optionen "tcptimeout" und
   "netwait".
 - Die Servervariable "savename" stellt den Pr�fix f�r automatisches 
   Speichern ein.
 - Die Servervariable "allowconnect" gibt die Kontrolle, welche Typen
   von Spielern (neue Spieler, menschliche Spieler, KI-Spieler, tote 
   Spieler, Barbaren) von einem Client �bernommen werden k�nnen.
 - Weitere Nationen hinzugef�gt.
 - Neue Landkarten im Verzeichnis data/scenario: 
   Britische Inseln (british-isles-80x76-v2.51.sav), 
   Iberische Halbinsel (iberian-peninsula-136x100-v0.9.sav), 
   Die Erde (hagworld-120x60-v1.2.sav).
 - Mehrsprachigkeit und �bersetzungen f�r Amiga.
 - Amiga-Client: History-Funktion f�r Chat.
 - Jede Menge Korrekturen und Aufr�umarbeiten.

�NDERUNGEN SEIT VERSION 1.11.0

 - Die Eingabezeile am Server kann nun editiert werden.
 - Die Einheiten zu Spielbeginn k�nnen nun �ber eine gewisse
   Fl�che verteilt werden. Siehe Serveroption "dispersion".
 - Der erste Client, der sich mit einem Server verbindet, kann
   nun eine h�here Zugriffsebene erhalten. Siehe Serveroption
   "cmdlevel".
 - Spielst�nde werden nun automatisch komprimiert gespeichert
   und beim Laden entpackt.
 - Minimale Anzahl von angrenzenden Ozeanfeldern ist erforder-
   lich, um Land in Meer zu transformieren (Standard ist 1).
 - Nuklearer Fallout. Industrie und Bev�lkerung erzeugen weiter
   Verschmutzung. Eine Atombombe erzeugt Fallout, der sich von
   Verschmutzung unterscheidet. Es gibt einen neuen Befehl, um
   Fallout zu beseitigen. Fallout tr�gt zum Nuklearen Winter bei,
   der ebenfalls die Landschaft ver�ndert, aber zu W�sten,
   Tundra, und Gletscher. Ein neues Symbol im Infofeld zeigt an,
   wie weit fortgeschritten der Nukleare Winter ist, und auf der
   Karte gibt es ein neues Symbol f�r Fallout. Die KI bem�ht sich
   nun mehr, Verschmutzung zu beseitigen, jedoch nicht Fallout.
 - Freeciv l�uft nun unter OpenVMS.
 - Dei meisten Dinge, die bisher mit der "civstyle" Serveroption
   eingestellt wurden, werden nun durch das ruleset eingestellt.
 - Fehler in der Anzeige des Baufortschritts wurde beseitigt.
 - Fehler beseitigt: Wenn ein Diplomat/Spion die Stadt unter-
   sucht, werden die Einheiten nun richtig angezeigt.
 - Mehrere Fehler des Goto-Befehls wurden beseitigt.
 - Fehler beseitigt: Durch speichern und Neustart des Spieles
   w�hrend einer Revolution konnte man die Anarchieperiode um-
   gehen.
 - Fehler beseitigt: Es war m�glich, einen Fallschirmspringer in
   einer Stadt abzusetzen, auch wenn dort feindliche Einheiten
   standen.

�NDERUNGEN SEIT VERSION 1.10.0

 - Die Mehrsprachigkeit wurde erweitert. Weitere Verbesserung
   n�tig. Gegenw�rtige �bersetzungen: de en_GB es fr hu ja nl no
   pl pt pt_BR ru.
 - Nebel des Krieges eingef�hrt. Kontrolliert von der Server-
   option "fogofwar".
 - Diplomatischer Status zwischen Nationen eingef�hrt: Krieg,
   Neutralit�t, kein Kontakt, Waffenstillstand, Frieden und
   B�ndnis.
 - Landschaftstransformation Land/Ozean. Ingenieure k�nnen Sumpf
   in Meer und Meer in Sumpf verwandeln. Wald kann in Sumpf ver-
   wandelt werden.
 - Es sind nun bis zu 30 Spieler m�glich.
 - Eingraben (von Einheiten) dauert nun 1 Runde (wie in Civ1/2).
 - Das Apollo-Wunder ist nun wie in Civ2 (zeigt die ganze Karte
   und nicht nur die St�dte). Einstellbar �ber die "civstyle"
   Serveroption.
 - Agressive See-Einheiten verursachen keine Unzufriedenheit,
   wenn sie sich in einer Stadt befinden.
 - Wenn ein Schiff eine Landeinheit angreift, wird nun beider
   Feuerkraft auf 1 reduziert (entspricht Civ2).
 - Wenn das gegenw�rtige Forschungsziel ge�ndert wird, und dann
   wieder zur�ckgewechselt wird, bleiben die Forschungspunkte
   erhalten.
 - Beim Klick auf das Infofeld werden mehr Informationen ange-
   zeigt.
 - Die Anzeige der Globalen Erw�rmung wurde verbessert.
 - Hinweis auf bevorstehendes Stadtwachstum.
 - Der Serverbefehl "remove <Spieler>" ist nicht mehr verf�gbar,
   wenn das Spiel begonnen hat.
 - �ber die Serveroptionen "fixedlength" und "timeout" kann ein
   Rundenzeitlimit vorgegeben werden.
 - Als Rundendauer kann l�nger sein (bis zu 1 Tag).
 - Goto-Befehl f�r Lufteinheiten. Wenn das Ziel au�er Reichweite
   ist, werden Zwischenstops in St�dten und auf Luftbasen einge-
   legt.
 - Einheiten k�nnen nun durch Klicken auf den Einheitenstapel in
   der Anzeige links ausgew�hlt werden.
 - Diplomaten und Spione k�nnen mit dem Goto-Befehl nun auch in
   gegnerische St�dte bewegt werden. Das Befehlsfenster �ffnet 
   sich, wenn sie die Stadt erreichen.
 - Verbesserte Umsetzung des Goto-Befehls.
 - Die Hilfe zeigt an, ob durch einen Fortschritt ein Geb�ude
   veraltet.
 - Die Stadtproduktion kann auf der Karte angezeigt werden.
 - Die Serveroption "autotoggle" schaltet den KI-Status eines
   Spielers ein und aus, wenn ein Spieler die Verbindung auf-
   nimmt oder trennt.
 - Der Hoover-Staudamm kann nun �berall gebaut werden (wie in
   Civ2).
 - Im Forschungsbericht werden nun die Runden pro Fortschritt
   angezeigt.
 - Verbesserte Darstellung der Karte und der Einheitenbewegung.
 - Clientoption "Automatisches Rundenende".
 - Die Strafen f�r �nderung der Stadtproduktion werden nun kor-
   rekter angewendet.
 - "Bewachen" und "Eingraben" Befehle auch im Stadtfenser ver-
   f�gbar.
 - Mehr Nationen.
 - Der GTK+ Client kann nun �ber eine Ressourcendatei angepasst
   werden.
 - Verbesserter Netzwerkcode f�r zuverl�ssigere Verbindungen.
 - F�r jede Nation gibt es nun eine eigene ruleset Datei.
 - Das Format f�r Registrierungsdateien wurde erweitert.
 - Weitere Optionen f�r das ./configure Script: --with-xaw und
   --enable-client=xaw3d .
 - Viele Fehler beseitigt und Code bereinigt.

Die �NDERUNGEN f�r die VERSIONEN 1.9.0 bis 1.6.3 finden Sie auf Englisch
in der Datei NEWS.


