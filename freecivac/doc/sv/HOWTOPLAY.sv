Hur man spelar Freeciv

  Ursprungligen av Michael Hohensee (aka Zarchon)

F�r upplysningar om hur man installerar Freeciv, se INSTALL.sv
F�r upplysningar om hur man k�r Freeciv, se README.sv

Om man aldrig har spelat civilisationsspel �r det enklast om man b�rjar
med att l�sa Freecivs handbok som �r tillg�nglig vid:

     http://www.freeciv.org/manual/manual.html

F�r upplysningar om hur man spelar Freeciv, forts�tt l�sa!

	N�r man har f�tt i g�ng Freeciv vill man antagligen spela sina
f�rsta spel. Man r�ds att spela ensam n�gra g�nger f�r att f� en
k�nsla f�r hur saker fungerar, men det �r inte n�dv�ndigt. Man kan
l�ra sig genom att spela mot andra m�nniskor eller mot datorn.

Fr�ga: Vilken �r den grundl�ggande strategin?

	F�r det f�rsta �r detta inte en *perfekt* strategi; det �r
inte ens en mycket bra strategi. Men l�t oss f�rst s�tta i g�ng med att
spela Freeciv. En del av lockelsen hos Freeciv �r att utveckla nya
strategier.

	Strategin �r indelat i flera steg:

		Det inledande utvidgningsskedet
		Teknologiskt underskede
		Det andra utvidgningsskedet
		Uppbyggnadsskedet
		Den slutgiltiga f�rintelsen av dina fiender-skedet

Det inledande utvidgningsskedet:

	Detta skede �r viktigast. Det f�rsta man b�r g�ra �r att
grunda st�der och utforska sin �. Man b�r ha m�nga st�der, mist 7
eller 8.

	Spelets m�l �r att beh�rska s� m�nga landrutor som m�jligt.
N�r man grundar en stad b�r man se till att att dess omr�de inte
kommer att �verlappa f�r mycket med andra st�ders omr�den. Man kan se
vilka rutor som anv�nds av en stad genom att klicka p� den. Kartan
�ver staden och dess omgivning visar stadens omr�de. Ta h�nsyn till
detta och f�rs�k samtidigt att h�lla dina st�der n�ra varandra. Ju
l�ngre fr�n varandra de �r, desto sv�rare �r det att f�rsvara och
f�rvalta dem i detta skede. (H�nvisning: F�rs�k att grunda st�der p�
h�star eller n�ra fisk.)

	N�r man har 1 eller 2 st�der b�r man s�tta forskningssatsen s�
h�gt som regeringsformen till�ter. Man ska strunta i skattesatsen
eftersom man inte kommer att f�rdigst�lla n�gra stadsf�rb�ttringar som
kostar underh�ll regelbundet. Man bygger i st�llet bos�ttare. Varje
stad b�r bygga bos�ttare. Ju fler bos�ttare man f�rdigst�ller, desto
fler st�der kan man grunda; ju fler st�der man har, desto fortare kan
man forska; ju fortare man kan forska, desto fortare kan man vinna.
N�r man har fyllt det tillg�ngliga utrymmet med st�der befaller man
sina bos�ttare att bygga bevattningsanl�ggningar och v�gar.

(Anm�rkning: Om mat�verskottet i en stad sjunker till +1 p� grund
av underst�d av f�r m�nga bos�ttare och man inte kan flytta om
arbetare s� att �verskottet blir st�rre l�ter man staden bygga ett
tempel i st�llet f�r bos�ttare. Om man inte har k�nner sig hotad av
n�gon annan spelare �n kan man strunta i att bygga krigsenheter ett
tag till.)

	Under denna tid l�r man sig teknologier s� fort som m�jligt.
Det man b�r sikta p� f�rst �r "republik", sedan "folkstyre", sedan
"j�rnv�g", sedan "industrialisering". (Vissa siktar p� "monarki" f�re
"republik".) S� snart som man forskat fram en regeringsform som man
vill anv�nda s�tter man i g�ng en revolution och byter till den nya
regeringsformen n�r revolutionen �r �ver. Sedan ser man till att
satserna �r som man vill ha dem, ty den h�gsta till�tna satsen skiljer
sig mellan olika regeringsformer (forskningssatsen s�tts s� h�gt som
m�jligt av sig sj�lv).

	N�r man har f�tt kunskap om folkstyre �r man rustad att g� in
i det andra utvidgningsskedet. Det g�r man genom att �ndra
regeringsform till folkstyre, befalla alla st�der att bygga tempel och
s�tta �verfl�dssatsen till 100%. N�r man g�r detta b�rjar alla st�der
genast att fira och v�xer 1 steg varje omg�ng s� l�nge det finns
mat�verskott. N�r de har blivit tillr�ckligt stora s�tt
�verfl�dssatsen till rimliga 20-40%. Detta f�rs�tter dig i det andra
utvidgningsskedet.

	Nackdelen med att s�tta �verfl�dssatsen till 100% �r att
forskningen avstannar fullst�ndigt. Efter att st�derna har vuxit och
forskningssatsen har h�jts till ungef�r 50% f�r man �ter nya
teknologier, men n�got saktare. Om man har utforskat ganska mycket
utan att ha hittat n�gon hotfull motspelare kan det vara bra att
forska s� mycket som m�jligt tills teknologierna tar f�r l�ng tid att
forska fram.

Det andra utvidgningsskedet:

	N�r st�derna har f�tt en l�mplig storlek avv�njer man dem med
�verfl�d och �kar skattesatsen. N�r de �r nere p� ungef�r 30% �verfl�d
�kar man forskningssatsen s� mycket man kan utan att skatteinkomsten
blir l�gre �n utgifterna. N�r man f�r j�rnv�g bygger man om alla v�gar
till j�rnv�gar, �tminstone alla som �r p� rutor som anv�nds av n�gon
stads arbetare eller ing�r i fj�rrv�gn�tet. (H�nvisning: Utrusta varje
ruta som anv�nds av en stad med v�g/j�rnv�g. D� g�r staden mer nytta.
Det finns ingen anledning att uppgradera mittrutan, den med staden p�.
Det g�rs av sig sj�lv.)

	Nu �r det dags att utveckla industrialisering och
krigsteknologier. Man b�r b�rja grunda st�der p� andra �ar och
utforska ordentligt om man inte redan har gjort det. Man b�r ta reda
p� var fienderna �r. Sikta p� teknologier som �r bra f�r b�tar och
f�rs�k f�rdigst�lla Magellans V�rldsomsegling. N�r man k�nner dig
beredd g�r man in i:

Uppbyggnadsskedet:

	Nu bygger man fabriker och kraftverk i st�derna. Man f�rs�ker
f� s� h�g tillverkningsf�rm�ga som m�jligt. F�rorening blir ett
problem. S� snart man kan f�rs�ker man forska fram masstillverkning
f�r kollektivtrafik och �tervinning f�r �tervinningsanl�ggning. N�r
man har gjort alla sina st�der till goda tillverkningsanl�ggningar
bygger man krigsenheter. (Anm�rkning: Om man f�r f�rbindelse med n�gon
annan spelare ska man genast bygga n�gra angreppsenheter och minst 1
f�rsvarsenhet f�r varje stad.)

	N�r man vill b�rja angripa n�gon s�tter man forskningssatsen
till 0% och h�jer skattesatsen s� mycket man kan utan att f� upplopp.
Kom ih�g att enheter kan k�pas f�r guld!

Den slutgiltiga f�rintelsen av dina fiender-skedet:

	Detta kan ske n�r som helst men det �r skojigare med de
framskridna vapnen.

	V�lj en f�rh�llandevis svag fiende och skicka �ver n�gra
b�tlaster trupper. Ta �ver fiendens st�der och l�t dem bygga fler
enheter f�r att utpl�na resten av fienden. Visa ingen medk�nsla!
Intill d�den!

Upprepa s� ofta som det beh�vs! ;-)

[Anm�rkning f�r fredliga: Freeciv l�ter �ven en spelare vinna genom
att f�rdigst�lla och s�nda iv�g ett rymdskepp som anl�nder till Alfa
Kentauri f�re alla andra spelares eventuella rymdskepp.]


Ytterligare fr�gor:

Fr�ga: Vilka andra strategier finns det?

	Det finns ett antal handledningar och strategianvisningar
tillg�ngliga vid Freecivs webplats vid:

	 http://www.freeciv.org/tutorials/

Dessutom beskriver Freecivs inbyggda hj�lp en annan strategi.


Fr�ga: Vilken tidsgr�ns skall man s�tta i flerspelarspel?

	Det beror p� antalet spelare. Om man bara �r 2 som spelar
klarar man sig vanligtvis med tidsgr�nsen 0. Om man �r > 2 eller en av
de 2 kommer att vara borta fr�n spelet under slumpm�ssliga tillf�llen
och den andre inte vill v�nta kan en tidsgr�ns p� 1 minut (n�r man
s�tter tidsgr�nsen anger man dock tiden i sekunder) vara tillr�ckligt.
Senare i spelet n�r det �r mer att g�ra vill man antagligen �ka
tidsgr�nsen till 4 minuter. I allm�nhet beh�vs det l�ngre tid ju fler
spelare man �r. S�tt den tidsgr�ns som passar men kom i h�g att det brukar
st�ra folk om man g�r �ver 5 minuter.

Fr�ga: Vilken kartstorlek ska man anv�nda?

	Kartstorleken beror p� hur m�nga spelare man �r och hur snart
man vill att spelet ska ta slut. Standardkartstorleken (80x50) �r
tillr�ckligt stor f�r ett ganska snabbt 2-spelarspel men ger ett 
*mycket* snabbt spel om > 3 spelare deltar.

	Snabba spel brukar vara otillfredst�llande f�r alla utom
vinnaren eftersom ingen har haft tid att utveckla n�got f�rsvar. Om
man har > 3 spelare b�r man anv�nda en 80x80-karta. Om man �r > 5
spelare b�r man ha en 100x100-karta.


Fr�ga: Vad inneb�r servervalm�jligheten "generator"?

	Den p�verkar det s�tt p� vilket kartan skapas. Om man spelat
Freeciv n�gra g�nger utan att �ndra p� denna servervalm�jlighet har
man s�kerligen h�rt talas om (och/eller upplevt) problemen med en
alldeles f�r liten �. Att beh�va b�rja p� en alldeles f�r liten � kan
g�ra folk vansinniga. F�r att �tg�rda detta har v�ra godhj�rtade
hj�ltar till programmerare byggt in servervalm�jligheten "generator".
N�r det �r satt till 1 ger det en vanlig karta med �ar av olika
(or�ttvisa) storlekar. N�r den �r satt till 2, 3 eller 4 ger den �ar
med lika storlek (ibland �ven n�gra ytterligare sm��ar). Genom att
spela med likv�rdiga �ar kan man se till att ingen kan beklaga sig
�ver att f�rlora p� grund av "den d�r f�rbannade lilla �n".

Fr�ga: Ska man f�renkla spelet genom att �ka m�ngden guld som spelarna
       f�r att b�rja med?

	Om en oerfaren spelare som spelar mot erfarna spelare f�resl�r
en �kning av guldm�ngden kommer antagligen ingen att beklaga sig.
Detta �r dock inget bra s�tt att l�ra sig att spela. Att b�rja med
massor av guld g�r spelet mycket enklare och g�r det sv�rare att l�ra
sig att hanskas med standardguldm�ngden. De flesta erfarna spelare
�kar inte m�ngden, och de vet hur de ska f� mest nytta av en �kning.
Det vet inte oerfarna spelare. D�rf�r g�r de i s� fall samma �de till
m�tes som Atlantis.

Anm�rkning: Samma sak g�ller inst�llningarna "techlevel" och
"researchspeed"

Fr�ga: Hur �r det med de andra inst�llningarna?

	Resten av dem har mest att g�ra med vilken sorts karta som
kommer att skapas inf�r spelet. Att �ka "specials" ger st�rre
sannolikhet att f� m�nga tillg�ngar/ruta och "huts" avg�r hur m�nga
mindre stambyar det ska finnas. Att �ka antalet bos�ttare och
utforskare g�r att spelet g�r fortare och �kar sannolikheten att
spelare �verlever barbarstammarna som i bland finns i de mindre
stambyarna.

	Inst�llningarna som har med j�rnv�g att g�ra avg�r hur mycket
en ruta ger i form av mat/sk�ldar/handel med j�rnv�g och "foodbox"
best�mmer hur mycket mat varje medborgare i en stad m�ste ha innan en
ny person kan l�ggas till.

	F�r �vrigt ger h�gre "mountains" mer berg, h�gre "deserts" ger
mer �ken och s� vidare.

Fr�ga: Hur f�r man en viss teknologi?

	Titta i den inbyggda hj�lpen. Den visar vilka teknologier man
m�ste ha f�rst.

Det g�r �ven att se detta i David Pfitzners "techtree"-karta som kan
laddas ned fr�n <http://msowww.anu.edu.au/~dwp/freeciv/>.

Om det inte g�r man kan l�sa i "data/default/techs.ruleset". D�r st�r
vilka teknologier som man m�ste ha innan man kan f� en given
teknologi.

Fr�ga: Vilka krigsenheter �r b�st?

   F�r angrepp:

	pansar, helikopter, kryssningsmissil, slagskepp, fraktskepp,
	k�rnvapen, haubits, bombflygplan.

   F�r f�rsvar:

	pansar, mekaniserat infanteri, haubits, slagskepp,
	kryssningsmissil, k�rnvapen.

Kom i h�g att det b�sta f�rsvaret �r ett kraftfullt angrepp.

Till�gg till detta dokument �r v�lkomna!
