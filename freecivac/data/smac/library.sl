define set_unit_type(id, utype)
{
  ut_set_name(id, utype.name);
  ut_set_graphic(id, utype.graphic, utype.graphic_alt);
  ut_set_move_rate(id, utype.move_rate);
  ut_set_ints(utype.move_type, utype.build_cost, utype.pop_cost,
              utype.attack, utype.defense,
	      utype.vision_range, utype.transport_cap,
	      utype.hitpoints, utype.firepower, utype.fuel,
	      utype.uk_happy, utype.uk_shield, utype.uk_food,
	      utype.uk_gold, id);
  ut_set_flags(id, utype.flags);
  ut_set_roles(id, utype.roles);
}

define clear_flags(utype)
{
  utype.flags = Integer_Type[F_LAST];
}

define set_flag(utype, flag)
{
  variable ind = unit_flag_from_str(flag);
  if (ind != F_LAST) {
    utype.flags[ind] = 1;
  }
}

define unset_flag(utype, flag)
{
  variable ind = unit_flag_from_str(flag);
  if (ind != F_LAST) {
    utype.flags[ind] = 0;
  }
}

define get_flag(utype, flag)
{
  variable ind = unit_flag_from_str(flag);
  if (ind != F_LAST) {
    return utype.flags[ind];
  } else {
    return 0;
  }
}

define clear_roles(utype)
{
  utype.roles = Integer_Type[L_LAST - L_FIRST];
}

define set_role(utype, role)
{
  variable ind = unit_role_from_str(role);
  if (ind != L_LAST) {
    utype.roles[ind - L_FIRST] = 1;
  }
}

define unset_role(utype, role)
{
  variable ind = unit_role_from_str(role);
  if (ind != L_LAST) {
    utype.roles[ind - L_FIRST] = 0;
  }
}

define get_unit_type(id)
{
  variable ut = ut_get(id);
  ut.flags = ut_get_flags(id);
  ut.roles = ut_get_roles(id);
  return ut;
}

define get_gov(id)
{
  variable gt = gt_get(id);
  return gt;
}

define set_gov(id, gtype)
{
  gt_set_name(id, gtype.name);
}

define end_form_compound_unit(ut, reactor, chassis, attval, defval, moveval,
                              attname, defname, specials)
{
  variable mincost, attcost, defcost, movecost, buildcost = 0.0;

  % Form unit name
  if (attname != "" and defname != "") {
    if (ut.defense > ut.attack) {
      % Defensive unit
      ut.name += defname + " " + chassis[1];
    } else if (ut.move_type == Air and get_flag(ut, "Fighter")) {
      % Air interceptor
      ut.name += attname + " " + chassis[1];
    } else {
      % Offensive unit
      ut.name += attname + " " + chassis[0];
    }
  }

  % The reactor value determines the hp and transport capacity
  ut.hitpoints += reactor * 10;
  ut.transport_cap *= reactor;

  % Calculate build cost, using SMAC formula
  % Cost = Weapon * (Armor + Speed) * 10 / (2 ^ (Reactor + 1))
  attcost = attval;
  defcost = defval;
  movecost = moveval;

  % The weapon value is never less than half the armor value
  if (attval < defval / 2) {
    attcost = defcost / 2;
  }
  % Armor is discounted 50 percent for sea units
  if (ut.move_type == Sea) {
    defcost /= 2;
  % Armor cost is doubled for air units
  } else if (ut.move_type == Air) {
    defcost *= 2;
  }

  buildcost = 1.0 * attcost * (defcost + movecost) * 10 / (2 ^ (reactor + 1));

  % Cost is halved for sea units
  if (ut.move_type == Sea) {
    buildcost /= 2;
  % Cost is quartered for combat air units
  } else if (ut.move_type == Air and ut.attack > 0) {
    buildcost /= 4;
  }

  % Cost is halved for units with speed 1
  if (ut.move_rate == 1) {
    buildcost /= 2;
  }

  % Minimum cost (Reactor * 2 - Reactor / 2) * 10 unless all values are 1.
  if (attval > 1 or defval > 1 or moveval > 1 or reactor > 1) {
    mincost = (reactor * 2 - reactor / 2) * 10;
    if (buildcost < mincost) {
      buildcost = mincost;
    }
  }

  % Cost +10 if both the weapon and armor value is greater than 1
  if (attval > 1 and defval > 1) {
    buildcost += 10;
  }

  % Cost +10 if a land unit's weapon, armor, and speed are all greater than 1
  if (ut.move_type == Land and attval > 1 and defval > 1 and moveval > 1) {
    buildcost += 10;
  }

  % Each special adds 25% to the cost
  buildcost += (buildcost * 0.25 * specials);

  % Ensure build cost is always a multiple of 10, and not zero
  ut.build_cost = typecast((buildcost + 9.99), Integer_Type);
  ut.build_cost /= 10;
  if (ut.build_cost == 0) {
    ut.build_cost++;
  }
  ut.build_cost *= 10;
}
