[spec]

; Format and options of this spec file:
options = "+spec2"

[info]

artists = "
    Tim F. Smith <yoohootim@hotmail.com>
"

[file]
gfx = "misc/buildings"

[grid_imprvm]

x_top_left = 1
y_top_left = 1
dx = 36
dy = 20
is_pixel_border = 1

tiles = { "row", "column", "tag"
  0,  0, "b.palace"
  0,  1, "b.barracks"
  0,  2, "b.granary"
  0,  3, "b.temple"
  0,  4, "b.marketplace"
  0,  5, "b.library"
  0,  6, "b.courthouse"
  0,  7, "b.city_walls"

  1,  0, "b.aqueduct"
  1,  1, "b.bank"
  1,  2, "b.cathedral"
  1,  3, "b.university"
  1,  4, "b.mass_transit"
  1,  5, "b.colosseum"
  1,  6, "b.factory"
  1,  7, "b.mfg_plant"

  2,  0, "b.sdi_defense"
  2,  1, "b.recycling_center"
  2,  2, "b.power_plant"
  2,  3, "b.hydro_plant"
  2,  4, "b.nuclear_plant"
  2,  5, "b.stock_exchange"
  2,  6, "b.sewer_system"
  2,  7, "b.supermarket"

  3,  0, "b.super_highways"
  3,  1, "b.research_lab"
  3,  2, "b.sam_battery"
  3,  3, "b.coastal_defense"
  3,  4, "b.solar_plant"
  3,  5, "b.harbour"
  3,  6, "b.offshore_platform"
  3,  7, "b.airport"

  4,  0, "b.police_station"
  4,  1, "b.port_facility"
  4,  2, "b.space_structural"
  4,  3, "b.space_component"
  4,  4, "b.space_modules"
  4,  5, "b.capitalization"
;  4,  6, ""
;  4,  7, ""

  5,  0, "b.pyramids"
  5,  1, "b.hanging_gardens"
  5,  2, "b.colossus"
  5,  3, "b.lighthouse"
  5,  4, "b.great_library"
  5,  5, "b.oracle"
  5,  6, "b.great_wall"
;  5,  7, ""

  6,  0, "b.sun_tzus_war_academy"
  6,  1, "b.king_richards_crusade"
  6,  2, "b.marco_polos_embassy"
  6,  3, "b.michelangelos_chapel"
  6,  4, "b.copernicus_observatory"
  6,  5, "b.magellans_expedition"
  6,  6, "b.shakespeares_theatre"
;  6,  7, ""

  7,  0, "b.leonardos_workshop"
  7,  1, "b.js_bachs_cathedral"
  7,  2, "b.isaac_newtons_college"
  7,  3, "b.asmiths_trading_co"
  7,  4, "b.darwins_voyage"
  7,  5, "b.statue_of_liberty"
  7,  6, "b.eiffel_tower"
;  7,  7, ""

  8,  0, "b.womens_suffrage"
  8,  1, "b.hoover_dam"
  8,  2, "b.manhattan_project"
  8,  3, "b.united_nations"
  8,  4, "b.apollo_program"
  8,  5, "b.seti_program"
  8,  6, "b.cure_for_cancer"
;  8,  7, ""

}

