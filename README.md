# Freeciv Alpha Centauri project

Originally developed at https://sourceforge.net/projects/freecivac/ by [benwebb](http://sourceforge.net/users/benwebb), [renatocborges](http://sourceforge.net/users/renatocborges) and [stewartadcock](http://sourceforge.net/users/stewartadcock) and published under the GPL-2.0 license.